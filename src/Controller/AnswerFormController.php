<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;
use App\Entity\Question;
use App\Entity\Factor;
use App\Entity\Answer;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
//services can have more functions
//service example: $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
//service example: $article = $articleService->getArticle($id);
use App\Service\AnswerService as ServiceAnswer;
use App\Service\UserService as ServiceUser;
use App\Service\PropertyService as ServiceProperty;
use App\Service\CompanyService as CompanyService;
use App\Service\QuestionService as QuestionService;

class AnswerFormController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

  /**
   * @Route("/answers/getall", methods={"GET"})
   */
  public function getAllAnswers(Request $request) {

      $entityManager = $this->getDoctrine()->getManager();
      $sql = "SELECT * FROM answer";
      $stmt = $entityManager->getConnection()->prepare($sql);
      $stmt->execute();
      $answerResults = $stmt->fetchAll();

      return new JsonResponse(array('message' => $answerResults));

  }

  /**
   * @Route("/answers/remove/{answerId}", methods={"GET"})
   */
  public function removeAnswer(Request $request, $answerId) {

      $entityManager = $this->getDoctrine()->getManager();
      $sql = "DELETE FROM answer WHERE id = '".$answerId."'";
      $stmt = $entityManager->getConnection()->prepare($sql);
      $stmt->execute();

      return new JsonResponse(array('message' => 'Affected '.$stmt->rowCount().' rows!'));

  }

    /**
     * @Route("/{locale}/questionform/answer/{companyId}/{formName}/", methods={"GET"})
     */
    public function questionFormAnswerForm(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $userId = $security->getUser()->getId();
          $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
          $hasPermission = $companyService->checkPermission($companyId, $userId);
          if (!$hasPermission) { return new JsonResponse(array('message' => 'No permission!')); }

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          $companyName = $company->getName();

          if ($formName) {
            $entityManager = $this->getDoctrine()->getManager();
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

            $tabsArray = array();
            foreach ($tabsResults as $i => $product) {
              array_push($tabsArray, $tabsResults[$i]['tab_name']);
            }
            $tabsArray = array_unique($tabsArray);
          } else {
            $formName = '';
          }

          $selectedTabName = array_shift($tabsArray);
          $selectedTabObj = (object)[];
          $selectedTabObj->tab_name = $selectedTabName;

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."' AND tab_name = '".$selectedTabName."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabQuestionResults = $stmt->fetchAll();
          if (!$tabQuestionResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          return $this->redirect('/'.$locale.'/questionform/answer/'.$companyId.'/'.$formName.'/'.$selectedTabObj->tab_name);

          /*
          return $this->render('questionform-answer/'.$locale.'.index.html.twig', array(
              'companyId' => $companyId,
              'companyName' => $companyName,
              'formName' => $formName,
              'selectedTab' => $selectedTabObj,
              'tabNames' => $tabsArray,
              'tabQuestions' => $tabQuestionResults ));
        */
        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/answer/{companyId}/{formName}/{tabName}/", methods={"GET"})
     */
    public function questionFormAnswerFormTab(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $targetTime = $request->get('targetTime');
          if (!$targetTime) { $targetTime = time(); }

          if (preg_match('/(jan)/', $tabName) || preg_match('/(tammikuu)/', $tabName)) {
             $targetTimeFormatted = date('Y-m-d', strtotime('first day of january this year'));
          } if (preg_match('/(feb)/', $tabName) || preg_match('/(helmikuu)/', $tabName)) {
             $targetTimeFormatted = date('Y-m-d', strtotime('first day of february this year'));
          } else {
             $targetTimeFormatted = date('Y-m-d', $targetTime);
          }


/*        switch ($tabName) {
            case 'jan';
            case 'tam';
              $targetTimeFormatted = date('Y-m-d', strtotime('first day of january this year'));
              break;
            case 'feb';
              $targetTimeFormatted = date('Y-m-d', strtotime('first day of february this year'));
              break;
            default;
              $targetTimeFormatted = date('Y-m-d', $targetTime);
              break;
          } */

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          $companyName = $company->getName();
          $entityManager = $this->getDoctrine()->getManager();

          if ($formName) {
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

            $tabsArray = array();
            foreach ($tabsResults as $i => $product) {
              array_push($tabsArray, $tabsResults[$i]['tab_name']);
              if ($tabsResults[$i]['tab_name'] === $tabName) {
                $selectedTab = $tabsResults[$i];
              }
            }
            $tabsArray = array_unique($tabsArray);
          } else {
            $formName = '';
          }

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."' AND tab_name = '".$selectedTab['tab_name']."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabQuestionResults = $stmt->fetchAll();
          if (!$tabQuestionResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          foreach ($tabQuestionResults as $i => $product) {
              $sql = "SELECT * FROM factor WHERE id = '".$tabQuestionResults[$i]['factor_id']."'";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $factorResults = $stmt->fetch();
              if (!$factorResults) { return new JsonResponse(array('message' => 'No factor results!')); }
              $tabQuestionResults[$i]['factor'] = $factorResults;
              $tabQuestionResults[$i]['factor']['valueFloated'] = (float)$factorResults['value'];

              $sql = "SELECT time, content, target_time, user_id FROM answer WHERE question_id = '".$tabQuestionResults[$i]['id']."' AND company_id = '".$companyId."' AND form_name = '".$formName."' ".
              "AND tab_name = '".$selectedTab['tab_name']."' ORDER BY time DESC;";
              //$sql = "SELECT time, content FROM answer WHERE question_id = '".$tabQuestionResults[$i]['id']."' AND company_id = '".$companyId."' AND form_name = '".$formName."' ".
              //"AND tab_name = '".$selectedTab['tab_name']."' AND time >= DATE(NOW()) - INTERVAL 7 DAY ORDER BY time DESC;";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $answerResults = $stmt->fetch();

              if ($answerResults) {
                $sql = "SELECT username, email FROM user WHERE id = '".$answerResults['user_id']."'";
                $stmt = $entityManager->getConnection()->prepare($sql);
                $stmt->execute();
                $userResults = $stmt->fetch();
                if (!$userResults) { return new JsonResponse(array('message' => 'No user results!')); }

                $tabQuestionResults[$i]['time'] = $answerResults['time'];
                $tabQuestionResults[$i]['user'] = $userResults;
                $tabQuestionResults[$i]['answer'] = $answerResults['content'];
                $tabQuestionResults[$i]['timeFi'] = date('d.m.Y', $answerResults['time']);
                $tabQuestionResults[$i]['timeUk'] = date('d/m/Y', $answerResults['time']);
                //$tabQuestionResults[$i]['targetTimeFi'] = date('d.m.Y', $answerResults['target_time']);
                //$tabQuestionResults[$i]['targetTimeUk'] = date('d/m/Y', $answerResults['target_time']);
                /* "Month or year" start */
                $monthEnd = strtotime('last day of this month', $answerResults['target_time']);
                $monthEndFormatted = date('d.m.Y', $monthEnd);
                if ($monthEndFormatted === date('d.m.Y', $answerResults['target_time'])) {
                  $tabQuestionResults[$i]['targetTimeFi'] = date('Y', $answerResults['target_time']);
                  $tabQuestionResults[$i]['targetTimeUk'] = date('Y', $answerResults['target_time']);
                } else {
                  $tabQuestionResults[$i]['targetTimeFi'] = date('m/Y', $answerResults['target_time']);
                  $tabQuestionResults[$i]['targetTimeUk'] = date('m/Y', $answerResults['target_time']);
                }
                /* "Month or year" end */
              } else {
                $tabQuestionResults[$i]['time'] = '';
              }
          }

          return $this->render('questionform-answer/'.$locale.'.index.html.twig', array(
              'companyId' => $companyId,
              'companyName' => $companyName,
              'targetTime' => $targetTime,
              'targetTimeFormatted' => $targetTimeFormatted,
              'formName' => $formName,
              'selectedTab' => $selectedTab,
              'tabNames' => $tabsArray,
              'tabQuestions' => $tabQuestionResults));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/answer/{companyId}/{formName}/{tabName}/", methods={"POST"})
     */
    public function questionFormAnswerFormTabPost(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $allParams = $request->request->all();
          $answers = $allParams['answers'];
          $targetTime = $allParams['targetTime'];

          $user = $security->getUser();
          $entityManager = $this->getDoctrine()->getManager();

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'No company!')); }

          foreach ($answers as $i => $product) {
            //echo $answers[$i]['content'];

            $answer = new Answer();
            $question = $this->getDoctrine()->getRepository(Question::class)->find($answers[$i]['question-id']);
            if (!$question) { return new JsonResponse(array('message' => 'No question!')); }

            $factor = $question->getFactor();
            if (!$question) { return new JsonResponse(array('message' => 'No factor!')); }

            $answer->setContent($answers[$i]['content']);
            $answer->setFormName($formName);
            $answer->setTabName($tabName);
            $answer->setQuestion($question);
            $answer->setUser($user);
            $answer->setCompany($company);
            $answer->setFactor($factor);
            $answer->setTargetTime(strtotime($targetTime));
            $answer->setTime(time());

            $entityManager->persist($answer);
            $entityManager->flush();
          }

          $this->addFlash('notice',
            'Vastaukset päivitetty onnistuneesti. Voit vastata lomakkeen muihin välilehtiin.');
          return $this->redirect('/'.$locale.'/questionform/answer/'.$companyId.'/'.$formName.'/'.$tabName.'/'.'?targetTime='.strtotime($targetTime));
          //return new JsonResponse(array('message' => $answers));

        } else {
          return $this->redirectToRoute('welcome');
        }
    }

    /**
     * @Route("/{locale}/questionform/showanswers/{companyId}/{formName}/", methods={"GET"})
   */
    public function questionFormShowAnswers(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          //return new JsonResponse(array('message' => 'questionFormShowAnswers'));
          $filteredBy = $request->get('filteredBy');
          if (!$filteredBy) { $filteredBy = 'all'; }

          $targetTime = $request->get('targetTime');
          if (!$targetTime) { $targetTime = time(); }

          $targetTimeFormatted = date('Y-m-d', $targetTime);

          $entityManager = $this->getDoctrine()->getManager();

          if ($formName) {
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          } else {
            $formName = '';
          }

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          //rename tabQuestions

          $questionResults = $stmt->fetchAll();
          if (!$questionResults) { return new JsonResponse(array('message' => 'No results!')); }

          $enableSubmit = false;
          $answers = array();
          $noAnswers = array();
          foreach ($questionResults as $i => $product) {
            $sql = "SELECT id, title_fi, title_en, value, measure FROM factor WHERE id = '".$questionResults[$i]['factor_id']."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $factorResults = $stmt->fetch();
            if (!$factorResults) { return new JsonResponse(array('message' => 'No factor results!')); }
            $questionResults[$i]['factor'] = $factorResults;
            $questionResults[$i]['factor']['valueFloated'] = (float)$factorResults['value'];

            $sql = "SELECT id, user_id, time, content, question_id, target_time FROM answer WHERE question_id = '".$questionResults[$i]['id']."' AND company_id = '".$companyId."' AND form_name = '".$formName."' ".
            "ORDER BY time DESC;";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $answerResults = $stmt->fetch();
            if ($answerResults) {
              $sql = "SELECT id, name, username, email FROM user WHERE id = '".$answerResults['user_id']."'";
              $stmt = $entityManager->getConnection()->prepare($sql);
              $stmt->execute();
              $userResults = $stmt->fetch();
              if (!$userResults) { return new JsonResponse(array('message' => 'No user results!')); }
              $questionResults[$i]['user'] = $userResults;

              $questionResults[$i]['time'] = $answerResults['time'];
              $questionResults[$i]['answer'] = $answerResults['content'];
              $questionResults[$i]['question_id'] = $answerResults['id'];

              $questionResults[$i]['timeFi'] = date('d.m.Y', $answerResults['time']);
              $questionResults[$i]['timeUk'] = date('d/m/Y', $answerResults['time']);
              $monthEnd = strtotime('last day of this month', $answerResults['time']);
              $monthEndFormatted = date('d.m.Y', $monthEnd);
              if ($monthEndFormatted === date('d.m.Y', $answerResults['target_time'])) {
                $questionResults[$i]['targetTimeFi'] = date('Y', $answerResults['target_time']);
                $questionResults[$i]['targetTimeUk'] = date('Y', $answerResults['target_time']);
              } else {
                $questionResults[$i]['targetTimeFi'] = date('m/Y', $answerResults['target_time']);
                $questionResults[$i]['targetTimeUk'] = date('m/Y', $answerResults['target_time']);
              }
              array_push($answers, $questionResults[$i]);
            } else {
              $questionResults[$i]['time'] = '';
              $questionResults[$i]['answer'] = 'No answer';
              $enableSubmit = true;
              array_push($noAnswers, $questionResults[$i]);
            }
          }

          if ($filteredBy !== 'all') {
            $newAnswers = array();
            foreach ($answers as $i => $value) {
                if ($answers[$i]['tab_name'] == $filteredBy)  {
                    $newAnswers[] = $answers[$i];
                }
            }
            $answers = $newAnswers;
          }

          /*
          TABS
          */
          $notAnsweredTabsArray = array();
          $notAnsweredQuestions = array();
          $addedInArray = array();
          foreach ($noAnswers as $i => $product) {
            if (!in_array($noAnswers[$i]['tab_name'], $addedInArray)) {
              array_push($notAnsweredTabsArray, $noAnswers[$i]);
              array_push($addedInArray, $noAnswers[$i]['tab_name']);
            }
          }

          foreach ($noAnswers as $i => $product) {
            foreach ($notAnsweredTabsArray as $o => $product) {
              if ($noAnswers[$i]['tab_name'] == $notAnsweredTabsArray[$o]['tab_name']) {
                if (!isset($notAnsweredTabsArray[$o]['questions'])) {
                  $notAnsweredTabsArray[$o]['questions'] = array();
                }
                /*
                $noAnswers[$i]['targetTimeFi'] = 'test';
                $monthEnd = strtotime('last day of this month', $noAnswers[$i]['target_time']);
                $monthEndFormatted = date('d.m.Y', $monthEnd);
                if ($monthEndFormatted === date('d.m.Y', $noAnswers[$i]['target_time'])) {
                  $noAnswers[$i]['targetTimeFi'] = date('Y', $noAnswers[$i]['target_time']);
                  $noAnswers[$i]['targetTimeUk'] = date('Y', $noAnswers[$i]['target_time']);
                } else {
                  $noAnswers[$i]['targetTimeFi'] = date('m/Y', $noAnswers[$i]['target_time']);
                  $noAnswers[$i]['targetTimeUk'] = date('m/Y', $noAnswers[$i]['target_time']);
                }
                */
                //$noAnswers[$i]['timeFi'] = date('d.m.Y', $noAnswers[$i]['time']);
                //$noAnswers[$i]['timeUk'] = date('d/m/Y', $noAnswers[$i]['time']);
                array_push($notAnsweredTabsArray[$o]['questions'], $noAnswers[$i]);
              }
            }
          }

          // Paginate the results of the query
          $answersPaginated = $paginator->paginate(
              // Doctrine Query, not results
              $answers,
              // Define the page parameter
              $request->query->getInt('page', 1),
              // Items per page
              5
          );

          $sql = "SELECT tab_name FROM answer WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabsResults = $stmt->fetchAll();
          if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          $answeredTabsArray = array();
          foreach ($tabsResults as $i => $product) {
              array_push($answeredTabsArray, $tabsResults[$i]['tab_name']);
          }
          $answeredTabsArray = array_unique($answeredTabsArray);

          //echo 'filtered by: '.$filteredBy;

          return $this->render('questionform-answer/'.$locale.'.show-answers.html.twig', array(
              'filteredBy' => $filteredBy,
              'targetTime' => $targetTime,
              'targetTimeFormatted' => $targetTimeFormatted,
              'companyId' => $companyId,
              'formName' => $formName,
              'answeredTabsArray' => $answeredTabsArray,
              'answers' => $answers,
              'answersPaginated' => $answersPaginated,
              'noAnswers' => $noAnswers,
              'notAnsweredTabsArray' => $notAnsweredTabsArray,
              'enableSubmit' => $enableSubmit ));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }
}
