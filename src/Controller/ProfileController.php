<?php
namespace App\Controller;

use App\Entity\Article;
use App\Entity\Property;
use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Knp\Component\Pager\PaginatorInterface;

use App\Service\ArticleService as ServiceArticle;
use App\Service\PropertyService as ServiceProperty;

class ProfileController extends AbstractController {

    private function assertLocale($locale) {
      if ($locale !== 'fi' && $locale !== 'en') {
        return $this->redirect('/');
      }
    }

    /**
     * @Route("/article/test", methods={"GET"})
     */
    public function test(Request $request) {

      $em = $this->getDoctrine()->getManager();
      $jsonResult = $em->getRepository(Article::class)
      ->createQueryBuilder('u')
      ->where('u.id = 1')
      ->getQuery()
      ->getArrayResult();

      return new JsonResponse($jsonResult);

    }

    /**
     * @Route("/{locale}/manage/profiles", methods={"GET"})
     */
    public function profileManage(Security $security, Request $request, PaginatorInterface $paginator, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          if ($security->getUser()->getRole() !== 'super-user') {
          //  return new JsonResponse(array('message' => 'No permission!'));
          }

          $entityManager = $this->getDoctrine()->getManager();
          $userId = $security->getUser()->getId();
          $sql = "SELECT id, username, email, name, role, joined FROM user";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $usersResults = $stmt->fetchAll();

          $totalUsersAmount = count($usersResults);

          foreach ($usersResults as $i => $product) {
            $usersResults[$i]['joinedDateFi'] = date('d.m.Y', $usersResults[$i]['joined']);
            $usersResults[$i]['joinedDateUk'] = date('d/m/Y', $usersResults[$i]['joined']);
          }

          $filteredBy = $request->get('filteredBy');
          if ($filteredBy && $filteredBy !== 'all') {
            if ($filteredBy == 'user') { $filteredBy = ''; } //basic user is '' in db
            foreach ($usersResults as $i => $product) {
              if ($usersResults[$i]['role'] !== $filteredBy) {
                unset($usersResults[$i]);
              }
            }
            if ($filteredBy == '') { $filteredBy = 'user'; } //back to work
          }
          else {
            $filteredBy = 'all';
          }


          // Paginate the results of the query
          $newUsersResults = $paginator->paginate(
              $usersResults,
              $request->query->getInt('page', 1),
              5
          );

          return $this->render('profile/'.$locale.'.profiles.html.twig', array
          ('users' => $newUsersResults, 'totalUsersAmount' => $totalUsersAmount, 'filteredBy' => $filteredBy));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/manage/profile/{userId}", methods={"GET"})
     */
    public function profileManageView(Security $security, Request $request, $locale, $userId) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $user = $security->getUser();
        if ($user->getId() !== $userId) { //for the cookie death
          $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
        }

        $user->joinedDateFi = date('d.m.Y', $user->getJoined());
        $user->joinedDateUk = date('d/m/Y', $user->getJoined());

        return $this->render('profile/'.$locale.'.manage.view.html.twig',array(
            'profile' => $user, 'locale' => $locale
        ));

    }

    /**
     * @Route("/{locale}/manage/profile/update/{userId}", methods={"GET"})
     */
    public function profileManageUpdate(Security $security, Request $request, PaginatorInterface $paginator, $locale, $userId) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if ($security->getUser()->getRole() !== 'super-user') {
        //  return new JsonResponse(array('message' => 'No permission!'));
        }

        $entityManager = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM user WHERE id = '".$userId."'";
        $stmt = $entityManager->getConnection()->prepare($sql);
        $stmt->execute();
        $usersResult = $stmt->fetch(); //fetchAll() for many

        if (!$usersResult) {
          return new JsonResponse(array('message' => 'No results!'));
        }

        $usersResult['joinedDateFi'] = date('d.m.Y', $usersResult['joined']);
        $usersResult['joinedDateUk'] = date('d/m/Y', $usersResult['joined']);

        return $this->render('profile/'.$locale.'.manage.update.html.twig',array(
            'profile' => $usersResult, 'locale' => $locale
        ));

    }

    /**
     * @Route("/{locale}/manage/profile/update/{userId}", methods={"POST"})
     */
    public function updateManagePost(Security $security, UserPasswordEncoderInterface $passwordEncoder, Request $request, $locale, $userId) {

          $em = $this->getDoctrine()->getManager();
          $user = $security->getUser();

          if ($security->getUser()->getRole() !== 'super-user') {
          //  return new JsonResponse(array('message' => 'No permission!'));
          }

          if ($user->getId() !== $userId) { //for the cookie death
            $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
          }

          $user->setName($request->get('name'));
          $user->setPhone($request->get('phone'));
          $user->setAddress($request->get('address'));
          $user->setZip($request->get('zip'));
          $user->setCity($request->get('city'));
          $user->setLanguage($request->get('language'));
          $user->setEmail($request->get('email'));
          $user->setRole($request->get('role'));

          $em->flush();
          $newLocale = $request->get('language');
          return $this->redirect('/'.$newLocale.'/manage/profiles/');

    }

    /**
     * @Route("/{locale}/profile", methods={"GET"})
     */
    public function profile(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $userId = $security->getUser()->getId();
          $user = $security->getUser();
          $user->joinedDateFi = date('d.m.Y', $user->getJoined());
          $user->joinedDateUk = date('d/m/Y', $user->getJoined());

          $em = $this->getDoctrine()->getManager();
          $properties = $em->getRepository(Property::class)
          ->createQueryBuilder('u')
          ->where('u.user = '.$userId)
          ->getQuery()
          ->getResult();

          $totalAreaSum = 0;
          $totalSizeSum = 0;
          $totalArticleQuantity = 0;
          foreach ($properties as $i => $product) {
             $properties[$i]->userId = $properties[$i]->getUser()->getId();

             $properties[$i]->username = $properties[$i]->getUser()->getUsername();

             foreach ($articles = $properties[$i]->getArticles() as $k => $product) {
                $totalAreaSum = $totalAreaSum + $articles[$k]->getArea();

                $totalArticleSizeSum = intval($articles[$k]->getMat()) +
                intval($articles[$k]->getKut()) +
                intval($articles[$k]->getKot()) +
                intval($articles[$k]->getMut()) +
                intval($articles[$k]->getMak()) +
                intval($articles[$k]->getKuk()) +
                intval($articles[$k]->getKok()) +
                intval($articles[$k]->getMuk()) +
                intval($articles[$k]->getEnp());
                $totalSizeSum = $totalSizeSum + $totalArticleSizeSum;

                $totalArticleQuantity = $totalArticleQuantity + 1;
             }
          }

          $propertiesSummary = array(
            'totalAreaSum' => $totalAreaSum,
            'totalSizeSum' => $totalSizeSum,
            'totalValue' => $locale,
            'totalArticleQuantity' => $totalArticleQuantity
          );

          return $this->render('profile/'.$locale.'.index.html.twig', array
          ('profile' => $user,
          'propertiesSummary' => $propertiesSummary,
          'properties' => $properties));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/profile/update/{data}", methods={"GET"})
     */
    public function update(Security $security, RequestStack $requestStack, Request $request, $locale, $data) {

        $response = $this->assertLocale($locale);
        //this works at least: if ($response instanceof \Symfony\Component\HttpFoundation\Response) { return $response; }
        if ($response) { return $response; }

        $user = $security->getUser();

        return $this->render('profile/'.$locale.'.update.html.twig',array(
            'profile' => $user, 'locale' => $locale, 'data' => $data
        ));
    }

    /**
     * @Route("/{locale}/profile/update/{data}", methods={"POST"})
     */
    public function updatePost(Security $security, UserPasswordEncoderInterface $passwordEncoder, Request $request, $locale, $data) {
        if ($data == 'info') {

          $em = $this->getDoctrine()->getManager();
          $user = $security->getUser();

          $user->setName($request->get('name'));
          $user->setPhone($request->get('phone'));
          $user->setAddress($request->get('address'));
          $user->setZip($request->get('zip'));
          $user->setCity($request->get('city'));
          $user->setLanguage($request->get('language'));
          $user->setEmail($request->get('email'));

          $em->flush();

          $newLocale = $request->get('language');
          return $this->redirect('/'.$newLocale.'/profile');

        } if ($data == 'password') {

          $em = $this->getDoctrine()->getManager();
          $user = $security->getUser();
          $checkPass = $passwordEncoder->isPasswordValid($user, $request->get('currentPassword'));

          if($checkPass === true && ($request->get('newPassword1') === $request->get('newPassword2'))) {
            $newPassword = $passwordEncoder->encodePassword($user, $request->get('newPassword1'));
            $user->setPassword($newPassword);
            $em->flush();

            $this->addFlash('notice',
              'Password changed successfully.');
            return $this->redirect('/'.$locale.'/profile/');
          } else {
            $this->addFlash('alert',
              'Error, re-type the old and new passwords again!');
            return $this->redirect('/'.$locale.'/profile/update/password');
          }

        } else {
          return $this->redirect('/'.$locale.'/profile');
        }

    }

}
