<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\ImportedArticle;
use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Yectep\PhpSpreadsheetBundle\Factory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer as Writer;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;

use App\Service\ImportedArticleService as ServiceImportedArticle;
use App\Service\ArticleService as ArticleService;
use App\Service\PropertyService as PropertyService;

class FunctionsController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

  private function checkPermission($security) {
    if ($security->getUser() && $security->getUser()->getRole() == 'super-user') {
      return;
    } else {
      return $this->redirect('/');
    }
  }

    /**
     * @Route("/{locale}/functions/excelget", methods={"GET"})
     */
    public function excelGetTestAction(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $filepath = "./other/";
      $filename = "input_malli.xlsx";

      if(file_exists($filepath.$filename)){

        $response = new BinaryFileResponse($filepath.$filename);

        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        if($mimeTypeGuesser->isSupported()){
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($filepath.$filename));
        } else {
            $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        }

        // Set content disposition inline of the file
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        return $response;

    } else {
      return new JsonResponse(array('message' => 'File does not exist!'));
    }

    }

    /**
     * @Route("/{locale}/functions/excel", methods={"GET"})
     */
    public function excelTestAction(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $filepath = "./other/";
      $filename = "input_malli.xlsx";

      if(file_exists($filepath.$filename)) {

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filepath.$filename);
        $sheet = $spreadsheet->setActiveSheetIndex(0);

        //iterate real data next

        /*
        $sheet->setCellValue('A3', '1');
        $sheet->setCellValue('B3', '20');
        $sheet->setCellValue('C3', '1');
        $sheet->setCellValue('D3', '4');
        $sheet->setCellValue('E3', '24');
        $sheet->setCellValue('F3', '1200');
        $sheet->setCellValue('G3', '140');
        $sheet->setCellValue('H3', '40');
        $sheet->setCellValue('I3', '18');
        $sheet->setCellValue('J3', '0.8');
        $sheet->setCellValue('K3', '0.1');
        $sheet->setCellValue('L3', '0');
        $sheet->setCellValue('M3', '2');
        $sheet->setCellValue('N3', '150');
        $sheet->setCellValue('O3', '10');

        $spreadsheet->setActiveSheetIndex(0);
        */
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");

        $response =  new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="ExportScan1.xlsx"');
        $response->headers->set('Cache-Control','max-age=0');

        return $response;

        /* Binary way below
        $writer->save($filepath."temp-".$filename);
        $response = new BinaryFileResponse($filepath."temp-".$filename);
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        if($mimeTypeGuesser->isSupported()) {
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($filepath."temp-".$filename));
        } else {
            $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        }

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        )->deleteFileAfterSend(true);

        return $response;
        */
    } else {
      return new JsonResponse(array('message' => 'File does not exist!'));
    }

    }

    /**
   * @Route("/{locale}/functions/uploadresultsexcel", methods={"GET"})
   */
  public function uploadResultsFromExcel(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
      if ($security->getUser()->getRole() == 'super-user') {

        return $this->render('functions/'.$locale.'.uploadresultsfromexcel.html.twig', array
        ('showData' => false));

      } else {
        return $this->redirectToRoute('welcome');
      }
    } else {
      return $this->redirectToRoute('welcome');
    }

  }

  /**
   * @Route("/{locale}/functions/uploadresultsexcel", methods={"POST"})
   */
  public function uploadResultsFromExcelPost(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    //return new JsonResponse(array('message' => $_FILES));
	$em = $this->getDoctrine()->getManager();

    if (isset($_FILES["excel"])) {
      $file = $_FILES["excel"]["tmp_name"]; // getting temporary source of excel file
      $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
      $sheet = $spreadsheet->getActiveSheet();

      $data = $sheet->toArray(null,true,true,true);
      array_shift($data);
      array_shift($data);
      if (!$data[count($data)-1]['A']) { //last has no ['A']
          array_pop($data);
      }
	$importedArticles = array();
	$propertyOwner = '';
	$validArticles = array();
	$invalidArticles = array();
	foreach ($data as $i => $value) {
		//echo $data[$i]['P'];
		$importedArticle = $em->getRepository(ImportedArticle::class)->find($data[$i]['A']);
        if ($importedArticle) {
		  if (!$data[$i]['P'] && !$data[$i]['Q'] && !$data[$i]['R'] && !$data[$i]['S']) {
			//return new JsonResponse(array('message' => 'No results in this file!'));
			array_push($invalidArticles, $importedArticle);
		  } else {
			array_push($validArticles, $importedArticle);
		  }
		  $additionality5yearsHa = 0;
		  $additionality10yearsHa = 0;
		  $additionality5yearsPercentage = 0;
		  $additionality10yearsPercentage = 0;

		  if ($data[$i]['P'] > 0) {
			  $additionality5yearsHa = $data[$i]['P'];
		  }
		  if ($data[$i]['Q'] > 0) {
			  $additionality10yearsHa = $data[$i]['Q'];
		  }
		  if ($data[$i]['R'] > 0) {
			  $additionality5yearsPercentage = $data[$i]['R'];
		  }
		  if ($data[$i]['S'] > 0) {
			  $additionality10yearsPercentage = $data[$i]['S'];
		  }

		  $importedArticle->setAdditionality5yearsHa($additionality5yearsHa);
		  $importedArticle->setAdditionality10yearsHa($additionality10yearsHa);
		  $importedArticle->setAdditionality5yearsPercentage($additionality5yearsPercentage);
		  $importedArticle->setAdditionality10yearsPercentage($additionality10yearsPercentage);

		  $property = $importedArticle->getProperty();
		  $propertyOwner = $property->getOwner();

		  array_push($importedArticles, $importedArticle);
		  $em->persist($importedArticle);
		  $em->flush();
        }
	}

      //return new JsonResponse(array('message' => $data));
	  return $this->render('functions/'.$locale.'.uploadresultsfromexcel.html.twig', array
    ('importedArticles' => $importedArticles,
		'invalidArticles' => $invalidArticles,
		'validArticles' => $validArticles,
		'propertyOwner' => $propertyOwner,
		'showData' => true));

  } else {
      return new JsonResponse(array('message' => 'No Excel!'));
  }

  }

  /**
   * @Route("/{locale}/functions/", methods={"GET"})
   */
  public function functionsAction(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    $hasPermission = $this->checkPermission($security);
    if ($hasPermission) { return $hasPermission; }

    return $this->render('functions/'.$locale.'.index.html.twig', array
    ());

  }

  /**
   * @Route ("/{locale}/functions/importedarticles/", methods={"GET"})
   */
  public function findUserAction(Security $security, $locale) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
      $foundOwners = $propertyService->getAllPropertiesAsArray();

      //return new JsonResponse($foundOwners);

      //return new JsonResponse($foundOwners);
      $ownerResults = array();
      if ($foundOwners) {
      $known = array();
      $filteredOwners = array_filter($foundOwners, function ($val) use (&$known) {
          $unique = !in_array($val['owner'], $known);
      $known[] = $val['owner'];
      return $unique;
      });
      $filteredOwners = array_values($filteredOwners);

      $ownerResults = array();
      foreach ($filteredOwners as $i => $value) {
        $propsArray = array();
        foreach ($foundOwners as $k => $value) {
          if ($filteredOwners[$i]['owner'] == $foundOwners[$k]['owner']) {
            array_push($propsArray, $foundOwners[$k]);
            $filteredOwners[$i]['propertyquantity'] = count($propsArray);
          }
        }
        $filteredOwners[$i]['name'] = $filteredOwners[$i]['owner'];
        unset($filteredOwners[$i]['owner']);
        unset($filteredOwners[$i]['address']);
        unset($filteredOwners[$i]['id']);
        unset($filteredOwners[$i]['identifier']);
        array_push($ownerResults, $filteredOwners[$i]);
      }

      //return new JsonResponse($ownerResults);
      }

      $ownerNames = [];
      foreach ($ownerResults as $i => $value) {
        $ownerNames[] = $ownerResults[$i]['name'];
      }

      return $this->render('functions/'.$locale.'.finduser.html.twig', [
        'owners' => $ownerResults,
        'ownerNames' => $ownerNames
      ]);

  }

  /**
   * @Route ("/functions/api/searchuser/{owner}", methods={"GET"})
   */
  public function searchUserAPI(Security $security, $owner) {

    $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
    $foundOwners = $propertyService->searchByOwner($owner);

    //return new JsonResponse($foundOwners);

    $known = array();
    $filteredOwners = array_filter($foundOwners, function ($val) use (&$known) {
        $unique = !in_array($val['owner'], $known);
    $known[] = $val['owner'];
    return $unique;
    });
    $filteredOwners = array_values($filteredOwners);

    $ownerResults = array();
    foreach ($filteredOwners as $i => $value) {
      $propsArray = array();
      foreach ($foundOwners as $k => $value) {
        if ($filteredOwners[$i]['owner'] == $foundOwners[$k]['owner']) {
          array_push($propsArray, $foundOwners[$k]);
          $filteredOwners[$i]['propertyquantity'] = count($propsArray);
        }
      }
      $filteredOwners[$i]['name'] = $filteredOwners[$i]['owner'];
      unset($filteredOwners[$i]['owner']);
      unset($filteredOwners[$i]['address']);
      unset($filteredOwners[$i]['id']);
      unset($filteredOwners[$i]['identifier']);
      array_push($ownerResults, $filteredOwners[$i]);
    }

    return new JsonResponse($ownerResults);

  }

  /**
   * @Route("/{locale}/functions/exportplans/", methods={"GET"})
   */
  public function showImportedArticlesExportPlans(Security $security, $locale, Request $request, PaginatorInterface $paginator) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $filterNoTrees = isset($allGetParams['filterNoTrees']);

      $allGetParams = $request->query->all();
      if (!isset($allGetParams['sel'])) {
        return new JsonResponse(array('message' => 'No owners!'));
      }

      $em = $this->getDoctrine()->getManager();
      $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
      $allProperties = array();

      $owners = $allGetParams['sel'];
      foreach ($owners as $i => $product) {
        $foundProperties = $propertyService->searchByOwner($owners[$i]);
        foreach ($foundProperties as $f => $product) {
          $allProperties[] = $foundProperties[$f];
        }
      }
      $foundProperties = $allProperties;
      //return new JsonResponse(array('message' => $foundProperties));

      $propertyObjects = array();
      foreach ($foundProperties as $i => $product) {
        $property = $em->getRepository(Property::class)->find($foundProperties[$i]['id']);
        if ($property) {
          array_push($propertyObjects, $property);
        }
      }

      $importedArticles = array();
      foreach ($propertyObjects as $i => $product) {
        foreach ($propertyObjects[$i]->getImportedArticles() as $k => $product) {
          $propertyObjects[$i]->getImportedArticles()[$k]->propertyName = $propertyObjects[$i]->getAddress();
          $propertyObjects[$i]->getImportedArticles()[$k]->propertyIdentifier = $propertyObjects[$i]->getIdentifier();
          $propertyObjects[$i]->getImportedArticles()[$k]->propertyOwner = $propertyObjects[$i]->getOwner();
          array_push($importedArticles, $propertyObjects[$i]->getImportedArticles()[$k]);
        }
      }

      foreach ($importedArticles as $i => $product) {
        $importedArticles[$i]->standNumber1 = $importedArticles[$i]->getStandNumber();
        $importedArticles[$i]->area1 = $importedArticles[$i]->getArea();
        $importedArticles[$i]->setProperty(null);
        $importedArticles[$i]->dateFi = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->dateEn = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->noTreeData = false;
        $importedArticles[$i]->propertyId = $property->getId();

        if ($importedArticles[$i]->getMantyAge() < 1
            && $importedArticles[$i]->getKuusiAge() < 1
            && $importedArticles[$i]->getRauduskoivuAge() < 1
            && $importedArticles[$i]->getHieskoivuAge() < 1
            && $importedArticles[$i]->getHaapaAge() < 1
            && $importedArticles[$i]->getHarmaaleppaAge() < 1
            && $importedArticles[$i]->getTervaleppaAge() < 1
            && $importedArticles[$i]->getLehtipuuAge() < 1
            && $importedArticles[$i]->getHavupuuAge() < 1) {
              $importedArticles[$i]->noTreeData = true;
        }
      }

      if ($filterNoTrees) {
        $importedArticlesWithoutTreeless = array();
        foreach ($importedArticles as $i => $product) {
          if (!$importedArticles[$i]->noTreeData) {
            array_push($importedArticlesWithoutTreeless, $importedArticles[$i]);
          }
        }
        $importedArticles = $importedArticlesWithoutTreeless;
      }

      if (isset($allGetParams['operation'])) {
        $operation = $allGetParams['operation'];
        $filteredByOperation = array();
          foreach ($importedArticles as $i => $product) {
            if ($operation == 'afforestation') {
              if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
              && ($importedArticles[$i]->getDevelopmentclass() == 'A0 - Aukea')) {
              array_push($filteredByOperation, $importedArticles[$i]);
              }
            }
          if ($operation == 'fertilization') {
            if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
            && ($importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '03 - Varttunut kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '04 - Uudistuskypsä metsikkö' )) {
            array_push($filteredByOperation, $importedArticles[$i]);
            }
          }
          }
          $importedArticles = $filteredByOperation;
      }

      $removedArticles = array();
      /*
      $selected = isset($allGetParams['sel']);
      if ($selected) {
        $selectedArray = $allGetParams['sel'];
        $filteredArticles = array();
        foreach ($selectedArray as $i => $product) {
          foreach ($importedArticles as $k => $product) {
            if ($selectedArray[$i] == $importedArticles[$k]->getId()) {
              array_push($filteredArticles, $importedArticles[$k]); //push to an empty array
              unset($importedArticles[$k]); //remove from the original array
            }
          }
        }
        $removedArticles = $importedArticles; //this becomes the original array
        $importedArticles = $filteredArticles; //old name for the new array
      }
    */

	  $newArticles = array(); //remove articles without results
	  foreach ($importedArticles as $i => $product) {
		  if ($importedArticles[$i]->getAdditionality5yearsHa() > 0.0
		  || $importedArticles[$i]->getAdditionality10yearsHa() > 0.0
		  || $importedArticles[$i]->getAdditionality5yearsPercentage() > 0.0
		  || $importedArticles[$i]->getAdditionality10yearsPercentage() > 0.0) {
			  array_push($newArticles, $importedArticles[$i]);
		  }
	  }
	  $importedArticles = $newArticles;

	  $removedArticlesWithResults = []; //remove removedarticles without results
	  foreach ($removedArticles as $i => $product) {
		  if ($removedArticles[$i]->getAdditionality5yearsHa() > 0.0
		  || $removedArticles[$i]->getAdditionality10yearsHa() > 0.0
		  || $removedArticles[$i]->getAdditionality5yearsPercentage() > 0.0
		  || $removedArticles[$i]->getAdditionality10yearsPercentage() > 0.0) {
			  array_push($removedArticlesWithResults, $removedArticles[$i]);
		  }
	  }
	  $removedArticles = $removedArticlesWithResults;

	  $result1Total = 0;
	  $result2Total = 0;
	  $result4Total = 0;
      $index = 1;
      $articleIDs = array();
      foreach ($importedArticles as $i => $product) {
		$importedArticles[$i]->totalVolumePerArea = number_format($importedArticles[$i]->getTotalVolumeHa() * $importedArticles[$i]->getArea(), 2, '.', '');

		$planYears = 0;
		if ($importedArticles[$i]->getAdditionality5yearsHa() > 0) { //kangasmaa
			$planYears = '5';
		} else { //turvemaa
			$planYears = '10';
		}
		$importedArticles[$i]->planYears = $planYears;
		$importedArticles[$i]->result1 = $importedArticles[$i]->getAdditionality5yearsHa(); //jako 5???
		$result1Total = $result1Total + $importedArticles[$i]->result1;
		$importedArticles[$i]->result2 = $importedArticles[$i]->getAdditionality5yearsHa(); //jako 5???
		$result2Total = $result1Total + $importedArticles[$i]->result2;
		if ($importedArticles[$i]->getAdditionality5yearsHa() > 0) { //kangasmaa
			$importedArticles[$i]->futureOperations = 'Kasvatuslannoitus 2020';
			$importedArticles[$i]->result4 = number_format($importedArticles[$i]->getAdditionality5yearsHa() * $importedArticles[$i]->getArea() * $planYears, 2, '.', '');
			$result4Total = $result4Total + number_format($importedArticles[$i]->result1 * $importedArticles[$i]->getArea() * $planYears, 2, '.', '');
			$importedArticles[$i]->resultAdditionHa = number_format($importedArticles[$i]->result1, 2, '.', '');
			$importedArticles[$i]->resultAdditionHa = number_format($importedArticles[$i]->resultAdditionHa, 2, '.', '');
			$importedArticles[$i]->compensationSum = 0;
			$importedArticles[$i]->compensationSumPerArticle = 0;
		} else { //turvemaa
			$importedArticles[$i]->futureOperations = 'Tuhkalannoitus 2020';
			$importedArticles[$i]->result4 = 0;
			$result4Total = 0;
			$importedArticles[$i]->resultAdditionHa = $importedArticles[$i]->result2;
			$importedArticles[$i]->compensationSum = 0;
			$importedArticles[$i]->compensationSumPerArticle = 0;
		}
        array_push($articleIDs, $importedArticles[$i]->getId());
        $importedArticles[$i]->index = $index;
        $index++;
      }

      /* Divided by name!
      $result = array();
      if (count($importedArticles) > 0) {
          foreach ($importedArticles as $value) {
              $name = $value->propertyOwner;

              $result[$name][] = $value;
          }
      }
      return new JsonResponse(array('message' => $result));
      */
    //return new JsonResponse(array('message' => $importedArticles));

      $arrayData = array();
      foreach ($importedArticles as $i => $product) {
        //$arrayData[] = array($importedArticles[$i]->propertyOwner);
        $importedArticleData = array();
        $importedArticleData[] = $importedArticles[$i]->propertyOwner;
        $importedArticleData[] = $importedArticles[$i]->standNumber1;
        $importedArticleData[] = $importedArticles[$i]->propertyIdentifier;
        $importedArticleData[] = $importedArticles[$i]->propertyName;
        $importedArticleData[] = $importedArticles[$i]->area1;
        $importedArticleData[] = $importedArticles[$i]->result1;
        $importedArticleData[] = $importedArticles[$i]->result2;
        $importedArticleData[] = $importedArticles[$i]->planYears;
        $arrayData[] = $importedArticleData;
      }

      $filepath = "./other/";
      $filename = "plans_sheet.xlsx";

      if(file_exists($filepath.$filename)) {

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filepath.$filename);
        $sheet = $spreadsheet->setActiveSheetIndex(0);

        $spreadsheet->getActiveSheet()
            ->fromArray(
                $arrayData,  // The data to set
                NULL,        // Array values with this value will not be set
                'A2'         // Top left coordinate of the worksheet range where
            );

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");

        $response =  new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="Valitut_hiilisuunnitelmat.xlsx"');
        $response->headers->set('Cache-Control','max-age=0');

        return $response;

        } else {
          return new JsonResponse(array('message' => 'File does not exist!'));
        }

  }

  /**
   * @Route("/{locale}/functions/importedarticles/getexcel/owner/{owner}", methods={"GET"})
   */
  public function showImportedArticlesExcel(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);
      $encoders = [new XmlEncoder(), new JsonEncoder()];
      $normalizers = [new ObjectNormalizer()];
      $serializer = new Serializer($normalizers, $encoders);
      //$allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));
      $allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));

      $keepTooShort = isset($allGetParams['keepTooShort']);
	    $filterNoTrees = isset($allGetParams['filterNoTrees']);

      //return new JsonResponse(array('message' => $allGetParams['operation']));

      $em = $this->getDoctrine()->getManager();
      $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
      $foundProperties = $propertyService->searchByOwner($owner);

      if (!$foundProperties) {
        return new JsonResponse(array('message' => 'No properties for the owner!'));
      }

      $user = $security->getUser();
      if (!$user) {
        return $this->redirect('/');
      }
      if ($security->getUser()->getRole() !== 'super-user') {
        return new JsonResponse(array('message' => 'No permission!'));
      }

      $propertyObjects = array();
      foreach ($foundProperties as $i => $product) {
        $property = $em->getRepository(Property::class)->find($foundProperties[$i]['id']);
        if ($property) {
          array_push($propertyObjects, $property);
        }
      }

      $importedArticles = array();
      foreach ($propertyObjects as $i => $product) {
        foreach ($propertyObjects[$i]->getImportedArticles() as $k => $product) {
          $propertyObjects[$i]->getImportedArticles()[$k]->propertyName = $propertyObjects[$i]->getAddress();
		  $propertyObjects[$i]->getImportedArticles()[$k]->propertyIdentifier = $propertyObjects[$i]->getIdentifier();
		  $articles = $propertyObjects[$i]->getImportedArticles()[$k];
          array_push($importedArticles, $articles);
        }
      }

      foreach ($importedArticles as $i => $product) {
        $importedArticles[$i]->setProperty(null);
        $importedArticles[$i]->dateFi = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->dateEn = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->noTreeData = false;
        //$importedArticles[$i]->propertyId = $property->getId();
        $importedArticles[$i]->propertyId = $importedArticleService->searchByPropertyIdByImportedArticleId($importedArticles[$i]->getId());

        if ($importedArticles[$i]->getMantyAge() < 1
            && $importedArticles[$i]->getKuusiAge() < 1
            && $importedArticles[$i]->getRauduskoivuAge() < 1
            && $importedArticles[$i]->getHieskoivuAge() < 1
            && $importedArticles[$i]->getHaapaAge() < 1
            && $importedArticles[$i]->getHarmaaleppaAge() < 1
            && $importedArticles[$i]->getTervaleppaAge() < 1
            && $importedArticles[$i]->getLehtipuuAge() < 1
            && $importedArticles[$i]->getHavupuuAge() < 1) {
              $importedArticles[$i]->noTreeData = true;
        }
      }

      if (!$keepTooShort) {
        $importedArticlesWithoutTooShort = array();
        foreach ($importedArticles as $i => $product) {
          if ($importedArticles[$i]->getDevelopmentclass() !== 'Taimikko alle 1,3 m' && $importedArticles[$i]->getDevelopmentclass() !== 'Taimikko yli 1,3 m') {
            array_push($importedArticlesWithoutTooShort, $importedArticles[$i]);
          }
        }
        $importedArticles = $importedArticlesWithoutTooShort;
      }

      if ($filterNoTrees) {
        $importedArticlesWithoutTreeless = array();
        foreach ($importedArticles as $i => $product) {
          if (!$importedArticles[$i]->noTreeData) {
            array_push($importedArticlesWithoutTreeless, $importedArticles[$i]);
          }
        }
        $importedArticles = $importedArticlesWithoutTreeless;
      }

      if (isset($allGetParams['operation'])) {
        $operation = $allGetParams['operation'];
        $filteredByOperation = array();
          foreach ($importedArticles as $i => $product) {
            if ($operation == 'afforestation') {
              if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
              && ($importedArticles[$i]->getDevelopmentclass() == 'A0 - Aukea')) {
              array_push($filteredByOperation, $importedArticles[$i]);
              }
            }
          if ($operation == 'fertilization') {
            if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
            && ($importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '03 - Varttunut kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '04 - Uudistuskypsä metsikkö' )) {
            array_push($filteredByOperation, $importedArticles[$i]);
            }
          }
          }
          $importedArticles = $filteredByOperation;
      }

      $removedArticles = array();
      $selected = isset($allGetParams['sel']);
      if ($selected) {
        $selectedArray = $allGetParams['sel'];
        $filteredArticles = array();
        foreach ($selectedArray as $i => $product) {
          foreach ($importedArticles as $k => $product) {
            if ($selectedArray[$i] == $importedArticles[$k]->getId()) {
              array_push($filteredArticles, $importedArticles[$k]); //push to an empty array
              unset($importedArticles[$k]); //remove from the original array
            }
          }
        }
        $removedArticles = $importedArticles; //this becomes the original array
        $importedArticles = $filteredArticles; //old name for the new array
      }


      $index = 1;
      $articleIDs = array();
      foreach ($importedArticles as $i => $product) {
        array_push($articleIDs, $importedArticles[$i]->getId());
        $importedArticles[$i]->index = $index;
        $index++;
      }
      $jsonedArticleIDs = json_encode($articleIDs);

/*
      $importedArticles = (array)$importedArticles;

      usort($importedArticles,function($first,$second){
        return strtolower($first->getStandNumber()) > strtolower($second->getStandNumber());
      });

      usort($importedArticles,function($first,$second){
        return strtolower($first->propertyName) > strtolower($second->propertyName);
      });
*/

      $importedArticlesPaginated = $paginator->paginate(
          // Doctrine Query, not results
          $importedArticles,
          // Define the page parameter
          $request->query->getInt('page', 1),
          // Items per page
          5
      );

      //serializing will remove "custom attributes" like "->index" above
      $jsonContent = $serializer->serialize($importedArticles, 'json');
      $unJsonContent = json_decode($jsonContent, true);
      foreach ($unJsonContent as $i => $product) {
        $sql = "SELECT property_id FROM imported_article WHERE id = '".$unJsonContent[$i]['id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyIdResults = $stmt->fetch();
        $sql = "SELECT address, identifier FROM property WHERE id = '".$propertyIdResults['property_id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyAddressAndIdentifierResults = $stmt->fetch();
        $unJsonContent[$i]['propertyId'] = $propertyIdResults['property_id'];
        $unJsonContent[$i]['propertyAddress'] = $propertyAddressAndIdentifierResults['address'];
        $unJsonContent[$i]['propertyIdentifier'] = $propertyAddressAndIdentifierResults['identifier'];
      }
      $jsonContent = $serializer->serialize($unJsonContent, 'json');

      return $this->render('functions/'.$locale.'.showimportedarticles-getexcel.html.twig',array(
          'owner' => $owner,
          'jsonedArticleIDs' => $jsonedArticleIDs,
          'importedArticles' => $importedArticles,
          'removedArticles' => $removedArticles,
          'importedArticlesPaginated' => $importedArticlesPaginated,
          'importedArticlesJsoned' => $jsonContent));
  }

  /**
   * @Route("/{locale}/functions/importedarticles/getexcel/owner/{owner}", methods={"POST"})
   */
  public function showImportedArticlesExcelPost(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $importedArticleService = new ServiceImportedArticle($this->getDoctrine()->getManager(), ImportedArticle::class);

      $allPostParams = $request->request->all();
      $importedArticlesJsoned = $allPostParams['importedArticlesJsoned'];

      //return new Response($importedArticlesJsoned, 200, [
      //'Content-Type' => 'application/json',
      //]);

      $importedArticlesArray = json_decode($importedArticlesJsoned, true);
      //return new JsonResponse(array('message' => $importedArticlesArray));

      $filepath = "./other/";
      $filename = "input_malli1.xlsx";

      if(file_exists($filepath.$filename)) {

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filepath.$filename);
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $arrayData = array();

        foreach ($importedArticlesArray as $i => $product) {
          $importedArticleData = array();
          $importedArticleData[] = $importedArticlesArray[$i]['id'];
          $importedArticleData[] = explode("-", $importedArticlesArray[$i]['propertyIdentifier'])[0];
          //$subgroup = $importedArticleService->getGrowplaceSubgroupForExcel($importedArticlesArray[$i]['growplace'], $importedArticlesArray[$i]['soiltype']);
          $subgroup = $importedArticlesArray[$i]['subgroupNumber'];
          $importedArticleData[] = $subgroup;
          //$growplace = $importedArticleService->getGrowplaceForExcel($importedArticlesArray[$i]['growplace']);
          $growplace = $importedArticlesArray[$i]['fertilityclassNumber'];
          $importedArticleData[] = $growplace;
          $importedArticleData[] = 0; //Pituusboniteetti
		  $stemCount = "0";
		  if (round($importedArticlesArray[$i]['totalDensity'], 0) > 0) {
			  $stemCount = round($importedArticlesArray[$i]['totalDensity'], 0);
		  }
          $importedArticleData[] = $stemCount; //runkoluku
		  $volumeHa = "0";
		  if (round($importedArticlesArray[$i]['totalVolumeHa'], 0) > 0) {
			$volumeHa = round($importedArticlesArray[$i]['totalVolumeHa'], 0);
		  }
		  $importedArticleData[] = $volumeHa;
		  $totalAge = "0";
		  if (round($importedArticlesArray[$i]['totalAge'], 0) > 0) {
			  $totalAge = round($importedArticlesArray[$i]['totalAge'], 0);
		  }
          $importedArticleData[] = $totalAge;

		  $totalLength = "0";
		  if (round($importedArticlesArray[$i]['totalLength'], 1) > 0) {
			  $totalLength = round($importedArticlesArray[$i]['totalLength'], 1);
		  }
          $importedArticleData[] = $totalLength;

          $mantyVolumePercentage = "0";
          if ($importedArticlesArray[$i]['mantyVolumeHa'] > 0) {
            $mantyVolumePercentage = $importedArticlesArray[$i]['mantyVolumeHa'] /
            ($importedArticlesArray[$i]['mantyVolumeHa'] +
            $importedArticlesArray[$i]['kuusiVolumeHa'] +
            $importedArticlesArray[$i]['rauduskoivuVolumeHa'] +
            $importedArticlesArray[$i]['hieskoivuVolumeHa'] +
            $importedArticlesArray[$i]['haapaVolumeHa'] +
            $importedArticlesArray[$i]['harmaaleppaVolumeHa'] +
            $importedArticlesArray[$i]['tervaleppaVolumeHa'] +
            $importedArticlesArray[$i]['lehtipuuVolumeHa'] +
            $importedArticlesArray[$i]['havupuuVolumeHa']);
            $mantyVolumePercentage = round($mantyVolumePercentage, 2);
            if ($mantyVolumePercentage == 0) { $mantyVolumePercentage = "0"; }
          }
          $importedArticleData[] = $mantyVolumePercentage;

          $kuusiVolumePercentage = "0";
          if ($importedArticlesArray[$i]['kuusiVolumeHa'] > 0) {
            $kuusiVolumePercentage = $importedArticlesArray[$i]['kuusiVolumeHa'] /
            ($importedArticlesArray[$i]['mantyVolumeHa'] +
            $importedArticlesArray[$i]['kuusiVolumeHa'] +
            $importedArticlesArray[$i]['rauduskoivuVolumeHa'] +
            $importedArticlesArray[$i]['hieskoivuVolumeHa'] +
            $importedArticlesArray[$i]['haapaVolumeHa'] +
            $importedArticlesArray[$i]['harmaaleppaVolumeHa'] +
            $importedArticlesArray[$i]['tervaleppaVolumeHa'] +
            $importedArticlesArray[$i]['lehtipuuVolumeHa'] +
            $importedArticlesArray[$i]['havupuuVolumeHa']);
            $kuusiVolumePercentage = round($kuusiVolumePercentage, 2);
            if ($kuusiVolumePercentage == 0) { $kuusiVolumePercentage = "0"; }
          }
          $importedArticleData[] = $kuusiVolumePercentage;

          $importedArticleData[] = "0"; //Ajankohta

          if ($allPostParams['fertilizationType'] == 'Ash') {
            $fertilizationType = 5;
          } if ($allPostParams['fertilizationType'] == 'Saltpeter') {
            $fertilizationType = 1;
          } if ($allPostParams['fertilizationType'] == 'NPK') {
            $fertilizationType = 3;
          } else {
            $fertilizationType = 5;
          }

          if ($subgroup == 1) {
            $fertilizationType = 1;
          }

		  //delete later?
          if ($subgroup == 1) {
            $fertilizationType = 1;
          } elseif ($subgroup == 3) {
            $fertilizationType = 5;
		  } elseif ($subgroup == 2) {
            $fertilizationType = 3;
		  } else {
            $fertilizationType = 1;
		  }
		  //delete later^

		  //newest:
		  if ($subgroup !== 1 && $subgroup !== 2 && $subgroup) { //if is turvemaa, "&& $subgroup" = ei puuton/invalid
            $fertilizationType = 5;
          } else if ($kuusiVolumePercentage > $mantyVolumePercentage) {
			$fertilizationType = 3;
		  } else if ($mantyVolumePercentage > $kuusiVolumePercentage) {
			$fertilizationType = 1;
		  } else {
			$fertilizationType = 1; //invalideille tämä
		  }
		  //newest end

          $importedArticleData[] = $fertilizationType;
          if ($subgroup == 1) {
            $nitrogenAmount = $allPostParams['nitrogenAmount'];
          } else {
            $nitrogenAmount = 0; //empty cell
          }

		  //delete later?
          if ($subgroup == 1) {
            $nitrogenAmount = 150;
          } elseif ($subgroup == 3) {
            $nitrogenAmount = '';
		  } else {
            $nitrogenAmount = '';
		  }
		  //delete later^

          $importedArticleData[] = $nitrogenAmount; //Typpimäärä
          if ($allPostParams['fertilizationType'] == 'Ash') {
            $fertilizationTime = 0;
          } else {
            $fertilizationTime = '';
          }

		  //delete later?
          $fertilizationTime = '';
		  //delete later^

          $importedArticleData[] = $fertilizationTime; //Aika tuhkalannoitukselle

		  //empty fields for results
		  $importedArticleData[] = '';
		  $importedArticleData[] = '';
		  $importedArticleData[] = '';
		  $importedArticleData[] = '';

		  $importedArticleData[] = $importedArticlesArray[$i]['standNumber'].' '.$importedArticlesArray[$i]['propertyAddress'];

          $arrayData[] = $importedArticleData;
        }

        //return new JsonResponse(array('message' => $arrayData));

        $spreadsheet->getActiveSheet()
            ->fromArray(
                $arrayData,  // The data to set
                NULL,        // Array values with this value will not be set
                'A3'         // Top left coordinate of the worksheet range where
            );

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");

        $ownerString = preg_replace('/\s+/', '_', $owner);
    		$ownerString = str_replace("ä", "a", $ownerString);
    		$ownerString = str_replace("å", "a", $ownerString);
    		$ownerString = str_replace("ö", "o", $ownerString);

        $response =  new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$ownerString.'_lukeinput.xlsx"');
        $response->headers->set('Cache-Control','max-age=0');

        return $response;

        } else {
          return new JsonResponse(array('message' => 'File does not exist!'));
        }

  }

    /**
   * @Route("/{locale}/functions/importedarticles/createplan/owner/{owner}", methods={"GET"})
   */
  public function showImportedArticlesCreatePlan(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $encoders = [new XmlEncoder(), new JsonEncoder()];
      $normalizers = [new ObjectNormalizer()];
      $serializer = new Serializer($normalizers, $encoders);
      //$allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));
      $allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));

      $filterNoTrees = isset($allGetParams['filterNoTrees']);

      //return new JsonResponse(array('message' => $allGetParams['operation']));

      $em = $this->getDoctrine()->getManager();
      $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
      $foundProperties = $propertyService->searchByOwner($owner);

      if (!$foundProperties) {
        return new JsonResponse(array('message' => 'No properties for the owner!'));
      }

      $user = $security->getUser();
      if (!$user) {
        return $this->redirect('/');
      }
      if ($security->getUser()->getRole() !== 'super-user') {
        return new JsonResponse(array('message' => 'No permission!'));
      }

      $propertyObjects = array();
      foreach ($foundProperties as $i => $product) {
        $property = $em->getRepository(Property::class)->find($foundProperties[$i]['id']);
        if ($property) {
          array_push($propertyObjects, $property);
        }
      }

      $importedArticles = array();
      foreach ($propertyObjects as $i => $product) {
        foreach ($propertyObjects[$i]->getImportedArticles() as $k => $product) {
          $propertyObjects[$i]->getImportedArticles()[$k]->propertyName = $propertyObjects[$i]->getAddress();
		  $propertyObjects[$i]->getImportedArticles()[$k]->propertyIdentifier = $propertyObjects[$i]->getIdentifier();
          array_push($importedArticles, $propertyObjects[$i]->getImportedArticles()[$k]);
        }
      }

      foreach ($importedArticles as $i => $product) {
        $importedArticles[$i]->setProperty(null);
        $importedArticles[$i]->dateFi = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->dateEn = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->noTreeData = false;
        $importedArticles[$i]->propertyId = $property->getId();

        if ($importedArticles[$i]->getMantyAge() < 1
            && $importedArticles[$i]->getKuusiAge() < 1
            && $importedArticles[$i]->getRauduskoivuAge() < 1
            && $importedArticles[$i]->getHieskoivuAge() < 1
            && $importedArticles[$i]->getHaapaAge() < 1
            && $importedArticles[$i]->getHarmaaleppaAge() < 1
            && $importedArticles[$i]->getTervaleppaAge() < 1
            && $importedArticles[$i]->getLehtipuuAge() < 1
            && $importedArticles[$i]->getHavupuuAge() < 1) {
              $importedArticles[$i]->noTreeData = true;
        }
      }

      if ($filterNoTrees) {
        $importedArticlesWithoutTreeless = array();
        foreach ($importedArticles as $i => $product) {
          if (!$importedArticles[$i]->noTreeData) {
            array_push($importedArticlesWithoutTreeless, $importedArticles[$i]);
          }
        }
        $importedArticles = $importedArticlesWithoutTreeless;
      }

      if (isset($allGetParams['operation'])) {
        $operation = $allGetParams['operation'];
        $filteredByOperation = array();
          foreach ($importedArticles as $i => $product) {
            if ($operation == 'afforestation') {
              if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
              && ($importedArticles[$i]->getDevelopmentclass() == 'A0 - Aukea')) {
              array_push($filteredByOperation, $importedArticles[$i]);
              }
            }
          if ($operation == 'fertilization') {
            if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
            && ($importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '03 - Varttunut kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '04 - Uudistuskypsä metsikkö' )) {
            array_push($filteredByOperation, $importedArticles[$i]);
            }
          }
          }
          $importedArticles = $filteredByOperation;
      }

      $removedArticles = array();
      $selected = isset($allGetParams['sel']);
      if ($selected) {
        $selectedArray = $allGetParams['sel'];
        $filteredArticles = array();
        foreach ($selectedArray as $i => $product) {
          foreach ($importedArticles as $k => $product) {
            if ($selectedArray[$i] == $importedArticles[$k]->getId()) {
              array_push($filteredArticles, $importedArticles[$k]); //push to an empty array
              unset($importedArticles[$k]); //remove from the original array
            }
          }
        }
        $removedArticles = $importedArticles; //this becomes the original array
        $importedArticles = $filteredArticles; //old name for the new array
      }

	  $newArticles = array(); //remove articles without results
	  foreach ($importedArticles as $i => $product) {
		  if ($importedArticles[$i]->getAdditionality5yearsHa() > 0.0
		  || $importedArticles[$i]->getAdditionality10yearsHa() > 0.0
		  || $importedArticles[$i]->getAdditionality5yearsPercentage() > 0.0
		  || $importedArticles[$i]->getAdditionality10yearsPercentage() > 0.0) {
			  array_push($newArticles, $importedArticles[$i]);
		  }
	  }
	  $importedArticles = $newArticles;

	  $removedArticlesWithResults = []; //remove removedarticles without results
	  foreach ($removedArticles as $i => $product) {
		  if ($removedArticles[$i]->getAdditionality5yearsHa() > 0.0
		  || $removedArticles[$i]->getAdditionality10yearsHa() > 0.0
		  || $removedArticles[$i]->getAdditionality5yearsPercentage() > 0.0
		  || $removedArticles[$i]->getAdditionality10yearsPercentage() > 0.0) {
			  array_push($removedArticlesWithResults, $removedArticles[$i]);
		  }
	  }
	  $removedArticles = $removedArticlesWithResults;

	  $result1Total = 0;
	  $result2Total = 0;
	  $result4Total = 0;
      $index = 1;
      $articleIDs = array();
      foreach ($importedArticles as $i => $product) {
		$importedArticles[$i]->totalVolumePerArea = number_format($importedArticles[$i]->getTotalVolumeHa() * $importedArticles[$i]->getArea(), 2, '.', '');

		$planYears = 0;
		if ($importedArticles[$i]->getAdditionality5yearsHa() > 0) { //kangasmaa
			$planYears = '5';
		} else { //turvemaa
			$planYears = '10';
		}
		$importedArticles[$i]->planYears = $planYears;
		$importedArticles[$i]->result1 = $importedArticles[$i]->getAdditionality5yearsHa(); //jako 5???
		$result1Total = $result1Total + $importedArticles[$i]->result1;
		$importedArticles[$i]->result2 = $importedArticles[$i]->getAdditionality5yearsHa(); //jako 5???
		$result2Total = $result1Total + $importedArticles[$i]->result2;
		if ($importedArticles[$i]->getAdditionality5yearsHa() > 0) { //kangasmaa
			$importedArticles[$i]->futureOperations = 'Kasvatuslannoitus 2020';
			$importedArticles[$i]->result4 = number_format($importedArticles[$i]->getAdditionality5yearsHa() * $importedArticles[$i]->getArea() * $planYears, 2, '.', '');
			$result4Total = $result4Total + number_format($importedArticles[$i]->result1 * $importedArticles[$i]->getArea() * $planYears, 2, '.', '');
			$importedArticles[$i]->resultAdditionHa = number_format($importedArticles[$i]->result1, 2, '.', '');
			$importedArticles[$i]->resultAdditionHa = number_format($importedArticles[$i]->resultAdditionHa, 2, '.', '');
			$importedArticles[$i]->compensationSum = 0;
			$importedArticles[$i]->compensationSumPerArticle = 0;
		} else { //turvemaa
			$importedArticles[$i]->futureOperations = 'Tuhkalannoitus 2020';
			$importedArticles[$i]->result4 = 0;
			$result4Total = 0;
			$importedArticles[$i]->resultAdditionHa = $importedArticles[$i]->result2;
			$importedArticles[$i]->compensationSum = 0;
			$importedArticles[$i]->compensationSumPerArticle = 0;
		}
        array_push($articleIDs, $importedArticles[$i]->getId());
        $importedArticles[$i]->index = $index;
        $index++;
      }
      $jsonedArticleIDs = json_encode($articleIDs);

      $importedArticlesPaginated = $paginator->paginate(
          // Doctrine Query, not results
          $importedArticles,
          // Define the page parameter
          $request->query->getInt('page', 1),
          // Items per page
          5
      );

      //serializing will remove "custom attributes" like "->index" above
      $jsonContent = $serializer->serialize($importedArticles, 'json');
      $unJsonContent = json_decode($jsonContent, true);
      foreach ($unJsonContent as $i => $product) {
        $sql = "SELECT property_id FROM imported_article WHERE id = '".$unJsonContent[$i]['id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyIdResults = $stmt->fetch();
        $sql = "SELECT address, identifier FROM property WHERE id = '".$propertyIdResults['property_id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyAddressAndIdentifierResults = $stmt->fetch();
        $unJsonContent[$i]['propertyId'] = $propertyIdResults['property_id'];
        $unJsonContent[$i]['propertyAddress'] = $propertyAddressAndIdentifierResults['address'];
        $unJsonContent[$i]['propertyIdentifier'] = $propertyAddressAndIdentifierResults['identifier'];
      }
      $jsonContent = $serializer->serialize($unJsonContent, 'json');

	  $propertiesArray = array('test1', 'test2');

      return $this->render('functions/'.$locale.'.showimportedarticles-createplan.html.twig',array(
		  'properties' => $propertiesArray,
      'owner' => $owner,
		  'result1Total' => $result1Total,
		  'result2Total' => $result2Total,
		  'result4Total' => $result4Total,
      'jsonedArticleIDs' => $jsonedArticleIDs,
      'importedArticles' => $importedArticles,
      'removedArticles' => $removedArticles,
      'importedArticlesPaginated' => $importedArticlesPaginated,
      'importedArticlesJsoned' => $jsonContent));
  }

  /**
   * @Route("/{locale}/functions/importedarticles/createplan/owner/{owner}", methods={"POST"})
   */
  public function showImportedArticlesCreatePlanPost(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

	  $allPostParams = $request->request->all();
      $topic = $allPostParams['topic'];
      $cost = $allPostParams['cost'];

      $encoders = [new XmlEncoder(), new JsonEncoder()];
      $normalizers = [new ObjectNormalizer()];
      $serializer = new Serializer($normalizers, $encoders);
      //$allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));
      $allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));

      $filterNoTrees = isset($allGetParams['filterNoTrees']);

      //return new JsonResponse(array('message' => $allGetParams['operation']));

      $em = $this->getDoctrine()->getManager();
      $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
      $foundProperties = $propertyService->searchByOwner($owner);

      if (!$foundProperties) {
        return new JsonResponse(array('message' => 'No properties for the owner!'));
      }

      $user = $security->getUser();
      if (!$user) {
        return $this->redirect('/');
      }
      if ($security->getUser()->getRole() !== 'super-user') {
        return new JsonResponse(array('message' => 'No permission!'));
      }

      $propertyObjects = array();
      foreach ($foundProperties as $i => $product) {
        $property = $em->getRepository(Property::class)->find($foundProperties[$i]['id']);
        if ($property) {
          array_push($propertyObjects, $property);
        }
      }

      $importedArticles = array();
      foreach ($propertyObjects as $i => $product) {
        foreach ($propertyObjects[$i]->getImportedArticles() as $k => $product) {
          $propertyObjects[$i]->getImportedArticles()[$k]->propertyName = $propertyObjects[$i]->getAddress();
		  $propertyObjects[$i]->getImportedArticles()[$k]->propertyIdentifier = $propertyObjects[$i]->getIdentifier();
          array_push($importedArticles, $propertyObjects[$i]->getImportedArticles()[$k]);
        }
      }

      foreach ($importedArticles as $i => $product) {
        $importedArticles[$i]->setProperty(null);
        $importedArticles[$i]->dateFi = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->dateEn = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->noTreeData = false;
        $importedArticles[$i]->propertyId = $property->getId();

        if ($importedArticles[$i]->getMantyAge() < 1
            && $importedArticles[$i]->getKuusiAge() < 1
            && $importedArticles[$i]->getRauduskoivuAge() < 1
            && $importedArticles[$i]->getHieskoivuAge() < 1
            && $importedArticles[$i]->getHaapaAge() < 1
            && $importedArticles[$i]->getHarmaaleppaAge() < 1
            && $importedArticles[$i]->getTervaleppaAge() < 1
            && $importedArticles[$i]->getLehtipuuAge() < 1
            && $importedArticles[$i]->getHavupuuAge() < 1) {
              $importedArticles[$i]->noTreeData = true;
        }
      }

      if ($filterNoTrees) {
        $importedArticlesWithoutTreeless = array();
        foreach ($importedArticles as $i => $product) {
          if (!$importedArticles[$i]->noTreeData) {
            array_push($importedArticlesWithoutTreeless, $importedArticles[$i]);
          }
        }
        $importedArticles = $importedArticlesWithoutTreeless;
      }

      if (isset($allGetParams['operation'])) {
        $operation = $allGetParams['operation'];
        $filteredByOperation = array();
          foreach ($importedArticles as $i => $product) {
            if ($operation == 'afforestation') {
              if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
              && ($importedArticles[$i]->getDevelopmentclass() == 'A0 - Aukea')) {
              array_push($filteredByOperation, $importedArticles[$i]);
              }
            }
          if ($operation == 'fertilization') {
            if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
            && ($importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '03 - Varttunut kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '04 - Uudistuskypsä metsikkö' )) {
            array_push($filteredByOperation, $importedArticles[$i]);
            }
          }
          }
          $importedArticles = $filteredByOperation;
      }

      $removedArticles = array();
      $selected = isset($allGetParams['sel']);
      if ($selected) {
        $selectedArray = $allGetParams['sel'];
        $filteredArticles = array();
        foreach ($selectedArray as $i => $product) {
          foreach ($importedArticles as $k => $product) {
            if ($selectedArray[$i] == $importedArticles[$k]->getId()) {
              array_push($filteredArticles, $importedArticles[$k]); //push to an empty array
              unset($importedArticles[$k]); //remove from the original array
            }
          }
        }
        $removedArticles = $importedArticles; //this becomes the original array
        $importedArticles = $filteredArticles; //old name for the new array
      }

	  $newArticles = array(); //remove articles without results
	  foreach ($importedArticles as $i => $product) {
		  if ($importedArticles[$i]->getAdditionality5yearsHa() > 0
		  || $importedArticles[$i]->getAdditionality10yearsHa() > 0
		  || $importedArticles[$i]->getAdditionality5yearsPercentage() > 0
		  || $importedArticles[$i]->getAdditionality10yearsPercentage() > 0) {
			  array_push($newArticles, $importedArticles[$i]);
		  }
	  }
	  $importedArticles = $newArticles;

	  $result1Total = 0;
	  $result2Total = 0;
	  $result4Total = 0;
      $index = 1;
      $articleIDs = array();
	  //yhteensäarvot postiin
	  $totalArea = 0;
	  $totalTotalGrowth = 0;
	  $totalResultAdditionHa = 0;
	  $totalCompensationSum = 0;
	  $totalCompensationSumPerArticle = 0;
	  $totalCompensationSumPerPlantime = 0;
      foreach ($importedArticles as $i => $product) {
		$importedArticles[$i]->totalVolumePerArea = number_format($importedArticles[$i]->getTotalVolumeHa() * $importedArticles[$i]->getArea(), 2, '.', '');

		$planYears = 0;
		if ($importedArticles[$i]->getAdditionality5yearsHa() > 0) { //kangasmaa
			$planYears = '5';
			$importedArticles[$i]->result1 = round($importedArticles[$i]->getAdditionality5yearsHa() * $importedArticles[$i]->getArea(), 2);
		} else { //turvemaa
			$planYears = '10';
			$importedArticles[$i]->result1 = 0;
		}
		$importedArticles[$i]->planYears = $planYears;
		$result1Total = $result1Total + $importedArticles[$i]->result1;
		$importedArticles[$i]->result2 = $importedArticles[$i]->getAdditionality5yearsHa(); //jako 5???
		$result2Total = $result1Total + $importedArticles[$i]->result2;
		if ($importedArticles[$i]->getAdditionality5yearsHa() > 0) { //kangasmaa
			$importedArticles[$i]->futureOperations = 'Kasvatuslannoitus 2020';
			$importedArticles[$i]->result4 = number_format($importedArticles[$i]->getAdditionality5yearsHa() * $importedArticles[$i]->getArea() * $planYears, 2, '.', '');
			$result4Total = number_format(round($result4Total + $importedArticles[$i]->result1 * $importedArticles[$i]->getArea() * $planYears, 2), 2,'.','');
			$importedArticles[$i]->resultAdditionHa = number_format(round($importedArticles[$i]->result1, 2) * $importedArticles[$i]->getArea(), 2,'.','');
			$importedArticles[$i]->resultAdditionHa = number_format($importedArticles[$i]->resultAdditionHa,2,'.','');
			$importedArticles[$i]->compensationSum = number_format(round($cost * $importedArticles[$i]->result1, 2),2,'.','');

			$importedArticles[$i]->compensationSumPerPlantime = number_format($importedArticles[$i]->compensationSum * $planYears,2,'.','');
			$importedArticles[$i]->compensationSumPerArticle = round($importedArticles[$i]->getArea() * $importedArticles[$i]->compensationSum
			* $planYears, 2); //kertaa $planYears jos ei alunperin jaeta
			$importedArticles[$i]->compensationSumPerArticle = number_format($importedArticles[$i]->compensationSumPerArticle,2,'.','');
		} else { //turvemaa
			$importedArticles[$i]->futureOperations = 'Tuhkalannoitus 2020';
			$importedArticles[$i]->result4 = 0;
			$result4Total = 0;
			$importedArticles[$i]->resultAdditionHa = $importedArticles[$i]->result2;
			$importedArticles[$i]->compensationSum = 0;
			$importedArticles[$i]->compensationSumPerPlantime = 0;
			$importedArticles[$i]->compensationSumPerArticle = 0;
		}
        array_push($articleIDs, $importedArticles[$i]->getId());
        $importedArticles[$i]->index = $index;
        $index++;

		$totalArea = $totalArea + $importedArticles[$i]->getArea();
		$totalTotalGrowth = round($totalTotalGrowth + $importedArticles[$i]->getTotalGrowth(), 2);
		$totalResultAdditionHa = round($totalResultAdditionHa + $importedArticles[$i]->resultAdditionHa, 2);
		$totalCompensationSum = round($totalCompensationSum + $importedArticles[$i]->compensationSum, 2);
		$totalCompensationSum = number_format($totalCompensationSum,2,'.','');
		$totalCompensationSumPerArticle = round($totalCompensationSumPerArticle + $importedArticles[$i]->compensationSumPerArticle, 2);
		$totalCompensationSumPerArticle = number_format($totalCompensationSumPerArticle,2,'.','');

		$totalCompensationSumPerPlantime = round($totalCompensationSumPerPlantime + $importedArticles[$i]->compensationSumPerPlantime, 2);
		$totalCompensationSumPerPlantime = number_format($totalCompensationSumPerPlantime,2,'.','');

      }
      $jsonedArticleIDs = json_encode($articleIDs);

      $importedArticlesPaginated = $paginator->paginate(
          // Doctrine Query, not results
          $importedArticles,
          // Define the page parameter
          $request->query->getInt('page', 1),
          // Items per page
          5
      );

      //serializing will remove "custom attributes" like "->index" above
      $jsonContent = $serializer->serialize($importedArticles, 'json');
      $unJsonContent = json_decode($jsonContent, true);
      foreach ($unJsonContent as $i => $product) {
        $sql = "SELECT property_id FROM imported_article WHERE id = '".$unJsonContent[$i]['id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyIdResults = $stmt->fetch();
        $sql = "SELECT address, identifier FROM property WHERE id = '".$propertyIdResults['property_id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyAddressAndIdentifierResults = $stmt->fetch();
        $unJsonContent[$i]['propertyId'] = $propertyIdResults['property_id'];
        $unJsonContent[$i]['propertyAddress'] = $propertyAddressAndIdentifierResults['address'];
        $unJsonContent[$i]['propertyIdentifier'] = $propertyAddressAndIdentifierResults['identifier'];
      }
      $jsonContent = $serializer->serialize($unJsonContent, 'json');

	  $propertiesArray = array('test1', 'test2');

      return $this->render('functions/'.$locale.'.showimportedarticles-readyplan.html.twig',array(
		  'properties' => $propertiesArray,
		  'cost' => $cost,
		  'topic' => $topic,
          'owner' => $owner,
		  'result1Total' => $result1Total,
		  'result2Total' => $result2Total,
		  'result4Total' => $result4Total,
          'jsonedArticleIDs' => $jsonedArticleIDs,
          'importedArticles' => $importedArticles,
          'removedArticles' => $removedArticles,
          'importedArticlesPaginated' => $importedArticlesPaginated,
          'importedArticlesJsoned' => $jsonContent,
		  'totalArea' => $totalArea,
		  'totalTotalGrowth' => $totalTotalGrowth,
		  'totalResultAdditionHa' => $totalResultAdditionHa,
		  'totalCompensationSum' => $totalCompensationSum,
		  'totalCompensationSumPerArticle' => $totalCompensationSumPerArticle,
		  'totalCompensationSumPerPlantime' => $totalCompensationSumPerPlantime));

  }

  /**
   * @Route("/{locale}/functions/importedarticles/getxml/owner/{owner}", methods={"GET"})
   */
  public function showImportedArticlesXml(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $encoders = [new XmlEncoder(), new JsonEncoder()];
      $normalizers = [new ObjectNormalizer()];
      $serializer = new Serializer($normalizers, $encoders);
      //$allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));
      $allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));

      $filterNoTrees = isset($allGetParams['filterNoTrees']);

      //return new JsonResponse(array('message' => $allGetParams['operation']));

      $em = $this->getDoctrine()->getManager();
      $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
      $foundProperties = $propertyService->searchByOwner($owner);

      if (!$foundProperties) {
        return new JsonResponse(array('message' => 'No properties for the owner!'));
      }

      $user = $security->getUser();
      if (!$user) {
        return $this->redirect('/');
      }
      if ($security->getUser()->getRole() !== 'super-user') {
        return new JsonResponse(array('message' => 'No permission!'));
      }

      $propertyObjects = array();
      foreach ($foundProperties as $i => $product) {
        $property = $em->getRepository(Property::class)->find($foundProperties[$i]['id']);
        if ($property) {
          array_push($propertyObjects, $property);
        }
      }

      $importedArticles = array();
      foreach ($propertyObjects as $i => $product) {
        foreach ($propertyObjects[$i]->getImportedArticles() as $k => $product) {
          $propertyObjects[$i]->getImportedArticles()[$k]->propertyName = $propertyObjects[$i]->getAddress();
		  $propertyObjects[$i]->getImportedArticles()[$k]->propertyIdentifier = $propertyObjects[$i]->getIdentifier();
          array_push($importedArticles, $propertyObjects[$i]->getImportedArticles()[$k]);
        }
      }

      foreach ($importedArticles as $i => $product) {
        $importedArticles[$i]->setProperty(null);
        $importedArticles[$i]->dateFi = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->dateEn = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->noTreeData = false;
        $importedArticles[$i]->propertyId = $property->getId();

        if ($importedArticles[$i]->getMantyAge() < 1
            && $importedArticles[$i]->getKuusiAge() < 1
            && $importedArticles[$i]->getRauduskoivuAge() < 1
            && $importedArticles[$i]->getHieskoivuAge() < 1
            && $importedArticles[$i]->getHaapaAge() < 1
            && $importedArticles[$i]->getHarmaaleppaAge() < 1
            && $importedArticles[$i]->getTervaleppaAge() < 1
            && $importedArticles[$i]->getLehtipuuAge() < 1
            && $importedArticles[$i]->getHavupuuAge() < 1) {
              $importedArticles[$i]->noTreeData = true;
        }
      }

      if ($filterNoTrees) {
        $importedArticlesWithoutTreeless = array();
        foreach ($importedArticles as $i => $product) {
          if (!$importedArticles[$i]->noTreeData) {
            array_push($importedArticlesWithoutTreeless, $importedArticles[$i]);
          }
        }
        $importedArticles = $importedArticlesWithoutTreeless;
      }

      if (isset($allGetParams['operation'])) {
        $operation = $allGetParams['operation'];
        $filteredByOperation = array();
          foreach ($importedArticles as $i => $product) {
            if ($operation == 'afforestation') {
              if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
              && ($importedArticles[$i]->getDevelopmentclass() == 'A0 - Aukea')) {
              array_push($filteredByOperation, $importedArticles[$i]);
              }
            }
          if ($operation == 'fertilization') {
            if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
            && ($importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '03 - Varttunut kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '04 - Uudistuskypsä metsikkö' )) {
            array_push($filteredByOperation, $importedArticles[$i]);
            }
          }
          }
          $importedArticles = $filteredByOperation;
      }

      $removedArticles = array();
      $selected = isset($allGetParams['sel']);
      if ($selected) {
        $selectedArray = $allGetParams['sel'];
        $filteredArticles = array();
        foreach ($selectedArray as $i => $product) {
          foreach ($importedArticles as $k => $product) {
            if ($selectedArray[$i] == $importedArticles[$k]->getId()) {
              array_push($filteredArticles, $importedArticles[$k]); //push to an empty array
              unset($importedArticles[$k]); //remove from the original array
            }
          }
        }
        $removedArticles = $importedArticles; //this becomes the original array
        $importedArticles = $filteredArticles; //old name for the new array
      }


      $index = 1;
      $articleIDs = array();
      foreach ($importedArticles as $i => $product) {
        array_push($articleIDs, $importedArticles[$i]->getId());
        $importedArticles[$i]->index = $index;
        $index++;
      }
      $jsonedArticleIDs = json_encode($articleIDs);

      $importedArticlesPaginated = $paginator->paginate(
          // Doctrine Query, not results
          $importedArticles,
          // Define the page parameter
          $request->query->getInt('page', 1),
          // Items per page
          5
      );

      //serializing will remove "custom attributes" like "->index" above
      $jsonContent = $serializer->serialize($importedArticles, 'json');
      $unJsonContent = json_decode($jsonContent, true);
      foreach ($unJsonContent as $i => $product) {
        $sql = "SELECT property_id FROM imported_article WHERE id = '".$unJsonContent[$i]['id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyIdResults = $stmt->fetch();
        $sql = "SELECT address, identifier FROM property WHERE id = '".$propertyIdResults['property_id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyAddressAndIdentifierResults = $stmt->fetch();
        $unJsonContent[$i]['propertyId'] = $propertyIdResults['property_id'];
        $unJsonContent[$i]['propertyAddress'] = $propertyAddressAndIdentifierResults['address'];
        $unJsonContent[$i]['propertyIdentifier'] = $propertyAddressAndIdentifierResults['identifier'];
      }
      $jsonContent = $serializer->serialize($unJsonContent, 'json');

      return $this->render('functions/'.$locale.'.showimportedarticles-getxml.html.twig',array(
          'owner' => $owner,
          'jsonedArticleIDs' => $jsonedArticleIDs,
          'importedArticles' => $importedArticles,
          'removedArticles' => $removedArticles,
          'importedArticlesPaginated' => $importedArticlesPaginated,
          'importedArticlesJsoned' => $jsonContent));
  }

  /**
   * @Route("/{locale}/functions/importedarticles/getxml/owner/{owner}", methods={"POST"})
   */
  public function showImportedArticlesXmlPost(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $allPostParams = $request->request->all();
      $importedArticlesJsoned = $allPostParams['importedArticlesJsoned'];

      //return new Response($importedArticlesJsoned, 200, [
      //'Content-Type' => 'application/json',
      //]);

      $importedArticlesArray = json_decode($importedArticlesJsoned, true);
      //return new JsonResponse(array('message' => $importedArticlesArray));

      $XMLRoot = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><Report></Report>');
      $XMLRoot->addAttribute('xsi:xsi:schemaLocation', 'ListOfCompartments http://10.100.182.146/ReportServer?%2FBitAppsForestKIT%2FBitForest%2FCompartmentReport%2FListOfCompartments&amp;rs%3ACommand=Render&amp;rs%3AFormat=XML&amp;rs%3ASessionID=snfzysjd23qwlem3aliorg45&amp;rc%3ASchema=True');
      $XMLRoot->addAttribute('Name', 'ListOfCompartments');
      $XMLRoot->addAttribute('Textbox16', '01.01.2020');
      $XMLRoot->addAttribute('Textbox18', 'Kuvioluettelo');
      $XMLRoot->addAttribute('xmlns:xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      $XMLRoot->addAttribute('xmlns', 'ListOfCompartments');

      $Tablix2 = $XMLRoot->addChild('Tablix2');
      $Details_Collection = $Tablix2->addChild('Details_Collection');
      $Details = $Details_Collection->addChild('Details');

      $table1 = $XMLRoot->addChild('table1');
      $table1->addAttribute('textbox17', 'Puustotiedot');
      $table1->addAttribute('Textbox25', 'Kuvio');
      $table1->addAttribute('Textbox26', 'Pinta-'."\r\n".'ala, ha');
      $table1->addAttribute('Textbox27', 'Kiinteistön nimi ja tunnus, pääryhmä, kasvupaikka, maalaji, kehitysluokka, saavutettavuus ja metsikön laatu');
      $table1->addAttribute('textbox14', 'puulaji');
      $table1->addAttribute('textbox15', 'ikä,'."\r\n".'v');
      $table1->addAttribute('Textbox290', 'tilavuus'."\r\n".'m³/kuvio   m³/ha');
      $table1->addAttribute('Textbox298', 'tukkia, m³/ha');
      $table1->addAttribute('Textbox299', 'kuitua,'."\r\n".'m³/ha');
      $table1->addAttribute('Textbox300', 'läpimitta,'."\r\n".'cm');
      $table1->addAttribute('Textbox301', 'pituus,'."\r\n".'m');
      $table1->addAttribute('Textbox302', 'runkoluku,'."\r\n".'kpl/ha');
      $table1->addAttribute('Textbox303', 'ppa, m²/ha');
      $table1->addAttribute('Textbox304', 'kasvu'."\r\n".'m³/ha/v');
      $table1->addAttribute('Textbox29', 'Toimenpiteet'."\r\n".'kuviolla');

      $Detail_Collection = $table1->addChild('Detail_Collection');

      foreach ($importedArticlesArray as $i => $product) {

        $Detail = $Detail_Collection->addChild('Detail');
        $siteClass = $importedArticlesArray[$i]['propertyAddress'].' '.
          $importedArticlesArray[$i]['propertyIdentifier']."\r\n".
          $importedArticlesArray[$i]['mainclass']."\r\n".
          $importedArticlesArray[$i]['growplace']."\r\n".
          $importedArticlesArray[$i]['accessibility']."\r\n".
          $importedArticlesArray[$i]['quality']."\r\n";
        $Detail->addAttribute('compNo', '1');
        $Detail->addAttribute('Area', $importedArticlesArray[$i]['area']);
        $Detail->addAttribute('siteClass', $siteClass);
        $Detail->addAttribute('textbox8', 'Erityispiirteet'."\r\n");
        $Detail->addAttribute('Textbox10', 'Hoitoluokka'."\r\n");
        $Detail->addAttribute('textbox9', 'Lisätiedot'."\r\n");
        $Subreport3 = $Detail->addChild('Subreport3');
        $Report = $Subreport3->addChild('Report');
        $Report->addAttribute('Name', 'ListOfCompartmentsStandInfo');
        $table1 = $Report->addChild('table1');
        $table1->addAttribute('Textbox1', 'Yhteensä');
        $table1->addAttribute('Textbox5', '111.22404371584699453551912568');
        $table1->addAttribute('Textbox6', '200.200000');
        $table1->addAttribute('Textbox13', '144.10');
        $table1->addAttribute('Textbox14', '70.500000000');
        $table1->addAttribute('Textbox15', '71.200000000');
        $table1->addAttribute('Textbox16', '21.444262295081967213114754098');
        $table1->addAttribute('Textbox17', '16.20928961748633879781420765');
        $table1->addAttribute('Textbox18', '627');
        $table1->addAttribute('Textbox19', '18.3');
        $table1->addAttribute('Textbox20', '2.20');
        $Detail_Collection2 = $table1->addChild('Detail_Collection');
        if ($importedArticlesArray[$i]['mantyAge'] > 0) {
          $mantyDetail = $Detail_Collection2->addChild('Detail');
          $mantyDetail->addAttribute('treeSpec', 'Mänty');
          $mantyDetail->addAttribute('age', $importedArticlesArray[$i]['mantyAge']);
          $mantyDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['mantyVolume']);
          $mantyDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['mantyVolumeHa']);
          $mantyDetail->addAttribute('logVolume', $importedArticlesArray[$i]['mantyLogVolume']);
          $mantyDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['mantyPulpVolume']);
          $mantyDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['mantyDiameter']);
          $mantyDetail->addAttribute('averageLength', $importedArticlesArray[$i]['mantyLength']);
          $mantyDetail->addAttribute('density', $importedArticlesArray[$i]['mantyDensity']);
          $mantyDetail->addAttribute('basalArea', $importedArticlesArray[$i]['mantyBasalarea']);
          $mantyDetail->addAttribute('growth', $importedArticlesArray[$i]['mantyGrowth']);
        }
        if ($importedArticlesArray[$i]['kuusiAge'] > 0) {
          $kuusiDetail = $Detail_Collection2->addChild('Detail');
          $kuusiDetail->addAttribute('treeSpec', 'Kuusi');
          $kuusiDetail->addAttribute('age', $importedArticlesArray[$i]['mantyAge']);
          $kuusiDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['kuusiVolume']);
          $kuusiDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['kuusiVolumeHa']);
          $kuusiDetail->addAttribute('logVolume', $importedArticlesArray[$i]['kuusiLogVolume']);
          $kuusiDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['kuusiPulpVolume']);
          $kuusiDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['kuusiDiameter']);
          $kuusiDetail->addAttribute('averageLength', $importedArticlesArray[$i]['kuusiLength']);
          $kuusiDetail->addAttribute('density', $importedArticlesArray[$i]['kuusiDensity']);
          $kuusiDetail->addAttribute('basalArea', $importedArticlesArray[$i]['kuusiBasalarea']);
          $kuusiDetail->addAttribute('growth', $importedArticlesArray[$i]['kuusiGrowth']);
        }
        if ($importedArticlesArray[$i]['rauduskoivuAge'] > 0) {
          $rauduskoivuDetail = $Detail_Collection2->addChild('Detail');
          $rauduskoivuDetail->addAttribute('treeSpec', 'Rauduskoivu');
          $rauduskoivuDetail->addAttribute('age', $importedArticlesArray[$i]['rauduskoivuAge']);
          $rauduskoivuDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['rauduskoivuVolume']);
          $rauduskoivuDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['rauduskoivuVolumeHa']);
          $rauduskoivuDetail->addAttribute('logVolume', $importedArticlesArray[$i]['rauduskoivuLogVolume']);
          $rauduskoivuDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['rauduskoivuPulpVolume']);
          $rauduskoivuDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['rauduskoivuDiameter']);
          $rauduskoivuDetail->addAttribute('averageLength', $importedArticlesArray[$i]['rauduskoivuLength']);
          $rauduskoivuDetail->addAttribute('density', $importedArticlesArray[$i]['rauduskoivuDensity']);
          $rauduskoivuDetail->addAttribute('basalArea', $importedArticlesArray[$i]['rauduskoivuBasalarea']);
          $rauduskoivuDetail->addAttribute('growth', $importedArticlesArray[$i]['rauduskoivuGrowth']);
        }
        if ($importedArticlesArray[$i]['hieskoivuAge'] > 0) {
          $rauduskoivuDetail = $Detail_Collection2->addChild('Detail');
          $rauduskoivuDetail->addAttribute('treeSpec', 'Hieskoivu');
          $rauduskoivuDetail->addAttribute('age', $importedArticlesArray[$i]['hieskoivuAge']);
          $rauduskoivuDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['hieskoivuVolume']);
          $rauduskoivuDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['hieskoivuVolumeHa']);
          $rauduskoivuDetail->addAttribute('logVolume', $importedArticlesArray[$i]['hieskoivuLogVolume']);
          $rauduskoivuDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['hieskoivuPulpVolume']);
          $rauduskoivuDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['hieskoivuDiameter']);
          $rauduskoivuDetail->addAttribute('averageLength', $importedArticlesArray[$i]['hieskoivuLength']);
          $rauduskoivuDetail->addAttribute('density', $importedArticlesArray[$i]['hieskoivuDensity']);
          $rauduskoivuDetail->addAttribute('basalArea', $importedArticlesArray[$i]['hieskoivuBasalarea']);
          $rauduskoivuDetail->addAttribute('growth', $importedArticlesArray[$i]['hieskoivuGrowth']);
        }
        if ($importedArticlesArray[$i]['haapaAge'] > 0) {
          $haapaDetail = $Detail_Collection2->addChild('Detail');
          $haapaDetail->addAttribute('treeSpec', 'Haapa');
          $haapaDetail->addAttribute('age', $importedArticlesArray[$i]['haapaAge']);
          $haapaDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['haapaVolume']);
          $haapaDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['haapaVolumeHa']);
          $haapaDetail->addAttribute('logVolume', $importedArticlesArray[$i]['haapaLogVolume']);
          $haapaDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['haapaPulpVolume']);
          $haapaDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['haapaDiameter']);
          $haapaDetail->addAttribute('averageLength', $importedArticlesArray[$i]['haapaLength']);
          $haapaDetail->addAttribute('density', $importedArticlesArray[$i]['haapaDensity']);
          $haapaDetail->addAttribute('basalArea', $importedArticlesArray[$i]['haapaBasalarea']);
          $haapaDetail->addAttribute('growth', $importedArticlesArray[$i]['haapaGrowth']);
        }
        if ($importedArticlesArray[$i]['harmaaleppaAge'] > 0) {
          $harmaaleppaDetail = $Detail_Collection2->addChild('Detail');
          $harmaaleppaDetail->addAttribute('treeSpec', 'Harmaaleppa');
          $harmaaleppaDetail->addAttribute('age', $importedArticlesArray[$i]['harmaaleppaAge']);
          $harmaaleppaDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['harmaaleppaVolume']);
          $harmaaleppaDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['harmaaleppaVolumeHa']);
          $harmaaleppaDetail->addAttribute('logVolume', $importedArticlesArray[$i]['harmaaleppaLogVolume']);
          $harmaaleppaDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['harmaaleppaPulpVolume']);
          $harmaaleppaDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['harmaaleppaDiameter']);
          $harmaaleppaDetail->addAttribute('averageLength', $importedArticlesArray[$i]['harmaaleppaLength']);
          $harmaaleppaDetail->addAttribute('density', $importedArticlesArray[$i]['harmaaleppaDensity']);
          $harmaaleppaDetail->addAttribute('basalArea', $importedArticlesArray[$i]['harmaaleppaBasalarea']);
          $harmaaleppaDetail->addAttribute('growth', $importedArticlesArray[$i]['harmaaleppaGrowth']);
        }
        if ($importedArticlesArray[$i]['tervaleppaAge'] > 0) {
          $tervaleppaDetail = $Detail_Collection2->addChild('Detail');
          $tervaleppaDetail->addAttribute('treeSpec', 'Tervaleppa');
          $tervaleppaDetail->addAttribute('age', $importedArticlesArray[$i]['tervaleppaAge']);
          $tervaleppaDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['tervaleppaVolume']);
          $tervaleppaDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['tervaleppaVolumeHa']);
          $tervaleppaDetail->addAttribute('logVolume', $importedArticlesArray[$i]['tervaleppaLogVolume']);
          $tervaleppaDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['tervaleppaPulpVolume']);
          $tervaleppaDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['tervaleppaDiameter']);
          $tervaleppaDetail->addAttribute('averageLength', $importedArticlesArray[$i]['tervaleppaLength']);
          $tervaleppaDetail->addAttribute('density', $importedArticlesArray[$i]['tervaleppaDensity']);
          $tervaleppaDetail->addAttribute('basalArea', $importedArticlesArray[$i]['tervaleppaBasalarea']);
          $tervaleppaDetail->addAttribute('growth', $importedArticlesArray[$i]['tervaleppaGrowth']);
        }
        if ($importedArticlesArray[$i]['lehtipuuAge'] > 0) {
          $lehtipuuDetail = $Detail_Collection2->addChild('Detail');
          $lehtipuuDetail->addAttribute('treeSpec', 'Lehtipuu');
          $lehtipuuDetail->addAttribute('age', $importedArticlesArray[$i]['lehtipuuAge']);
          $lehtipuuDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['lehtipuuVolume']);
          $lehtipuuDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['lehtipuuVolumeHa']);
          $lehtipuuDetail->addAttribute('logVolume', $importedArticlesArray[$i]['lehtipuuLogVolume']);
          $lehtipuuDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['lehtipuuPulpVolume']);
          $lehtipuuDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['lehtipuuDiameter']);
          $lehtipuuDetail->addAttribute('averageLength', $importedArticlesArray[$i]['lehtipuuLength']);
          $lehtipuuDetail->addAttribute('density', $importedArticlesArray[$i]['lehtipuuDensity']);
          $lehtipuuDetail->addAttribute('basalArea', $importedArticlesArray[$i]['lehtipuuBasalarea']);
          $lehtipuuDetail->addAttribute('growth', $importedArticlesArray[$i]['lehtipuuGrowth']);
        }
        if ($importedArticlesArray[$i]['havupuuAge'] > 0) {
          $havupuuDetail = $Detail_Collection2->addChild('Detail');
          $havupuuDetail->addAttribute('treeSpec', 'Havupuu');
          $havupuuDetail->addAttribute('age', $importedArticlesArray[$i]['havupuuAge']);
          $havupuuDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['havupuuVolume']);
          $havupuuDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['havupuuVolumeHa']);
          $havupuuDetail->addAttribute('logVolume', $importedArticlesArray[$i]['havupuuLogVolume']);
          $havupuuDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['havupuuPulpVolume']);
          $havupuuDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['havupuuDiameter']);
          $havupuuDetail->addAttribute('averageLength', $importedArticlesArray[$i]['havupuuLength']);
          $havupuuDetail->addAttribute('density', $importedArticlesArray[$i]['havupuuDensity']);
          $havupuuDetail->addAttribute('basalArea', $importedArticlesArray[$i]['havupuuBasalarea']);
          $havupuuDetail->addAttribute('growth', $importedArticlesArray[$i]['havupuuGrowth']);
        }
      }

      //echo $XMLRoot->asXML();
/*    No file
      $response = new Response($XMLRoot->asXML());
      $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
      return $response;
*/

/*    Older way to return the file
      $response = new Response($XMLRoot->asXML());
      $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
      $response->headers->set('Content-Disposition', 'attachment; filename="'.$owner.' - Valitut kuviot.xml"');
      if ($response) {
        return $response;
      }
*/
      $dom_sxe = dom_import_simplexml($XMLRoot);
      $dom = new \DOMDocument('1.0');
      $dom_sxe = $dom->importNode($dom_sxe, true);
      $dom_sxe = $dom->appendChild($dom_sxe);
      $dom->formatOutput = true;
      $dom->encoding = 'utf-8';

      $response = new Response($dom->saveXML());
      $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
      $response->headers->set('Content-Disposition', 'attachment; filename="'.$owner.' - Valitut kuviot.xml"');
      if ($response) {
        return $response;
      }

  }

  /**
   * @Route("/{locale}/functions/importedarticles/createplan/owner/{owner}", methods={"GET"})
   */
   //useless?
  public function createPlan(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    $encoders = [new XmlEncoder(), new JsonEncoder()];
    $normalizers = [new ObjectNormalizer()];
    $serializer = new Serializer($normalizers, $encoders);
    //$allGetParams = $request->query->all();
    //return new JsonResponse(array('message' => $allGetParams));
    $allGetParams = $request->query->all();
    //return new JsonResponse(array('message' => $allGetParams));

    $filterNoTrees = isset($allGetParams['filterNoTrees']);

    //return new JsonResponse(array('message' => $allGetParams['operation']));

    $em = $this->getDoctrine()->getManager();
    $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
    $foundProperties = $propertyService->searchByOwner($owner);

    if (!$foundProperties) {
      return new JsonResponse(array('message' => 'No properties for the owner!'));
    }

    $user = $security->getUser();
    if (!$user) {
      return $this->redirect('/');
    }
    if ($security->getUser()->getRole() !== 'super-user') {
      return new JsonResponse(array('message' => 'No permission!'));
    }

    $propertyObjects = array();
    foreach ($foundProperties as $i => $product) {
      $property = $em->getRepository(Property::class)->find($foundProperties[$i]['id']);
      if ($property) {
        array_push($propertyObjects, $property);
      }
    }

    $importedArticles = array();
    foreach ($propertyObjects as $i => $product) {
      foreach ($propertyObjects[$i]->getImportedArticles() as $k => $product) {
        $propertyObjects[$i]->getImportedArticles()[$k]->propertyName = $propertyObjects[$i]->getAddress();
		$propertyObjects[$i]->getImportedArticles()[$k]->propertyIdentifier = $propertyObjects[$i]->getIdentifier();
        array_push($importedArticles, $propertyObjects[$i]->getImportedArticles()[$k]);
      }
    }

    foreach ($importedArticles as $i => $product) {
      $importedArticles[$i]->setProperty(null);
      $importedArticles[$i]->dateFi = date('m/Y', $importedArticles[$i]->getAdded());
      $importedArticles[$i]->dateEn = date('m/Y', $importedArticles[$i]->getAdded());
      $importedArticles[$i]->noTreeData = false;
      $importedArticles[$i]->propertyId = $property->getId();

      if ($importedArticles[$i]->getMantyAge() < 1
          && $importedArticles[$i]->getKuusiAge() < 1
          && $importedArticles[$i]->getRauduskoivuAge() < 1
          && $importedArticles[$i]->getHieskoivuAge() < 1
          && $importedArticles[$i]->getHaapaAge() < 1
          && $importedArticles[$i]->getHarmaaleppaAge() < 1
          && $importedArticles[$i]->getTervaleppaAge() < 1
          && $importedArticles[$i]->getLehtipuuAge() < 1
          && $importedArticles[$i]->getHavupuuAge() < 1) {
            $importedArticles[$i]->noTreeData = true;
      }
    }

    if ($filterNoTrees) {
      $importedArticlesWithoutTreeless = array();
      foreach ($importedArticles as $i => $product) {
        if (!$importedArticles[$i]->noTreeData) {
          array_push($importedArticlesWithoutTreeless, $importedArticles[$i]);
        }
      }
      $importedArticles = $importedArticlesWithoutTreeless;
    }

    if (isset($allGetParams['operation'])) {
      $operation = $allGetParams['operation'];
      $filteredByOperation = array();
        foreach ($importedArticles as $i => $product) {
          if ($operation == 'afforestation') {
            if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
            && ($importedArticles[$i]->getDevelopmentclass() == 'A0 - Aukea')) {
            array_push($filteredByOperation, $importedArticles[$i]);
            }
          }
        if ($operation == 'fertilization') {
          if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
          && ($importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
            || $importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
            || $importedArticles[$i]->getDevelopmentclass() == '03 - Varttunut kasvatusmetsikkö'
            || $importedArticles[$i]->getDevelopmentclass() == '04 - Uudistuskypsä metsikkö' )) {
          array_push($filteredByOperation, $importedArticles[$i]);
          }
        }
        }
        $importedArticles = $filteredByOperation;
    }

    $removedArticles = array();
    $selected = isset($allGetParams['sel']);
    if ($selected) {
      $selectedArray = $allGetParams['sel'];
      $filteredArticles = array();
      foreach ($selectedArray as $i => $product) {
        foreach ($importedArticles as $k => $product) {
          if ($selectedArray[$i] == $importedArticles[$k]->getId()) {
            array_push($filteredArticles, $importedArticles[$k]); //push to an empty array
            unset($importedArticles[$k]); //remove from the original array
          }
        }
      }
      $removedArticles = $importedArticles; //this becomes the original array
      $importedArticles = $filteredArticles; //old name for the new array
    }


    $index = 1;
    $articleIDs = array();
    foreach ($importedArticles as $i => $product) {
      array_push($articleIDs, $importedArticles[$i]->getId());
      $importedArticles[$i]->index = $index;
      $index++;
    }
    $jsonedArticleIDs = json_encode($articleIDs);

    $importedArticlesPaginated = $paginator->paginate(
        // Doctrine Query, not results
        $importedArticles,
        // Define the page parameter
        $request->query->getInt('page', 1),
        // Items per page
        15
    );

    //serializing will remove "custom attributes" like "->index" above
    $jsonContent = $serializer->serialize($importedArticles, 'json');
    $unJsonContent = json_decode($jsonContent, true);
    foreach ($unJsonContent as $i => $product) {
      $sql = "SELECT property_id FROM imported_article WHERE id = '".$unJsonContent[$i]['id']."'";
      $stmt = $em->getConnection()->prepare($sql);
      $stmt->execute();
      $propertyIdResults = $stmt->fetch();
      $sql = "SELECT address, identifier FROM property WHERE id = '".$propertyIdResults['property_id']."'";
      $stmt = $em->getConnection()->prepare($sql);
      $stmt->execute();
      $propertyAddressAndIdentifierResults = $stmt->fetch();
      $unJsonContent[$i]['propertyId'] = $propertyIdResults['property_id'];
      $unJsonContent[$i]['propertyAddress'] = $propertyAddressAndIdentifierResults['address'];
      $unJsonContent[$i]['propertyIdentifier'] = $propertyAddressAndIdentifierResults['identifier'];
    }
    $jsonContent = $serializer->serialize($unJsonContent, 'json');

    return $this->render('functions/'.$locale.'.showimportedarticles-createplan.html.twig',array(
        'owner' => $owner,
        'jsonedArticleIDs' => $jsonedArticleIDs,
        'importedArticles' => $importedArticles,
        'removedArticles' => $removedArticles,
        'importedArticlesPaginated' => $importedArticlesPaginated,
        'importedArticlesJsoned' => $jsonContent));
  }

  /**
   * @Route("/{locale}/functions/importedarticles/createplan/owner/{owner}", methods={"POST"})
   */
  public function createPlanPost(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $encoders = [new XmlEncoder(), new JsonEncoder()];
      $normalizers = [new ObjectNormalizer()];
      $serializer = new Serializer($normalizers, $encoders);
      //$allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));
      $allGetParams = $request->query->all();
      //return new JsonResponse(array('message' => $allGetParams));

      $filterNoTrees = isset($allGetParams['filterNoTrees']);

      //return new JsonResponse(array('message' => $allGetParams['operation']));

      $em = $this->getDoctrine()->getManager();
      $propertyService = new PropertyService($this->getDoctrine()->getManager(), Property::class);
      $foundProperties = $propertyService->searchByOwner($owner);

      if (!$foundProperties) {
        return new JsonResponse(array('message' => 'No properties for the owner!'));
      }

      $user = $security->getUser();
      if (!$user) {
        return $this->redirect('/');
      }
      if ($security->getUser()->getRole() !== 'super-user') {
        return new JsonResponse(array('message' => 'No permission!'));
      }

      $propertyObjects = array();
      foreach ($foundProperties as $i => $product) {
        $property = $em->getRepository(Property::class)->find($foundProperties[$i]['id']);
        if ($property) {
          array_push($propertyObjects, $property);
        }
      }

      $importedArticles = array();
      foreach ($propertyObjects as $i => $product) {
        foreach ($propertyObjects[$i]->getImportedArticles() as $k => $product) {
          $propertyObjects[$i]->getImportedArticles()[$k]->propertyName = $propertyObjects[$i]->getAddress();
		  $propertyObjects[$i]->getImportedArticles()[$k]->propertyIdentifier = $propertyObjects[$i]->getIdentifier();
          array_push($importedArticles, $propertyObjects[$i]->getImportedArticles()[$k]);
        }
      }

      foreach ($importedArticles as $i => $product) {
        $importedArticles[$i]->setProperty(null);
        $importedArticles[$i]->dateFi = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->dateEn = date('m/Y', $importedArticles[$i]->getAdded());
        $importedArticles[$i]->noTreeData = false;
        $importedArticles[$i]->propertyId = $property->getId();

        if ($importedArticles[$i]->getMantyAge() < 1
            && $importedArticles[$i]->getKuusiAge() < 1
            && $importedArticles[$i]->getRauduskoivuAge() < 1
            && $importedArticles[$i]->getHieskoivuAge() < 1
            && $importedArticles[$i]->getHaapaAge() < 1
            && $importedArticles[$i]->getHarmaaleppaAge() < 1
            && $importedArticles[$i]->getTervaleppaAge() < 1
            && $importedArticles[$i]->getLehtipuuAge() < 1
            && $importedArticles[$i]->getHavupuuAge() < 1) {
              $importedArticles[$i]->noTreeData = true;
        }
      }

      if ($filterNoTrees) {
        $importedArticlesWithoutTreeless = array();
        foreach ($importedArticles as $i => $product) {
          if (!$importedArticles[$i]->noTreeData) {
            array_push($importedArticlesWithoutTreeless, $importedArticles[$i]);
          }
        }
        $importedArticles = $importedArticlesWithoutTreeless;
      }

      if (isset($allGetParams['operation'])) {
        $operation = $allGetParams['operation'];
        $filteredByOperation = array();
          foreach ($importedArticles as $i => $product) {
            if ($operation == 'afforestation') {
              if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
              && ($importedArticles[$i]->getDevelopmentclass() == 'A0 - Aukea')) {
              array_push($filteredByOperation, $importedArticles[$i]);
              }
            }
          if ($operation == 'fertilization') {
            if (($importedArticles[$i]->getMainclass() == 'Metsämaa')
            && ($importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '02 - Nuori kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '03 - Varttunut kasvatusmetsikkö'
              || $importedArticles[$i]->getDevelopmentclass() == '04 - Uudistuskypsä metsikkö' )) {
            array_push($filteredByOperation, $importedArticles[$i]);
            }
          }
          }
          $importedArticles = $filteredByOperation;
      }

      $removedArticles = array();
      $selected = isset($allGetParams['sel']);
      if ($selected) {
        $selectedArray = $allGetParams['sel'];
        $filteredArticles = array();
        foreach ($selectedArray as $i => $product) {
          foreach ($importedArticles as $k => $product) {
            if ($selectedArray[$i] == $importedArticles[$k]->getId()) {
              array_push($filteredArticles, $importedArticles[$k]); //push to an empty array
              unset($importedArticles[$k]); //remove from the original array
            }
          }
        }
        $removedArticles = $importedArticles; //this becomes the original array
        $importedArticles = $filteredArticles; //old name for the new array
      }


      $index = 1;
      $articleIDs = array();
      foreach ($importedArticles as $i => $product) {
        array_push($articleIDs, $importedArticles[$i]->getId());
        $importedArticles[$i]->index = $index;
        $index++;
      }
      $jsonedArticleIDs = json_encode($articleIDs);

      $importedArticlesPaginated = $paginator->paginate(
          // Doctrine Query, not results
          $importedArticles,
          // Define the page parameter
          $request->query->getInt('page', 1),
          // Items per page
          15
      );

      //serializing will remove "custom attributes" like "->index" above
      $jsonContent = $serializer->serialize($importedArticles, 'json');
      $unJsonContent = json_decode($jsonContent, true);
      foreach ($unJsonContent as $i => $product) {
        $sql = "SELECT property_id FROM imported_article WHERE id = '".$unJsonContent[$i]['id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyIdResults = $stmt->fetch();
        $sql = "SELECT address, identifier FROM property WHERE id = '".$propertyIdResults['property_id']."'";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $propertyAddressAndIdentifierResults = $stmt->fetch();
        $unJsonContent[$i]['propertyId'] = $propertyIdResults['property_id'];
        $unJsonContent[$i]['propertyAddress'] = $propertyAddressAndIdentifierResults['address'];
        $unJsonContent[$i]['propertyIdentifier'] = $propertyAddressAndIdentifierResults['identifier'];
      }
      $jsonContent = $serializer->serialize($unJsonContent, 'json');

      return $this->render('functions/'.$locale.'.showimportedarticles-showplan.html.twig',array(
          'owner' => $owner,
          'jsonedArticleIDs' => $jsonedArticleIDs,
          'importedArticles' => $importedArticles,
          'removedArticles' => $removedArticles,
          'importedArticlesPaginated' => $importedArticlesPaginated,
          'importedArticlesJsoned' => $jsonContent));

  }

  /**
   * @Route("/{locale}/functions/removepropertiesandarticles", methods={"GET"})
   */
  public function removeAction(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    $em = $this->getDoctrine()->getManager();

    $sql = "DELETE FROM article";
    $stmt = $em->getConnection()->prepare($sql);
    $stmt->execute();

    $sql = "DELETE FROM imported_article";
    $stmt = $em->getConnection()->prepare($sql);
    $stmt->execute();

    $sql = "DELETE FROM property";
    $stmt = $em->getConnection()->prepare($sql);
    $stmt->execute();

	$sql = "ALTER TABLE article AUTO_INCREMENT = 1;";
    $stmt = $em->getConnection()->prepare($sql);
    $stmt->execute();

	$sql = "ALTER TABLE property AUTO_INCREMENT = 1;";
    $stmt = $em->getConnection()->prepare($sql);
    $stmt->execute();

	$sql = "ALTER TABLE imported_article AUTO_INCREMENT = 1;";
    $stmt = $em->getConnection()->prepare($sql);
    $stmt->execute();

    return new JsonResponse(array('message' => 'Done'));
  }

  /**
   * @Route("/{locale}/functions/checkboxtest", methods={"GET"})
   */
  public function checkboxTestAction(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    return $this->render('functions/'.$locale.'.checkboxtest.html.twig',array(
        ));
  }

  /**
   * @Route("/{locale}/functions/checkboxtest", methods={"POST"})
   */
  public function checkboxTestActionPost(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      return new JsonResponse(array('message' => 'createPlanPost'));

  }

  /**
   * @Route("/{locale}/functions/excel2", methods={"GET"})
   */
  public function excelTestAction2(Security $security, $locale, Request $request) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Kohde');
    $sheet->setCellValue('C1', 'Kasvupaikka');
    $sheet->mergeCells('C1:E1');
    //$sheet->getActiveSheet()->getStyle('C1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_YELLOW);
    $sheet->setCellValue('F1', 'Puusto');
    $sheet->mergeCells('F1:K1');
    $sheet->setCellValue('L1', 'Lannoitukset');
    $sheet->mergeCells('L1:N1');
    $sheet->setCellValue('O1', 'Tuhkalannoitukset');
    $sheet->setCellValue('P1', 'm3/ha/v');
    $sheet->mergeCells('P1:Q1');
    $sheet->setCellValue('R1', '%');
    $sheet->mergeCells('R1:S1');

    $sheet->setCellValue('A2', '1');
    $sheet->setCellValue('B2', 'Kunta, nro');
    $sheet->setCellValue('C2', 'Alaryhmä (kangas=1, korpi=2, räme=3)');
    $sheet->setCellValue('D2', 'Kasvupaikkaluokka (kangasmaat; 1=lehto, 2=OMT, 3=MT, 4=VT, 5=CT, 6=CLt), Turvekangasluokka (ojitetut turvemaat; 2=ruohotkg, 3=mustikkatkg, 4=puolukkatkg, 5=varputkg, 6=jäkälätkg)');
    $sheet->setCellValue('E2', 'Pituusboniteetti-indeksi, H100');
    $sheet->setCellValue('F2', 'Runkoluku, kpl/ha');
    $sheet->setCellValue('G2', 'TIlavuus, m3/ha');
    $sheet->setCellValue('H2', 'Ikä, v');
    $sheet->setCellValue('I2', 'Valtapituus, m');
    $sheet->setCellValue('J2', 'männyn osuus tilavuudesta, 0...1');
    $sheet->setCellValue('K2', 'kuusen osuus tilavuudesta, 0...1');
    $sheet->setCellValue('L2', 'TT_1, ajankohta');
    $sheet->setCellValue('M2', 'TL_1, lann.laji, 1=?, 2=?, 3=tuhka');
    $sheet->setCellValue('N2', 'TM_1, typpimäärä, kgN/ha');
    $sheet->setCellValue('O2', 'Peruslannoitus , v (aika peruslannoituksesta, negatiivinen luku), vain turvemailla');
    $sheet->setCellValue('P2', 'lisäkasvu 5v-jaksolle, m3/ha/v');
    $sheet->setCellValue('Q2', 'lisäkasvu 10v-jaksolle, m3/ha/v');
    $sheet->setCellValue('R2', 'turvemaiden lisäkasvu-% 5v-jaksolle');
    $sheet->setCellValue('S2', 'turvemaiden lisäkasvu-% 10v-jaksolle');

    $sheet->setCellValue('A3', '1');
    $sheet->setCellValue('B3', '20');
    $sheet->setCellValue('C3', '1');
    $sheet->setCellValue('D3', '4');
    $sheet->setCellValue('E3', '24');
    $sheet->setCellValue('F3', '1200');
    $sheet->setCellValue('G3', '140');
    $sheet->setCellValue('H3', '40');
    $sheet->setCellValue('I3', '18');
    $sheet->setCellValue('J3', '0.8');
    $sheet->setCellValue('K3', '0.1');
    $sheet->setCellValue('L3', '0');
    $sheet->setCellValue('M3', '2');
    $sheet->setCellValue('N3', '150');
    $sheet->setCellValue('O3', '10');

    $spreadsheet->getActiveSheet()->setTitle('Lähtötiedot');

    $spreadsheet->createSheet();
    $spreadsheet->setActiveSheetIndex(1);
    $spreadsheet->getActiveSheet()->setTitle('H100<->kasvupaikka');

    $spreadsheet->createSheet();
    $spreadsheet->setActiveSheetIndex(2);
    $spreadsheet->getActiveSheet()->setTitle('Kuntakoodi<->lämpösumma');

    $spreadsheet->createSheet();
    $spreadsheet->setActiveSheetIndex(3);
    $spreadsheet->getActiveSheet()->setTitle('Muuta');

    $spreadsheet->setActiveSheetIndex(0);

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");

    $response =  new StreamedResponse(
        function () use ($writer) {
            $writer->save('php://output');
        }
    );
    $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    $response->headers->set('Content-Disposition', 'attachment;filename="ExportScan.xlsx"');
    $response->headers->set('Cache-Control','max-age=0');
    return $response;

  }

  /**
   * @Route("/{locale}/functions/importedarticles/getexcel1/owner/{owner}", methods={"POST"})
   */
  public function showImportedArticlesExcelPost1(Security $security, $locale, Request $request, PaginatorInterface $paginator, $owner) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $allPostParams = $request->request->all();
      $importedArticlesJsoned = $allPostParams['importedArticlesJsoned'];

      //return new Response($importedArticlesJsoned, 200, [
      //'Content-Type' => 'application/json',
      //]);

      $importedArticlesArray = json_decode($importedArticlesJsoned, true);
      //return new JsonResponse(array('message' => $importedArticlesArray));

      $XMLRoot = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><Report></Report>');
      $XMLRoot->addAttribute('xsi:xsi:schemaLocation', 'ListOfCompartments http://10.100.182.146/ReportServer?%2FBitAppsForestKIT%2FBitForest%2FCompartmentReport%2FListOfCompartments&amp;rs%3ACommand=Render&amp;rs%3AFormat=XML&amp;rs%3ASessionID=snfzysjd23qwlem3aliorg45&amp;rc%3ASchema=True');
      $XMLRoot->addAttribute('Name', 'ListOfCompartments');
      $XMLRoot->addAttribute('Textbox16', '01.01.2020');
      $XMLRoot->addAttribute('Textbox18', 'Kuvioluettelo');
      $XMLRoot->addAttribute('xmlns:xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      $XMLRoot->addAttribute('xmlns', 'ListOfCompartments');

      $Tablix2 = $XMLRoot->addChild('Tablix2');
      $Details_Collection = $Tablix2->addChild('Details_Collection');
      $Details = $Details_Collection->addChild('Details');

      $table1 = $XMLRoot->addChild('table1');
      $table1->addAttribute('textbox17', 'Puustotiedot');
      $table1->addAttribute('Textbox25', 'Kuvio');
      $table1->addAttribute('Textbox26', 'Pinta-'."\r\n".'ala, ha');
      $table1->addAttribute('Textbox27', 'Kiinteistön nimi ja tunnus, pääryhmä, kasvupaikka, maalaji, kehitysluokka, saavutettavuus ja metsikön laatu');
      $table1->addAttribute('textbox14', 'puulaji');
      $table1->addAttribute('textbox15', 'ikä,'."\r\n".'v');
      $table1->addAttribute('Textbox290', 'tilavuus'."\r\n".'m³/kuvio   m³/ha');
      $table1->addAttribute('Textbox298', 'tukkia, m³/ha');
      $table1->addAttribute('Textbox299', 'kuitua,'."\r\n".'m³/ha');
      $table1->addAttribute('Textbox300', 'läpimitta,'."\r\n".'cm');
      $table1->addAttribute('Textbox301', 'pituus,'."\r\n".'m');
      $table1->addAttribute('Textbox302', 'runkoluku,'."\r\n".'kpl/ha');
      $table1->addAttribute('Textbox303', 'ppa, m²/ha');
      $table1->addAttribute('Textbox304', 'kasvu'."\r\n".'m³/ha/v');
      $table1->addAttribute('Textbox29', 'Toimenpiteet'."\r\n".'kuviolla');

      $Detail_Collection = $table1->addChild('Detail_Collection');

      foreach ($importedArticlesArray as $i => $product) {

        $Detail = $Detail_Collection->addChild('Detail');
        $siteClass = $importedArticlesArray[$i]['propertyAddress'].' '.
          $importedArticlesArray[$i]['propertyIdentifier']."\r\n".
          $importedArticlesArray[$i]['mainclass']."\r\n".
          $importedArticlesArray[$i]['growplace']."\r\n".
          $importedArticlesArray[$i]['accessibility']."\r\n".
          $importedArticlesArray[$i]['quality']."\r\n";
        $Detail->addAttribute('compNo', '1');
        $Detail->addAttribute('Area', $importedArticlesArray[$i]['area']);
        $Detail->addAttribute('siteClass', $siteClass);
        $Detail->addAttribute('textbox8', 'Erityispiirteet'."\r\n");
        $Detail->addAttribute('Textbox10', 'Hoitoluokka'."\r\n");
        $Detail->addAttribute('textbox9', 'Lisätiedot'."\r\n");
        $Subreport3 = $Detail->addChild('Subreport3');
        $Report = $Subreport3->addChild('Report');
        $Report->addAttribute('Name', 'ListOfCompartmentsStandInfo');
        $table1 = $Report->addChild('table1');
        $table1->addAttribute('Textbox1', 'Yhteensä');
        $table1->addAttribute('Textbox5', '111.22404371584699453551912568');
        $table1->addAttribute('Textbox6', '200.200000');
        $table1->addAttribute('Textbox13', '144.10');
        $table1->addAttribute('Textbox14', '70.500000000');
        $table1->addAttribute('Textbox15', '71.200000000');
        $table1->addAttribute('Textbox16', '21.444262295081967213114754098');
        $table1->addAttribute('Textbox17', '16.20928961748633879781420765');
        $table1->addAttribute('Textbox18', '627');
        $table1->addAttribute('Textbox19', '18.3');
        $table1->addAttribute('Textbox20', '2.20');
        $Detail_Collection2 = $table1->addChild('Detail_Collection');
        if ($importedArticlesArray[$i]['mantyAge'] > 0) {
          $mantyDetail = $Detail_Collection2->addChild('Detail');
          $mantyDetail->addAttribute('treeSpec', 'Mänty');
          $mantyDetail->addAttribute('age', $importedArticlesArray[$i]['mantyAge']);
          $mantyDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['mantyVolume']);
          $mantyDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['mantyVolumeHa']);
          $mantyDetail->addAttribute('logVolume', $importedArticlesArray[$i]['mantyLogVolume']);
          $mantyDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['mantyPulpVolume']);
          $mantyDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['mantyDiameter']);
          $mantyDetail->addAttribute('averageLength', $importedArticlesArray[$i]['mantyLength']);
          $mantyDetail->addAttribute('density', $importedArticlesArray[$i]['mantyDensity']);
          $mantyDetail->addAttribute('basalArea', $importedArticlesArray[$i]['mantyBasalarea']);
          $mantyDetail->addAttribute('growth', $importedArticlesArray[$i]['mantyGrowth']);
        }
        if ($importedArticlesArray[$i]['kuusiAge'] > 0) {
          $kuusiDetail = $Detail_Collection2->addChild('Detail');
          $kuusiDetail->addAttribute('treeSpec', 'Kuusi');
          $kuusiDetail->addAttribute('age', $importedArticlesArray[$i]['mantyAge']);
          $kuusiDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['kuusiVolume']);
          $kuusiDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['kuusiVolumeHa']);
          $kuusiDetail->addAttribute('logVolume', $importedArticlesArray[$i]['kuusiLogVolume']);
          $kuusiDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['kuusiPulpVolume']);
          $kuusiDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['kuusiDiameter']);
          $kuusiDetail->addAttribute('averageLength', $importedArticlesArray[$i]['kuusiLength']);
          $kuusiDetail->addAttribute('density', $importedArticlesArray[$i]['kuusiDensity']);
          $kuusiDetail->addAttribute('basalArea', $importedArticlesArray[$i]['kuusiBasalarea']);
          $kuusiDetail->addAttribute('growth', $importedArticlesArray[$i]['kuusiGrowth']);
        }
        if ($importedArticlesArray[$i]['rauduskoivuAge'] > 0) {
          $rauduskoivuDetail = $Detail_Collection2->addChild('Detail');
          $rauduskoivuDetail->addAttribute('treeSpec', 'Rauduskoivu');
          $rauduskoivuDetail->addAttribute('age', $importedArticlesArray[$i]['rauduskoivuAge']);
          $rauduskoivuDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['rauduskoivuVolume']);
          $rauduskoivuDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['rauduskoivuVolumeHa']);
          $rauduskoivuDetail->addAttribute('logVolume', $importedArticlesArray[$i]['rauduskoivuLogVolume']);
          $rauduskoivuDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['rauduskoivuPulpVolume']);
          $rauduskoivuDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['rauduskoivuDiameter']);
          $rauduskoivuDetail->addAttribute('averageLength', $importedArticlesArray[$i]['rauduskoivuLength']);
          $rauduskoivuDetail->addAttribute('density', $importedArticlesArray[$i]['rauduskoivuDensity']);
          $rauduskoivuDetail->addAttribute('basalArea', $importedArticlesArray[$i]['rauduskoivuBasalarea']);
          $rauduskoivuDetail->addAttribute('growth', $importedArticlesArray[$i]['rauduskoivuGrowth']);
        }
        if ($importedArticlesArray[$i]['hieskoivuAge'] > 0) {
          $rauduskoivuDetail = $Detail_Collection2->addChild('Detail');
          $rauduskoivuDetail->addAttribute('treeSpec', 'Hieskoivu');
          $rauduskoivuDetail->addAttribute('age', $importedArticlesArray[$i]['hieskoivuAge']);
          $rauduskoivuDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['hieskoivuVolume']);
          $rauduskoivuDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['hieskoivuVolumeHa']);
          $rauduskoivuDetail->addAttribute('logVolume', $importedArticlesArray[$i]['hieskoivuLogVolume']);
          $rauduskoivuDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['hieskoivuPulpVolume']);
          $rauduskoivuDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['hieskoivuDiameter']);
          $rauduskoivuDetail->addAttribute('averageLength', $importedArticlesArray[$i]['hieskoivuLength']);
          $rauduskoivuDetail->addAttribute('density', $importedArticlesArray[$i]['hieskoivuDensity']);
          $rauduskoivuDetail->addAttribute('basalArea', $importedArticlesArray[$i]['hieskoivuBasalarea']);
          $rauduskoivuDetail->addAttribute('growth', $importedArticlesArray[$i]['hieskoivuGrowth']);
        }
        if ($importedArticlesArray[$i]['haapaAge'] > 0) {
          $haapaDetail = $Detail_Collection2->addChild('Detail');
          $haapaDetail->addAttribute('treeSpec', 'Haapa');
          $haapaDetail->addAttribute('age', $importedArticlesArray[$i]['haapaAge']);
          $haapaDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['haapaVolume']);
          $haapaDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['haapaVolumeHa']);
          $haapaDetail->addAttribute('logVolume', $importedArticlesArray[$i]['haapaLogVolume']);
          $haapaDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['haapaPulpVolume']);
          $haapaDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['haapaDiameter']);
          $haapaDetail->addAttribute('averageLength', $importedArticlesArray[$i]['haapaLength']);
          $haapaDetail->addAttribute('density', $importedArticlesArray[$i]['haapaDensity']);
          $haapaDetail->addAttribute('basalArea', $importedArticlesArray[$i]['haapaBasalarea']);
          $haapaDetail->addAttribute('growth', $importedArticlesArray[$i]['haapaGrowth']);
        }
        if ($importedArticlesArray[$i]['harmaaleppaAge'] > 0) {
          $harmaaleppaDetail = $Detail_Collection2->addChild('Detail');
          $harmaaleppaDetail->addAttribute('treeSpec', 'Harmaaleppa');
          $harmaaleppaDetail->addAttribute('age', $importedArticlesArray[$i]['harmaaleppaAge']);
          $harmaaleppaDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['harmaaleppaVolume']);
          $harmaaleppaDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['harmaaleppaVolumeHa']);
          $harmaaleppaDetail->addAttribute('logVolume', $importedArticlesArray[$i]['harmaaleppaLogVolume']);
          $harmaaleppaDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['harmaaleppaPulpVolume']);
          $harmaaleppaDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['harmaaleppaDiameter']);
          $harmaaleppaDetail->addAttribute('averageLength', $importedArticlesArray[$i]['harmaaleppaLength']);
          $harmaaleppaDetail->addAttribute('density', $importedArticlesArray[$i]['harmaaleppaDensity']);
          $harmaaleppaDetail->addAttribute('basalArea', $importedArticlesArray[$i]['harmaaleppaBasalarea']);
          $harmaaleppaDetail->addAttribute('growth', $importedArticlesArray[$i]['harmaaleppaGrowth']);
        }
        if ($importedArticlesArray[$i]['tervaleppaAge'] > 0) {
          $tervaleppaDetail = $Detail_Collection2->addChild('Detail');
          $tervaleppaDetail->addAttribute('treeSpec', 'Tervaleppa');
          $tervaleppaDetail->addAttribute('age', $importedArticlesArray[$i]['tervaleppaAge']);
          $tervaleppaDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['tervaleppaVolume']);
          $tervaleppaDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['tervaleppaVolumeHa']);
          $tervaleppaDetail->addAttribute('logVolume', $importedArticlesArray[$i]['tervaleppaLogVolume']);
          $tervaleppaDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['tervaleppaPulpVolume']);
          $tervaleppaDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['tervaleppaDiameter']);
          $tervaleppaDetail->addAttribute('averageLength', $importedArticlesArray[$i]['tervaleppaLength']);
          $tervaleppaDetail->addAttribute('density', $importedArticlesArray[$i]['tervaleppaDensity']);
          $tervaleppaDetail->addAttribute('basalArea', $importedArticlesArray[$i]['tervaleppaBasalarea']);
          $tervaleppaDetail->addAttribute('growth', $importedArticlesArray[$i]['tervaleppaGrowth']);
        }
        if ($importedArticlesArray[$i]['lehtipuuAge'] > 0) {
          $lehtipuuDetail = $Detail_Collection2->addChild('Detail');
          $lehtipuuDetail->addAttribute('treeSpec', 'Lehtipuu');
          $lehtipuuDetail->addAttribute('age', $importedArticlesArray[$i]['lehtipuuAge']);
          $lehtipuuDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['lehtipuuVolume']);
          $lehtipuuDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['lehtipuuVolumeHa']);
          $lehtipuuDetail->addAttribute('logVolume', $importedArticlesArray[$i]['lehtipuuLogVolume']);
          $lehtipuuDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['lehtipuuPulpVolume']);
          $lehtipuuDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['lehtipuuDiameter']);
          $lehtipuuDetail->addAttribute('averageLength', $importedArticlesArray[$i]['lehtipuuLength']);
          $lehtipuuDetail->addAttribute('density', $importedArticlesArray[$i]['lehtipuuDensity']);
          $lehtipuuDetail->addAttribute('basalArea', $importedArticlesArray[$i]['lehtipuuBasalarea']);
          $lehtipuuDetail->addAttribute('growth', $importedArticlesArray[$i]['lehtipuuGrowth']);
        }
        if ($importedArticlesArray[$i]['havupuuAge'] > 0) {
          $havupuuDetail = $Detail_Collection2->addChild('Detail');
          $havupuuDetail->addAttribute('treeSpec', 'Havupuu');
          $havupuuDetail->addAttribute('age', $importedArticlesArray[$i]['havupuuAge']);
          $havupuuDetail->addAttribute('volume_m3', $importedArticlesArray[$i]['havupuuVolume']);
          $havupuuDetail->addAttribute('volume_m3_ha', $importedArticlesArray[$i]['havupuuVolumeHa']);
          $havupuuDetail->addAttribute('logVolume', $importedArticlesArray[$i]['havupuuLogVolume']);
          $havupuuDetail->addAttribute('pulpVolume', $importedArticlesArray[$i]['havupuuPulpVolume']);
          $havupuuDetail->addAttribute('averageDiameter', $importedArticlesArray[$i]['havupuuDiameter']);
          $havupuuDetail->addAttribute('averageLength', $importedArticlesArray[$i]['havupuuLength']);
          $havupuuDetail->addAttribute('density', $importedArticlesArray[$i]['havupuuDensity']);
          $havupuuDetail->addAttribute('basalArea', $importedArticlesArray[$i]['havupuuBasalarea']);
          $havupuuDetail->addAttribute('growth', $importedArticlesArray[$i]['havupuuGrowth']);
        }
      }

      //echo $XMLRoot->asXML();
/*    No file
      $response = new Response($XMLRoot->asXML());
      $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
      return $response;
*/

/*    Older way to return the file
      $response = new Response($XMLRoot->asXML());
      $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
      $response->headers->set('Content-Disposition', 'attachment; filename="'.$owner.' - Valitut kuviot.xml"');
      if ($response) {
        return $response;
      }
*/
      $dom_sxe = dom_import_simplexml($XMLRoot);
      $dom = new \DOMDocument('1.0');
      $dom_sxe = $dom->importNode($dom_sxe, true);
      $dom_sxe = $dom->appendChild($dom_sxe);
      $dom->formatOutput = true;
      $dom->encoding = 'utf-8';

      $response = new Response($dom->saveXML());
      $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
      $response->headers->set('Content-Disposition', 'attachment; filename="'.$owner.' - Valitut kuviot.xml"');
      if ($response) {
        return $response;
      }

  }

}
