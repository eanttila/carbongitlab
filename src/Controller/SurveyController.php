<?php
namespace App\Controller;

use App\Entity\Survey;
use App\Entity\Property;
use App\Entity\Article;
use App\Entity\ImportedArticle;
use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Yectep\PhpSpreadsheetBundle\Factory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer as Writer;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;

use App\Service\SurveyService as ServiceSurvey;
use App\Service\ImportedArticleService as ServiceImportedArticle;
use App\Service\ArticleService as ArticleService;
use App\Service\PropertyService as PropertyService;

class SurveyController extends AbstractController {
	
	public function __construct(string $env)  {
		$this->env = $env;
		if ($this->env == 'dev') {
			$this->apiUrl = 'http://localhost/wordpress/surveys/import.php';
		} else {
			$this->apiUrl = 'https://greencarbon.fi/surveys/import.php';
		}
	}

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

  private function checkPermission($security) {
    if ($security->getUser() && $security->getUser()->getRole() == 'super-user') {
      return;
    } else {
      return $this->redirect('/');
    }
  }
  
    /**
  * @Route("/{locale}/functions/getenv/", methods={"GET"})
  */
 public function getEnvAction(Security $security, $locale, Request $request) {
   $response = $this->assertLocale($locale);
   if ($response) { return $response; }

   return new JsonResponse(array('message' => $this->env));

   }

  /**
  * @Route("/{locale}/functions/publishsurvey/{surveyId}", methods={"GET"})
  */
 public function surveyPublishAction(Security $security, $locale, Request $request, $surveyId) {
   $response = $this->assertLocale($locale);
   if ($response) { return $response; }

   $em = $this->getDoctrine()->getManager();
   $survey = $em->getRepository(Survey::class)->find($surveyId);
   if (!$survey) {
     return new JsonResponse(array('message' => 'No survey!'));
   }

   $allGetParams = $request->query->all();
   //return new JsonResponse(array('message' => $allGetParams));
   $includeLogo = isset($allGetParams['includeLogo']);
   $includeEmail = isset($allGetParams['includeEmail']);
   $staticSurvey = isset($allGetParams['staticSurvey']);

   $response = file_get_contents($this->apiUrl.'?surveyId='.$survey->getSurveyId().'&surveyName='.$survey->getName().'&postId='.$survey->getPostId().'&includeEmail='.$includeEmail.'&staticSurvey='.$staticSurvey.'&includeLogo='.$includeLogo);
   $responseArr = json_decode($response, true);
   //return new JsonResponse(array('message' => $responseArr));

   if ($responseArr['status'] == "success") {
     $survey->setLastPublished(time());
     $em = $this->getDoctrine()->getManager();
     $em->persist($survey);
     $em->flush();
     $this->addFlash('notice', 'Kysely julkaistiin osoitteessa https://greencarbon.fi/surveys/'.$survey->getName());
     return $this->redirect('/'.$locale.'/functions/survey/show/'.$surveyId);
   } else {
     $this->addFlash('notice', 'Kyselyn julkaisu epäonnistui! Virhe: '.$responseArr['message']);
     return $this->redirect('/'.$locale.'/functions/survey/show/'.$surveyId);
   }

   }

	 /**
   * @Route("/{locale}/functions/updatesurvey/{surveyId}", methods={"GET"})
   */
  public function surveyUpdateAction(Security $security, $locale, Request $request, $surveyId) {
    $response = $this->assertLocale($locale);
    if ($response) { return $response; }

	  //get surveyData and save setSurveyData()
	  //then: $data = json_decode($survey->getSurveyData(), true);
	  //php foreach $data:
	  //get all answers for questionNames and save as Answer entity

    $em = $this->getDoctrine()->getManager();
    $survey = $em->getRepository(Survey::class)->find($surveyId);
    if (!$survey) {
      return new JsonResponse(array('message' => 'No survey!'));
    }

    $json = file_get_contents('https://api.surveyjs.io/public/Survey/getSurvey?surveyId='.$survey->getSurveyId());
    $obj = json_decode($json, true);
    //print_r($obj);
    return new JsonResponse(array('message' => $obj));

    $survey->setSurveyData(json_encode($obj));
    $survey->setLastUpdated(time());

    $em = $this->getDoctrine()->getManager();
    $em->persist($survey);
    $em->flush();

	  return new JsonResponse(array('message' => json_decode($survey->getSurveyData(), true)));

    //$this->addFlash('notice', 'Kysely päivitettiin.');
    //return $this->redirect('/'.$locale.'/functions/survey/show/'.$surveyId);

    }

     /**
     * @Route("/{locale}/functions/survey", methods={"GET"})
     */
    public function surveysAction(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

	  $em = $this->getDoctrine()->getManager();
	  $surveyService  = new ServiceSurvey($this->getDoctrine()->getManager(), Survey::class);
	  $surveys = $surveyService->getAllSurveysAsArray();

	  //return new JsonResponse(array('message' => $templates));
	  return $this->render('surveys/'.$locale.'.index.html.twig',array(
		'surveys' => $surveys
	  ));

    }

	 /**
     * @Route("/{locale}/functions/addsurvey", methods={"GET"})
     */
    public function surveysAddAction(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

	  //return new JsonResponse(array('message' => $templates));
	  return $this->render('surveys/'.$locale.'.addsurvey.html.twig',array(

	  ));

    }

	 /**
     * @Route("/{locale}/functions/addsurvey", methods={"POST"})
     */
    public function surveysAddActionPost(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

	  $allPostParams = $request->request->all();
	  //return new JsonResponse(array('message' => $allPostParams));

	  $entityManager = $this->getDoctrine()->getManager();

	  $survey = new Survey();
	  $survey->setName($allPostParams['surveyName']);
	  $survey->setSurveyId($allPostParams['surveyId']);
	  $survey->setResultId($allPostParams['resultId']);
	  $survey->setPostId($allPostParams['postId']);
	  $survey->setSurveyData('');
	  $survey->setLastUpdated('');
	  $survey->setLastPublished('');

	  $entityManager = $this->getDoctrine()->getManager();
	  $entityManager->persist($survey);
	  $entityManager->flush();

      $this->addFlash('notice', 'Kysely lisättiin');
      return $this->redirect('/'.$locale.'/functions/survey');

    }

     /**
     * @Route("/{locale}/functions/survey/show/{surveyId}", methods={"GET"})
     */
    public function surveyShowAction(Security $security, $locale, Request $request, $surveyId) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }
	  
	  $allGetParams = $request->query->all();
	  if (isset($allGetParams['importMessage'])) {
		  //return new JsonResponse(array('message' => $allGetParams['importMessage']));
		  $this->addFlash('notice', $allGetParams['importMessage']);
		  return $this->redirect('/'.$locale.'/functions/survey/show/'.$surveyId);
	  }

	  $em = $this->getDoctrine()->getManager();
	  $survey = $em->getRepository(Survey::class)->find($surveyId);
	  if (!$survey) {
		return new JsonResponse(array('message' => 'No survey!'));
	  }

	  //$JSONdata = json_decode($survey->getSurveyData());
	  //$data = json_decode($survey->getSurveyData(), true);
    //return new JsonResponse(array('message' => $data));

    /*
	  $pages = $data['pages'];
	  //print_r($data['pages']);

	  $questions = array();
	  if ($pages) {
	  foreach ($pages as $i => $product) {
		  foreach ($pages[$i]['elements'] as $k => $product) {
			  array_push($questions, $pages[$i]['elements'][$k]);
		  }
	  }
	  }
	  //return new JsonResponse(array('message' => $questions));
	  //get answers from answers table where surveyname=survey->getName && name=question['name']
    */

	  return $this->render('surveys/'.$locale.'.show.html.twig',array(
		'survey' => $survey,
		'questions' => []
	  ));

	  //return new JsonResponse(array('message' => json_decode(($survey->getSurveyData()))));

    }

	  /**
     * @Route("/{locale}/surveytest", methods={"GET"})
     */
    public function surveyTestAction(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

	  $entityManager = $this->getDoctrine()->getManager();

	  $survey = new Survey();
	  $survey->setName('test');
	  $survey->setSurveyId('surveyId');
	  $survey->setResultId('resultId');
	  $survey->setPostId('postId');
	  $survey->setSurveyData('{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu"}}]}{"locale":"fi","title":{"default":"Testikysely","fi":"Työntekijäkysely"},"description":{"default":"Testataan","fi":"Kysely yrityksen työntekijöille"},"pages":[{"name":"Ensimmäinen sivu","elements":[{"type":"radiogroup","name":"question3","title":{"fi":"Olen sukupuoleltani:"},"choices":[{"value":"item1","text":{"fi":"Mies"}},{"value":"item2","text":{"fi":"Nainen"}},{"value":"item3","text":{"fi":"Muunsukupuolinen"}}]},{"type":"boolean","name":"question9","title":{"fi":"Olen seurannut ilmastokeskustelua valveutuneena:"},"labelTrue":{"default":"Yes","fi":"Kyllä"},"labelFalse":{"default":"No","fi":"En"}},{"type":"rating","name":"question1","title":{"fi":"Ilmastoahdistukseni on asteikolla 1-5 seuraava:"}},{"type":"html","name":"question4"}],"title":{"fi":"Ensimmäinen sivu"}},{"name":"Toinen sivu","elements":[{"type":"checkbox","name":"question2","title":{"fi":"Ilmastokeskustelu on aiheuttanut seuraavat näistä:"},"choices":[{"value":"item1","text":{"fi":"Olen alkanut miettimään kulutustottumuksiani"}},{"value":"item2","text":{"fi":"Olen entistäkin huolestuneempi"}},{"value":"item3","text":{"fi":"Aion osallistua ilmastokeskusteluun parhaani mukaan jatkossakin"}}]},{"type":"radiogroup","name":"question5","title":{"fi":"Yleisin tapani työmatkoihini on:"},"choices":[{"value":"item1","text":{"fi":"Yksityinen ajoneuvo"}},{"value":"item2","text":{"fi":"Julkinen ajoneuvo"}},{"value":"item3","text":{"fi":"Kävely/polkupyörä"}}]},{"type":"text","name":"question6","title":{"fi":"Kuinka paljon palkasta ilmaston hyväksi:"}}],"title":{"fi":"Toinen sivu"}},{"name":"Kolmas sivu","elements":[{"type":"comment","name":"question7","title":{"fi":"Vapaamuotoinen palaute kyselystä:"}}],"title":{"fi":"Kolmas sivu123"}}]}');

	  $entityManager = $this->getDoctrine()->getManager();
	  $entityManager->persist($survey);
	  $entityManager->flush();

      return new JsonResponse(array('message' => 'done!'));

    }

	 /**
     * @Route("/{locale}/survey/{client}", methods={"GET"})
     */
    public function surveyClientAction(Security $security, $locale, Request $request, $client) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

	  //search using name
	  $em = $this->getDoctrine()->getManager();
	  $sql = "SELECT id FROM survey WHERE name = '".$client."'";
	  $stmt = $em->getConnection()->prepare($sql);
	  $stmt->execute();
	  $surveyNameResults = $stmt->fetch();

	  if (!$surveyNameResults) {
		  return new JsonResponse(array('message' => 'No survey with the name!'));
	  }

	  $survey = $em->getRepository(Survey::class)->find($surveyNameResults['id']);
	  if (!$survey) {
		return new JsonResponse(array('message' => 'No survey with the id!'));
	  }

	  if ($survey->getSurveyData() == '') {
		return new JsonResponse(array('message' => 'No survey data!'));
	  }

	  $JSONdata = $survey->getSurveyData();
	  $postId = $survey->getPostId();
	  return $this->render('surveys/'.$locale.'.display.html.twig', array(
		'JSONdata' => $JSONdata,
		'postId' => $postId
	  ));

    }

      /**
     * @Route("/{locale}/surveyOLD/{client}", methods={"GET"})
     */
    public function surveyClientActionOLD(Security $security, $locale, Request $request, $client) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      return $this->render('surveys/'.$locale.'.showsurvey.company1.html.twig',array(

	  ));

    }

    /**
    * @Route("/{locale}/functions/removesurveys", methods={"GET"})
    */
    public function removeSurveyAction(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $em = $this->getDoctrine()->getManager();

      $sql = "DELETE FROM survey";
      $stmt = $em->getConnection()->prepare($sql);
      $stmt->execute();

      $sql = "ALTER TABLE survey AUTO_INCREMENT = 1;";
      $stmt = $em->getConnection()->prepare($sql);
      $stmt->execute();

      return new JsonResponse(array('message' => 'Done'));
    }

     /**
     * @Route("/{locale}/surveyOLD/", methods={"GET"})
     */
    public function surveyActionOLD(Security $security, $locale, Request $request) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }

      $hasPermission = $this->checkPermission($security);
      if ($hasPermission) { return $hasPermission; }

	  $rootDir = "../templates/surveys/";

	  $currentDirectoryItems = array_diff(scandir($rootDir . "/", 1), [".", ".."]);

	  $surveys = [];

	  foreach ($currentDirectoryItems as $i => $product) {
		  if ($currentDirectoryItems[$i] !== 'fi.index.html.twig') {
			  $file = fopen($rootDir.$currentDirectoryItems[$i],"r");
			  //echo fgets($file);
			  $client = str_replace(array('fi.showsurvey.', '.html.twig'), '', $currentDirectoryItems[$i]);
			  $idAndResultId = str_replace("{#", "", fgets($file));
			  $idAndResultId = str_replace("#}\r\n", "", $idAndResultId);

			  $id = "";

			  $templateParent = array(
			  'client' => $client,
			  'template' => $currentDirectoryItems[$i],
			  'id' => $id);
			  fclose($file);
			  array_push($surveys, $templateParent);
		  }
	  }

      //return new JsonResponse(array('message' => $templates));
	  return $this->render('surveys/'.$locale.'.index.html.twig',array(
		'surveys' => $surveys
	  ));

    }

}
