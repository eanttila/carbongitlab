<?php

// src/Controller/RegistrationController.php
namespace App\Controller;

use App\Form\UserType;
use App\Form\UserTypeFi;
use App\Entity\User;
use App\Security\LoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends Controller
{
    private function assertLocale($locale) {
      if ($locale !== 'fi' && $locale !== 'en') {
        return $this->redirect('/');
      }
    }

    /**
     * @Route("/{locale}/register", name="user_registration")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, LoginFormAuthenticator $authenticator, GuardAuthenticatorHandler $guardHandler, $locale)
    {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        // 1) build the form
        $user = new User();

        if ($locale == 'fi') {
          $form = $this->createForm(UserTypeFi::class, $user);
        } else {
          $form = $this->createForm(UserType::class, $user);
        }

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $user->setEmail($user->getUsername());
            $user->setName('');
            $user->setPhone('');
            $user->setAddress('');
            $user->setZip('');
            $user->setCity('');
            $user->setJoined(time());
            $user->setLanguage($locale);
			$user->setRole('');

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $guardHandler->authenticateUserAndHandleSuccess(
               $user,
               $request,
               $authenticator,
               'welcome' // firewall name in security.yaml
            );

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            //return $this->redirectToRoute('welcome');
        }

        return $this->render(
            'registration/'.$locale.'.register.html.twig',
            array('form' => $form->createView())
        );
    }
}
