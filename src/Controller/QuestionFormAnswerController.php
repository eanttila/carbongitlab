<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;
use App\Entity\Question;
use App\Entity\Factor;
use App\Entity\Answer;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Knp\Component\Pager\PaginatorInterface;
//services can have more functions
//service example: $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
//service example: $article = $articleService->getArticle($id);
use App\Service\AnswerService as ServiceAnswer;
use App\Service\UserService as ServiceUser;
use App\Service\PropertyService as ServiceProperty;
use App\Service\CompanyService as CompanyService;
use App\Service\QuestionService as QuestionService;

class QuestionFormAnswerController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

    /**
     * @Route("/{locale}/questionform/answer/{companyId}/{formName}/", methods={"GET"})
     */
    public function questionFormAnswerForm(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          $companyName = $company->getName();

          if ($formName) {
            $entityManager = $this->getDoctrine()->getManager();
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

            $tabsArray = array();
            foreach ($tabsResults as $i => $product) {
              array_push($tabsArray, $tabsResults[$i]['tab_name']);
            }
            $tabsArray = array_unique($tabsArray);
          } else {
            $formName = '';
          }

          $selectedTabName = array_shift($tabsArray);
          $selectedTabObj = (object)[];
          $selectedTabObj->tab_name = $selectedTabName;

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."' AND tab_name = '".$selectedTabName."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabQuestionResults = $stmt->fetchAll();
          if (!$tabQuestionResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          return $this->redirect('/'.$locale.'/questionform/answer/'.$companyId.'/'.$formName.'/'.$selectedTabObj->tab_name);

          /*
          return $this->render('questionform-answer/'.$locale.'.index.html.twig', array(
              'companyId' => $companyId,
              'companyName' => $companyName,
              'formName' => $formName,
              'selectedTab' => $selectedTabObj,
              'tabNames' => $tabsArray,
              'tabQuestions' => $tabQuestionResults ));
        */
        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/answer/{companyId}/{formName}/{tabName}/", methods={"GET"})
     */
    public function questionFormAnswerFormTab(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'no company!')); }
          $companyName = $company->getName();
          $entityManager = $this->getDoctrine()->getManager();

          if ($formName) {
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

            $tabsArray = array();
            foreach ($tabsResults as $i => $product) {
              if ($tabsResults[$i]['tab_name'] !== $tabName) {
                array_push($tabsArray, $tabsResults[$i]['tab_name']);
              } else if ($tabsResults[$i]['tab_name'] === $tabName) {
                $selectedTab = $tabsResults[$i];
              }
            }
            $tabsArray = array_unique($tabsArray);
          } else {
            $formName = '';
          }

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."' AND tab_name = '".$selectedTab['tab_name']."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabQuestionResults = $stmt->fetchAll();
          if (!$tabQuestionResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          $enableSubmit = false;
          foreach ($tabQuestionResults as $i => $product) {

            $sql = "SELECT time, content FROM answer WHERE question_id = '".$tabQuestionResults[$i]['id']."' AND company_id = '".$companyId."' AND form_name = '".$formName."' ".
            "AND tab_name = '".$selectedTab['tab_name']."' AND time >= DATE(NOW()) - INTERVAL 7 DAY ORDER BY time DESC;";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $answerResults = $stmt->fetch();
            if ($answerResults) {
              $tabQuestionResults[$i]['time'] = $answerResults['time'];
              $tabQuestionResults[$i]['answer'] = $answerResults['content'];
              $tabQuestionResults[$i]['timeFi'] = date('d.m.Y', $answerResults['time']);
              $tabQuestionResults[$i]['timeUk'] = date('d/m/Y', $answerResults['time']);
            } else {
              $tabQuestionResults[$i]['time'] = '';
              $enableSubmit = true;
            }
            $monthEnd = strtotime('last day of this month', $tabQuestionResults[$i]['target_time']);
            $monthEndFormatted = date('d.m.Y', $monthEnd);
            if ($monthEndFormatted === date('d.m.Y', $tabQuestionResults[$i]['target_time'])) {
              $tabQuestionResults[$i]['targetTimeFi'] = date('Y', $tabQuestionResults[$i]['target_time']);
              $tabQuestionResults[$i]['targetTimeUk'] = date('Y', $tabQuestionResults[$i]['target_time']);
            } else {
              $tabQuestionResults[$i]['targetTimeFi'] = date('m/Y', $tabQuestionResults[$i]['target_time']);
              $tabQuestionResults[$i]['targetTimeUk'] = date('m/Y', $tabQuestionResults[$i]['target_time']);
            }
          }

          return $this->render('questionform-answer/'.$locale.'.index.html.twig', array(
              'companyId' => $companyId,
              'companyName' => $companyName,
              'formName' => $formName,
              'selectedTab' => $selectedTab,
              'tabNames' => $tabsArray,
              'tabQuestions' => $tabQuestionResults,
              'enableSubmit' => $enableSubmit ));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route("/{locale}/questionform/answer/{companyId}/{formName}/{tabName}/", methods={"POST"})
     */
    public function questionFormAnswerFormTabPost(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName, $tabName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          $allParams = $request->request->all();
          $answers = $allParams['answers'];

          $user = $security->getUser();
          $entityManager = $this->getDoctrine()->getManager();

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          if (!$company) { return new JsonResponse(array('message' => 'No company!')); }

          foreach ($answers as $i => $product) {
            //echo $answers[$i]['content'];

            $answer = new Answer();
            $question = $this->getDoctrine()->getRepository(Question::class)->find($answers[$i]['question-id']);
            if (!$question) { return new JsonResponse(array('message' => 'No question!')); }

            $factor = $question->getFactor();
            if (!$question) { return new JsonResponse(array('message' => 'No factor!')); }

            $targetTime = $question->getTargetTime();

            $answer->setContent($answers[$i]['content']);
            $answer->setFormName($formName);
            $answer->setTabName($tabName);
            $answer->setQuestion($question);
            $answer->setUser($user);
            $answer->setCompany($company);
            $answer->setFactor($factor);
            $answer->setTargetTime($targetTime);
            $answer->setTime(time());

            $entityManager->persist($answer);
            $entityManager->flush();
          }

          $this->addFlash('notice',
            'Vastaukset päivitetty onnistuneesti.');
          return $this->redirect('/'.$locale.'/questionform/answer/'.$companyId.'/'.$formName.'/'.$tabName);
          //return new JsonResponse(array('message' => $answers));

        } else {
          return $this->redirectToRoute('welcome');
        }
    }

    /**
     * @Route("/{locale}/questionform/showanswers/{companyId}/{formName}/", methods={"GET"})
   */
    public function questionFormShowAnswers(Security $security, Request $request, PaginatorInterface $paginator, $locale, $companyId, $formName) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          //return new JsonResponse(array('message' => 'questionFormShowAnswers'));
          $filteredBy = $request->get('filteredBy');
          if (!$filteredBy) { $filteredBy = 'all'; }

          $entityManager = $this->getDoctrine()->getManager();

          if ($formName) {
            $sql = "SELECT tab_name FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $tabsResults = $stmt->fetchAll();
            if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          } else {
            $formName = '';
          }

          $sql = "SELECT * FROM question WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          //rename tabQuestions

          $questionResults = $stmt->fetchAll();
          if (!$questionResults) { return new JsonResponse(array('message' => 'No results!')); }

          $enableSubmit = false;
          $answers = array();
          $noAnswers = array();
          foreach ($questionResults as $i => $product) {

            $sql = "SELECT id, time, content, question_id FROM answer WHERE question_id = '".$questionResults[$i]['id']."' AND company_id = '".$companyId."' AND form_name = '".$formName."' ".
            "AND time >= DATE(NOW()) - INTERVAL 7 DAY ORDER BY time DESC;";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $answerResults = $stmt->fetch();
            if ($answerResults) {
              $questionResults[$i]['time'] = $answerResults['time'];
              $questionResults[$i]['answer'] = $answerResults['content'];
              $questionResults[$i]['question_id'] = $answerResults['id'];
              $questionResults[$i]['timeFi'] = date('d.m.Y', $answerResults['time']);
              $questionResults[$i]['timeUk'] = date('d/m/Y', $answerResults['time']);
              $monthEnd = strtotime('last day of this month', $questionResults[$i]['target_time']);
              $monthEndFormatted = date('d.m.Y', $monthEnd);
              if ($monthEndFormatted === date('d.m.Y', $questionResults[$i]['target_time'])) {
                $questionResults[$i]['targetTimeFi'] = date('Y', $questionResults[$i]['target_time']);
                $questionResults[$i]['targetTimeUk'] = date('Y', $questionResults[$i]['target_time']);
              } else {
                $questionResults[$i]['targetTimeFi'] = date('m/Y', $questionResults[$i]['target_time']);
                $questionResults[$i]['targetTimeUk'] = date('m/Y', $questionResults[$i]['target_time']);
              }
              array_push($answers, $questionResults[$i]);
            } else {
              $questionResults[$i]['time'] = '';
              $questionResults[$i]['answer'] = 'No answer';
              $enableSubmit = true;
              array_push($noAnswers, $questionResults[$i]);
            }
          }

          if ($filteredBy !== 'all') {
            $newAnswers = array();
            foreach ($answers as $i => $value) {
                if ($answers[$i]['tab_name'] == $filteredBy)  {
                    $newAnswers[] = $answers[$i];
                }
            }
            $answers = $newAnswers;
          }

          /*
          TABS
          */
          $notAnsweredTabsArray = array();
          $notAnsweredQuestions = array();
          $addedInArray = array();
          foreach ($noAnswers as $i => $product) {
            if (!in_array($noAnswers[$i]['tab_name'], $addedInArray)) {
              array_push($notAnsweredTabsArray, $noAnswers[$i]);
              array_push($addedInArray, $noAnswers[$i]['tab_name']);
            }
          }

          foreach ($noAnswers as $i => $product) {
            foreach ($notAnsweredTabsArray as $o => $product) {
              if ($noAnswers[$i]['tab_name'] == $notAnsweredTabsArray[$o]['tab_name']) {
                if (!isset($notAnsweredTabsArray[$o]['questions'])) {
                  $notAnsweredTabsArray[$o]['questions'] = array();
                }
                $noAnswers[$i]['targetTimeFi'] = 'test';
                $monthEnd = strtotime('last day of this month', $noAnswers[$i]['target_time']);
                $monthEndFormatted = date('d.m.Y', $monthEnd);
                if ($monthEndFormatted === date('d.m.Y', $noAnswers[$i]['target_time'])) {
                  $noAnswers[$i]['targetTimeFi'] = date('Y', $noAnswers[$i]['target_time']);
                  $noAnswers[$i]['targetTimeUk'] = date('Y', $noAnswers[$i]['target_time']);
                } else {
                  $noAnswers[$i]['targetTimeFi'] = date('m/Y', $noAnswers[$i]['target_time']);
                  $noAnswers[$i]['targetTimeUk'] = date('m/Y', $noAnswers[$i]['target_time']);
                }
                //$noAnswers[$i]['timeFi'] = date('d.m.Y', $noAnswers[$i]['time']);
                //$noAnswers[$i]['timeUk'] = date('d/m/Y', $noAnswers[$i]['time']);
                array_push($notAnsweredTabsArray[$o]['questions'], $noAnswers[$i]);
              }
            }
          }

          // Paginate the results of the query
          $answersPaginated = $paginator->paginate(
              // Doctrine Query, not results
              $answers,
              // Define the page parameter
              $request->query->getInt('page', 1),
              // Items per page
              5
          );

          $sql = "SELECT tab_name FROM answer WHERE form_name = '".$formName."' AND company_id = '".$companyId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $tabsResults = $stmt->fetchAll();
          if (!$tabsResults) { return new JsonResponse(array('message' => 'No tab results!')); }

          $answeredTabsArray = array();
          foreach ($tabsResults as $i => $product) {
              array_push($answeredTabsArray, $tabsResults[$i]['tab_name']);
          }
          $answeredTabsArray = array_unique($answeredTabsArray);

          //echo 'filtered by: '.$filteredBy;

          return $this->render('questionform-answer/'.$locale.'.show-answers.html.twig', array(
              'filteredBy' => $filteredBy,
              'companyId' => $companyId,
              'formName' => $formName,
              'answeredTabsArray' => $answeredTabsArray,
              'answers' => $answers,
              'answersPaginated' => $answersPaginated,
              'noAnswers' => $noAnswers,
              'notAnsweredTabsArray' => $notAnsweredTabsArray,
              'enableSubmit' => $enableSubmit ));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }
}
