<?php
namespace App\Controller;

use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

//services can have more functions
//service example: $articleService = new ServiceArticle($this->getDoctrine()->getManager(),Article::class);
//service example: $article = $articleService->getArticle($id);
use App\Service\ArticleService as ServiceArticle;
use App\Service\UserService as ServiceUser;
use App\Service\PropertyService as ServiceProperty;
use App\Service\CompanyService as CompanyService;


class CompanyController extends AbstractController {

  private function assertLocale($locale) {
    if ($locale !== 'fi' && $locale !== 'en') {
      return $this->redirect('/');
    }
  }

    /**
     * @Route("/company/removeusertest", methods={"GET"})
     */
    public function test(Request $request) {

        $company = $this->getDoctrine()->getRepository(Company::class)->find(2);
        $user = $this->getDoctrine()->getRepository(User::class)->find(10);

        $company->removeUser($user);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($company);
        $entityManager->flush();

        $companyId = $company->getId();
        $locale = 'fi';
        return $this->redirect('/'.$locale.'/company/'.$companyId);

    }

    /**
     * @Route("/{locale}/company/{companyId}/adduser/{userId}", methods={"GET"})
     */
    public function addUserAPI(Security $security, Request $request, $locale, $companyId, $userId) {
        //$ownUserId = $security->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $sql = "INSERT INTO `user_companies` (`company_id`, `user_id`) VALUES ('".$companyId."', '".$userId."')";
        $stmt = $entityManager->getConnection()->prepare($sql);
        $stmt->execute();

        /* Older way that won't work on heroku:
        $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
        if ($ownUserId == $userId) { //so that the cookie won't die
          $user = $security->getUser();
        } else {
          $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
        }

        $company->addUser($user);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($company);
        $entityManager->flush();
        */

        return $this->redirect('/'.$locale.'/company/'.$companyId);

    }

    /**
     * @Route("/{locale}/company/{companyId}/removeuser/{userId}", methods={"GET"})
     */
    public function removeUserAPI(Security $security, Request $request, $locale, $companyId, $userId) {
        //$ownUserId = $security->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $sql = "DELETE FROM user_companies WHERE company_id = '".$companyId."' AND user_id = '".$userId."'";
        $stmt = $entityManager->getConnection()->prepare($sql);
        $stmt->execute();

        /* Older way that won't work on heroku:
        $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
        if ($ownUserId == $userId) { //so that the cookie won't die
          $user = $security->getUser();
        } else {
          $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
        }

        $company->removeUser($user);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($company);
        $entityManager->flush();
        */

        return $this->redirect('/'.$locale.'/company/'.$companyId);

    }

    /**
     * @Route ("/company/{companyId}/searchuser/{email}", name= "company_searchuser", methods={"GET"})
     */
    public function searchUserAPI(Security $security, $companyId, $email) {

      $userService = new ServiceUser($this->getDoctrine()->getManager(), Property::class);
      $foundUsers = $userService->searchByEmail($email);

      $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);

      $newArray = array();

      if (!$company) {
        return new JsonResponse($newArray);
      }

      $companyUsers = $company->getUsers();

      foreach ($foundUsers as $i => $product) {
        array_push($newArray, array('id' => $foundUsers[$i]['id'],
        'name' => $foundUsers[$i]['name'],
        'username' => $foundUsers[$i]['username'],
        'added' => false));
      }

      foreach ($companyUsers as $i => $product) {
        foreach ($newArray as $k => $product1) {
          if ($newArray[$k]['id'] == $companyUsers[$i]->getId()) {
            $newArray[$k]['added'] = true;
            //unset($newArray[$k]);
          }
        }
      }

/*
      foreach ($newArray as $i => $product) {
        foreach ($companyUsers as $k => $product1) {
          if (isset($newArray[$i])) {
            if ($newArray[$i]['id'] == $companyUsers[$k]->getId()) {
              //$newArray[$i]['added'] = true;
              unset($newArray[$i]);
              }
          }
        }
      }
*/
      return new JsonResponse($newArray);

    }

    /**
     * @Route ("/{locale}/company/{companyId}/adduser/", name= "company_adduser", methods={"GET"})
     */
    public function addUser(Security $security, $locale, $companyId) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);


        return $this->render('companies/'.$locale.'.addUser.html.twig', ['company' => $company]);

    }

    /**
     * @Route("/{locale}/company", name="company_list", methods={"GET"})
     */
    public function company(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {

          //$user = $security->getUser(); cookie death
          //$companies = $user->getCompanies();

          $userId = $security->getUser()->getId();
          $entityManager = $this->getDoctrine()->getManager();
          $sql = "SELECT * FROM user_companies WHERE user_id = '".$userId."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $companyResults = $stmt->fetchAll();

          $companies = array();
          foreach ($companyResults as $i => $product) {
             array_push($companies, $this->getDoctrine()->getRepository(Company::class)->find($companyResults[$i]['company_id']));
          }

          return $this->render('companies/'.$locale.'.index.html.twig', array
          ('companies' => $companies));

        } else {
          return $this->redirectToRoute('welcome');
        }

    }

    /**
     * @Route ("/{locale}/newcompany", methods={"GET"})
     */
    public function new(Security $security, Request $request, $locale) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if ($security->getUser()->getRole() == '') {
          return new JsonResponse(array('message' => 'No permission!'));
        }

        $newCompany = new Company();

        return $this->render('companies/'.$locale.'.new.html.twig',array(
            'company'=>$newCompany ));
    }

    /**
     * @Route ("/{locale}/newcompany", methods={"POST"})
     */
    public function newPost(Security $security, Request $request, $locale) {
      $company = new Company();

      if ($security->getUser()->getRole() == '') {
        return new JsonResponse(array('message' => 'No permission!'));
      }

      $userId = $security->getUser()->getId();

      $entityManager = $this->getDoctrine()->getManager();

      $company->setName($request->get('name'));
      $company->setBusinessId($request->get('businessId'));
      $company->setIndustry($request->get('industry'));
      //$company->addUser($user); cookie death

      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($company);
      $entityManager->flush();

      $companyId = $company->getId();
      $sql = "INSERT INTO `user_companies` (`company_id`, `user_id`) VALUES ('".$companyId."', '".$userId."')";
      $stmt = $entityManager->getConnection()->prepare($sql);
      $stmt->execute();

      return $this->redirect('/'.$locale.'/company/'.$companyId);
    }

    /**
     * @Route ("/{locale}/company/{companyId}", name= "company_show", methods={"GET"})
     */
    public function show(Security $security, $locale, $companyId) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        //$company = $this->getDoctrine()->getRepository(Company::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $userId = $security->getUser()->getId();
        $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
        $hasPermission = $companyService->checkPermission($companyId, $userId);
        if (!$hasPermission) { return new JsonResponse(array('message' => 'No permission!')); }

        /* questions */
        $sql = "SELECT * FROM question WHERE company_id = '".$companyId."'";
        $stmt = $entityManager->getConnection()->prepare($sql);
        $stmt->execute();
        $questionResults = $stmt->fetchAll();

        $questionFormsArray = array();
        $addedInArray = array();
        foreach ($questionResults as $i => $product) {
          if (!in_array($questionResults[$i]['form_name'], $addedInArray)) {
            array_push($questionFormsArray, $questionResults[$i]);
            array_push($addedInArray, $questionResults[$i]['form_name']);
          }
        }

        foreach ($questionFormsArray as $i => $product) {
          $sql = "SELECT * FROM question WHERE company_id = '".$companyId."' AND form_name = '".$questionFormsArray[$i]['form_name']."'";
          $stmt = $entityManager->getConnection()->prepare($sql);
          $stmt->execute();
          $questionsResults = $stmt->fetchAll();
          $questionFormsArray[$i]['questions'] = $questionsResults;
          $questionFormsArray[$i]['tabs'] = array();

          $tabsArray = array();
          $addedInArray = array();
          foreach ($questionsResults as $o => $product) {
            if (!in_array($questionsResults[$o]['tab_name'], $addedInArray)) {
              array_push($questionFormsArray[$i]['tabs'], $questionsResults[$o]);
              array_push($addedInArray, $questionsResults[$o]['tab_name']);
            }
          }
        }

        /* answers */
        $sql = "SELECT * FROM answer WHERE company_id = '".$companyId."'";
        $stmt = $entityManager->getConnection()->prepare($sql);
        $stmt->execute();
        $answerResults = $stmt->fetchAll();

        $answerFormsArray = array();
        $addedInArray = array();
        $answerYears = array();
        foreach ($answerResults as $i => $product) {
          if (!in_array(date('Y', $answerResults[$i]['target_time']), $addedInArray)) {
            $answerResults[$i]['year'] = date('Y', $answerResults[$i]['target_time']);
            array_push($answerYears, $answerResults[$i]);
            array_push($addedInArray, date('Y', $answerResults[$i]['target_time']));
          }
          if (!in_array($answerResults[$i]['form_name'], $addedInArray)) {

            $sql = "SELECT * FROM answer WHERE company_id = '".$companyId."' AND form_name = '".$answerResults[$i]['form_name']."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $totalAnswerResults = $stmt->fetchAll();

            $sql = "SELECT * FROM question WHERE company_id = '".$companyId."' AND form_name = '".$answerResults[$i]['form_name']."'";
            $stmt = $entityManager->getConnection()->prepare($sql);
            $stmt->execute();
            $questionResults = $stmt->fetchAll();

            $answerResults[$i]['totalAnswers'] = count($totalAnswerResults);
            $answerResults[$i]['totalQuestions'] = count($questionResults);

            array_push($answerFormsArray, $answerResults[$i]);
            array_push($addedInArray, $answerResults[$i]['form_name']);
          }
        }

          $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
          return $this->render('companies/'.$locale.'.show.html.twig', [
          'company' => $company,
          'forms' => $questionFormsArray,
          'answers' => $answerFormsArray,
          'answerYears' => $answerYears
          ]);

    }

    /**
     * @Route("/{locale}/company/update/{id}", methods={"GET"})
     */
    public function update(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if ($security->getUser()->getRole() == '') {
          return new JsonResponse(array('message' => 'No permission!'));
        }

        $company = $this->getDoctrine()->getRepository(Company::class)->find($id);

        return $this->render('companies/'.$locale.'.update.html.twig',array(
            'company' => $company ));
    }

    /**
     * @Route("/{locale}/company/update/{id}", methods={"POST"})
     */
    public function updatePost(Security $security, Request $request, $locale, $id) {
        $response = $this->assertLocale($locale);
        if ($response) { return $response; }

        if ($security->getUser()->getRole() == '') {
          return new JsonResponse(array('message' => 'No permission!'));
        }

        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository(Company::class)->find($id);

        if (!$company) {
          throw $this->createNotFoundException(
            'No company found for id '.$id
          );
        }

        $company->setName($request->get('name'));
        $company->setBusinessId($request->get('businessId'));
        $company->setIndustry($request->get('industry'));

        $em->flush();

        return $this->redirect('/'.$locale.'/company/update/'.$id);
    }

    /**
     * @Route ("/{locale}/company/delete/{id}", methods={"GET"})
     */
    public function delete(Security $security, Request $request, $locale, $id) {
      $response = $this->assertLocale($locale);
      if ($response) { return $response; }
/*
      $em = $this->getDoctrine()->getManager();
      $article = $em->getRepository(Company::class)->find($id);

      //check if own company???

      $companyService = new CompanyService($this->getDoctrine()->getManager(), Company::class);
      $companyService->deleteCompany($id);
*/
      return $this->redirect('/'.$locale.'/company/');
    }
}
