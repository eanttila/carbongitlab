<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImportedArticleRepository")
 */
class ImportedArticle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @Assert\Length(max=1000)
    * @ORM\Column(type="decimal", scale=2)
   */
   private $area;
   
   /**
   * @ORM\Column(type="text", length=100)
   */
   private $standNumber;

   /**
   * @Assert\Length(max=11)
  * @ORM\Column(type="integer")
  */
   private $subgroupNumber;

   /**
   * @Assert\Length(max=11)
  * @ORM\Column(type="integer")
  */
   private $fertilityclassNumber;

   /**
   * @ORM\Column(type="text", length=100)
   */
   private $mainclass;

   /**
   * @ORM\Column(type="text", length=100)
   */
   private $growplace;

   /**
   * @ORM\Column(type="text", length=100)
   */
   private $soiltype;

   /**
   * @ORM\Column(type="text", length=100)
   */
   private $developmentclass;

   /**
   * @ORM\Column(type="text", length=100)
   */
   private $accessibility;

   /**
   * @ORM\Column(type="text", length=100)
   */
   private $quality;

   /**
   * @ORM\Column(type="text", length=100)
   */
   private $operations;

   /**
   * @Assert\Length(max=200)
  * @ORM\Column(type="integer")
  */
   private $added;

   /**
   * @Assert\Length(max=1000)
   * @ORM\Column(type="decimal", scale=2)
  */
  private $mantyAge;

  /**
  * @Assert\Length(max=1000)
  * @ORM\Column(type="decimal", scale=2)
 */
 private $mantyVolume;

 /**
 * @Assert\Length(max=1000)
 * @ORM\Column(type="decimal", scale=2)
*/
private $mantyVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $mantyLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $mantyPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $mantyDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $mantyLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $mantyDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $mantyBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $mantyGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $kuusiGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $rauduskoivuGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $hieskoivuGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $haapaGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $harmaaleppaGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $tervaleppaGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $havupuuGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $lehtipuuGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalAge;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalVolume; //remove later

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalVolumeHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalLogVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalPulpVolume;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalDiameter;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalLength;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $totalDensity;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalBasalarea;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
  private $totalGrowth;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $additionality5yearsHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $additionality10yearsHa;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $additionality5yearsPercentage;

/**
* @Assert\Length(max=1000)
* @ORM\Column(type="decimal", scale=2)
*/
private $additionality10yearsPercentage;

/**
* @ORM\ManyToOne(targetEntity="Property", inversedBy="importedarticles")
* @ORM\JoinColumn(name="property_id", referencedColumnName="id")
*/
private $property;

   //Getters & Setters
  public function getId(){
    return $this->id;
  }

  public function getArea(){
    return $this->area;
  }
  
  public function getStandNumber(){
    return $this->standNumber;
  }

  public function getSubgroupNumber(){
    return $this->subgroupNumber;
  }

  public function getFertilityclassNumber(){
    return $this->fertilityclassNumber;
  }

  public function getMainclass(){
    return $this->mainclass;
  }

  public function getGrowplace(){
    return $this->growplace;
  }

  public function getSoiltype(){
    return $this->soiltype;
  }

  public function getDevelopmentclass(){
    return $this->developmentclass;
  }

  public function getAccessibility(){
    return $this->accessibility;
  }

  public function getQuality(){
    return $this->quality;
  }

  public function getOperations(){
    return $this->operations;
  }

  public function getAdded(){
    return $this->added;
  }

  public function getMantyAge(){
    return $this->mantyAge;
  }

  public function getMantyVolume(){
    return $this->mantyVolume;
  }

  public function getMantyVolumeHa(){
    return $this->mantyVolumeHa;
  }

  public function getMantyLogVolume(){
    return $this->mantyLogVolume;
  }

  public function getMantyPulpVolume(){
    return $this->mantyPulpVolume;
  }

  public function getMantyDiameter(){
    return $this->mantyDiameter;
  }

  public function getMantyLength(){
    return $this->mantyLength;
  }

  public function getMantyDensity(){
    return $this->mantyDensity;
  }

  public function getMantyBasalarea(){
    return $this->mantyBasalarea;
  }

  public function getMantyGrowth(){
    return $this->mantyGrowth;
  }

  public function getKuusiAge(){
    return $this->kuusiAge;
  }

  public function getKuusiVolume(){
    return $this->kuusiVolume;
  }

  public function getKuusiVolumeHa(){
    return $this->kuusiVolumeHa;
  }

  public function getKuusiLogVolume(){
    return $this->kuusiLogVolume;
  }

  public function getKuusiPulpVolume(){
    return $this->kuusiPulpVolume;
  }

  public function getKuusiDiameter(){
    return $this->kuusiDiameter;
  }

  public function getKuusiLength(){
    return $this->kuusiLength;
  }

  public function getKuusiDensity(){
    return $this->kuusiDensity;
  }

  public function getKuusiBasalarea(){
    return $this->kuusiBasalarea;
  }

  public function getKuusiGrowth(){
    return $this->kuusiGrowth;
  }

  public function getRauduskoivuAge(){
    return $this->rauduskoivuAge;
  }

  public function getRauduskoivuVolume(){
    return $this->rauduskoivuVolume;
  }

  public function getRauduskoivuVolumeHa(){
    return $this->rauduskoivuVolumeHa;
  }

  public function getRauduskoivuLogVolume(){
    return $this->rauduskoivuLogVolume;
  }

  public function getRauduskoivuPulpVolume(){
    return $this->rauduskoivuPulpVolume;
  }

  public function getRauduskoivuDiameter(){
    return $this->rauduskoivuDiameter;
  }

  public function getRauduskoivuLength(){
    return $this->rauduskoivuLength;
  }

  public function getRauduskoivuDensity(){
    return $this->rauduskoivuDensity;
  }

  public function getRauduskoivuBasalarea(){
    return $this->rauduskoivuBasalarea;
  }

  public function getRauduskoivuGrowth(){
    return $this->rauduskoivuGrowth;
  }

  public function getHieskoivuAge(){
    return $this->hieskoivuAge;
  }

  public function getHieskoivuVolume(){
    return $this->hieskoivuVolume;
  }

  public function getHieskoivuVolumeHa(){
    return $this->hieskoivuVolumeHa;
  }

  public function getHieskoivuLogVolume(){
    return $this->hieskoivuLogVolume;
  }

  public function getHieskoivuPulpVolume(){
    return $this->hieskoivuPulpVolume;
  }

  public function getHieskoivuDiameter(){
    return $this->hieskoivuDiameter;
  }

  public function getHieskoivuLength(){
    return $this->hieskoivuLength;
  }

  public function getHieskoivuDensity(){
    return $this->hieskoivuDensity;
  }

  public function getHieskoivuBasalarea(){
    return $this->hieskoivuBasalarea;
  }

  public function getHieskoivuGrowth(){
    return $this->hieskoivuGrowth;
  }

  public function getHaapaAge(){
    return $this->haapaAge;
  }

  public function getHaapaVolume(){
    return $this->haapaVolume;
  }

  public function getHaapaVolumeHa(){
    return $this->haapaVolumeHa;
  }

  public function getHaapaLogVolume(){
    return $this->haapaLogVolume;
  }

  public function getHaapaPulpVolume(){
    return $this->haapaPulpVolume;
  }

  public function getHaapaDiameter(){
    return $this->haapaDiameter;
  }

  public function getHaapaLength(){
    return $this->haapaLength;
  }

  public function getHaapaDensity(){
    return $this->haapaDensity;
  }

  public function getHaapaBasalarea(){
    return $this->haapaBasalarea;
  }

  public function getHaapaGrowth(){
    return $this->haapaGrowth;
  }

  public function getHarmaaleppaAge(){
    return $this->harmaaleppaAge;
  }

  public function getHarmaaleppaVolume(){
    return $this->harmaaleppaVolume;
  }

  public function getHarmaaleppaVolumeHa(){
    return $this->harmaaleppaVolumeHa;
  }

  public function getHarmaaleppaLogVolume(){
    return $this->harmaaleppaLogVolume;
  }

  public function getHarmaaleppaPulpVolume(){
    return $this->harmaaleppaPulpVolume;
  }

  public function getHarmaaleppaDiameter(){
    return $this->harmaaleppaDiameter;
  }

  public function getHarmaaleppaLength(){
    return $this->harmaaleppaLength;
  }

  public function getHarmaaleppaDensity(){
    return $this->harmaaleppaDensity;
  }

  public function getHarmaaleppaBasalarea(){
    return $this->harmaaleppaBasalarea;
  }

  public function getHarmaaleppaGrowth(){
    return $this->harmaaleppaGrowth;
  }

  public function getTervaleppaAge(){
    return $this->tervaleppaAge;
  }

  public function getTervaleppaVolume(){
    return $this->tervaleppaVolume;
  }

  public function getTervaleppaVolumeHa(){
    return $this->tervaleppaVolumeHa;
  }

  public function getTervaleppaLogVolume(){
    return $this->tervaleppaLogVolume;
  }

  public function getTervaleppaPulpVolume(){
    return $this->tervaleppaPulpVolume;
  }

  public function getTervaleppaDiameter(){
    return $this->tervaleppaDiameter;
  }

  public function getTervaleppaLength(){
    return $this->tervaleppaLength;
  }

  public function getTervaleppaDensity(){
    return $this->tervaleppaDensity;
  }

  public function getTervaleppaBasalarea(){
    return $this->tervaleppaBasalarea;
  }

  public function getTervaleppaGrowth(){
    return $this->tervaleppaGrowth;
  }

  public function getLehtipuuAge(){
    return $this->lehtipuuAge;
  }

  public function getLehtipuuVolume(){
    return $this->lehtipuuVolume;
  }

  public function getLehtipuuVolumeHa(){
    return $this->lehtipuuVolumeHa;
  }

  public function getLehtipuuLogVolume(){
    return $this->lehtipuuLogVolume;
  }

  public function getLehtipuuPulpVolume(){
    return $this->lehtipuuPulpVolume;
  }

  public function getLehtipuuDiameter(){
    return $this->lehtipuuDiameter;
  }

  public function getLehtipuuLength(){
    return $this->lehtipuuLength;
  }

  public function getLehtipuuDensity(){
    return $this->lehtipuuDensity;
  }

  public function getLehtipuuBasalarea(){
    return $this->lehtipuuBasalarea;
  }

  public function getLehtipuuGrowth(){
    return $this->lehtipuuGrowth;
  }

  public function getHavupuuAge(){
    return $this->havupuuAge;
  }

  public function getHavupuuVolume(){
    return $this->havupuuVolume;
  }

  public function getHavupuuVolumeHa(){
    return $this->havupuuVolumeHa;
  }

  public function getHavupuuLogVolume(){
    return $this->havupuuLogVolume;
  }

  public function getHavupuuPulpVolume(){
    return $this->havupuuPulpVolume;
  }

  public function getHavupuuDiameter(){
    return $this->havupuuDiameter;
  }

  public function getHavupuuLength(){
    return $this->havupuuLength;
  }

  public function getHavupuuDensity(){
    return $this->havupuuDensity;
  }

  public function getHavupuuBasalarea(){
    return $this->havupuuBasalarea;
  }

  public function getHavupuuGrowth(){
    return $this->havupuuGrowth;
  }

  public function getTotalAge() {
      return $this->totalAge;
  }

  public function getTotalVolume() {
      return $this->totalVolume;
  }

  public function getTotalVolumeHa() {
      return $this->totalVolumeHa;
  }

  public function getTotalLogVolume() {
      return $this->totalLogVolume;
  }

  public function getTotalPulpVolume() {
      return $this->totalPulpVolume;
  }

  public function getTotalDiameter() {
      return $this->totalDiameter;
  }

  public function getTotalLength() {
      return $this->totalLength;
  }

  public function getTotalDensity() {
      return $this->totalDensity;
  }

  public function getTotalBasalarea() {
      return $this->totalBasalarea;
  }

  public function getTotalGrowth() {
      return $this->totalGrowth;
  }

  public function getAdditionality5yearsHa() {
      return $this->additionality5yearsHa;
  }

  public function getAdditionality10yearsHa() {
      return $this->additionality10yearsHa;
  }

  public function getAdditionality5yearsPercentage() {
      return $this->additionality5yearsPercentage;
  }

  public function getAdditionality10yearsPercentage() {
      return $this->additionality10yearsPercentage;
  }

  /**
   * Get property
   *
   * @return \App\Entity\Property
   */
    public function getProperty()
    {
        return $this->property;
    }

  public function setArea($area){
    $this->area = $area;
  }
  
  public function setStandNumber($standNumber){
    $this->standNumber = $standNumber;
  }

  public function setSubgroupNumber($subgroupNumber){
    $this->subgroupNumber = $subgroupNumber;
  }

  public function setFertilityclassNumber($fertilityclassNumber){
    $this->fertilityclassNumber = $fertilityclassNumber;
  }

  public function setMainclass($mainclass){
    $this->mainclass = $mainclass;
  }

  public function setGrowplace($growplace){
    $this->growplace = $growplace;
  }

  public function setSoiltype($soiltype){
    $this->soiltype = $soiltype;
  }

  public function setDevelopmentclass($developmentclass){
    $this->developmentclass = $developmentclass;
  }

  public function setAccessibility($accessibility){
    $this->accessibility = $accessibility;
  }

  public function setQuality($quality){
    $this->quality = $quality;
  }

  public function setOperations($operations){
    $this->operations = $operations;
  }

  public function setAdded($added){
    $this->added = $added;
  }

  public function setMantyAge($mantyAge){
    $this->mantyAge = $mantyAge;
  }

  public function setMantyVolume($mantyVolume){
    $this->mantyVolume = $mantyVolume;
  }

  public function setMantyVolumeHa($mantyVolumeHa){
    $this->mantyVolumeHa = $mantyVolumeHa;
  }

  public function setMantyLogVolume($mantyLogVolume){
    $this->mantyLogVolume = $mantyLogVolume;
  }

  public function setMantyPulpVolume($mantyPulpVolume){
    $this->mantyPulpVolume = $mantyPulpVolume;
  }

  public function setMantyDiameter($mantyDiameter){
    $this->mantyDiameter = $mantyDiameter;
  }

  public function setMantyLength($mantyLength){
    $this->mantyLength = $mantyLength;
  }

  public function setMantyDensity($mantyDensity){
    $this->mantyDensity = $mantyDensity;
  }

  public function setMantyBasalarea($mantyBasalarea){
    $this->mantyBasalarea = $mantyBasalarea;
  }

  public function setMantyGrowth($mantyGrowth){
    $this->mantyGrowth = $mantyGrowth;
  }

  public function setKuusiAge($kuusiAge){
    $this->kuusiAge = $kuusiAge;
  }

  public function setKuusiVolume($kuusiVolume){
    $this->kuusiVolume = $kuusiVolume;
  }

  public function setKuusiVolumeHa($kuusiVolumeHa){
    $this->kuusiVolumeHa = $kuusiVolumeHa;
  }

  public function setKuusiLogVolume($kuusiLogVolume){
    $this->kuusiLogVolume = $kuusiLogVolume;
  }

  public function setKuusiPulpVolume($kuusiPulpVolume){
    $this->kuusiPulpVolume = $kuusiPulpVolume;
  }

  public function setKuusiDiameter($kuusiDiameter){
    $this->kuusiDiameter = $kuusiDiameter;
  }

  public function setKuusiLength($kuusiLength){
    $this->kuusiLength = $kuusiLength;
  }

  public function setKuusiDensity($kuusiDensity){
    $this->kuusiDensity = $kuusiDensity;
  }

  public function setKuusiBasalarea($kuusiBasalarea){
    $this->kuusiBasalarea = $kuusiBasalarea;
  }

  public function setKuusiGrowth($kuusiGrowth){
    $this->kuusiGrowth = $kuusiGrowth;
  }

  public function setRauduskoivuAge($rauduskoivuAge){
    $this->rauduskoivuAge = $rauduskoivuAge;
  }

  public function setRauduskoivuVolume($rauduskoivuVolume){
    $this->rauduskoivuVolume = $rauduskoivuVolume;
  }

  public function setRauduskoivuVolumeHa($rauduskoivuVolumeHa){
    $this->rauduskoivuVolumeHa = $rauduskoivuVolumeHa;
  }

  public function setRauduskoivuLogVolume($rauduskoivuLogVolume){
    $this->rauduskoivuLogVolume = $rauduskoivuLogVolume;
  }

  public function setRauduskoivuPulpVolume($rauduskoivuPulpVolume){
    $this->rauduskoivuPulpVolume = $rauduskoivuPulpVolume;
  }

  public function setRauduskoivuDiameter($rauduskoivuDiameter){
    $this->rauduskoivuDiameter = $rauduskoivuDiameter;
  }

  public function setRauduskoivuLength($rauduskoivuLength){
    $this->rauduskoivuLength = $rauduskoivuLength;
  }

  public function setRauduskoivuDensity($rauduskoivuDensity){
    $this->rauduskoivuDensity = $rauduskoivuDensity;
  }

  public function setRauduskoivuBasalarea($rauduskoivuBasalarea){
    $this->rauduskoivuBasalarea = $rauduskoivuBasalarea;
  }

  public function setRauduskoivuGrowth($rauduskoivuGrowth){
    $this->rauduskoivuGrowth = $rauduskoivuGrowth;
  }

  public function setHieskoivuAge($hieskoivuAge){
    $this->hieskoivuAge = $hieskoivuAge;
  }

  public function setHieskoivuVolume($hieskoivuVolume){
    $this->hieskoivuVolume = $hieskoivuVolume;
  }

  public function setHieskoivuVolumeHa($hieskoivuVolumeHa){
    $this->hieskoivuVolumeHa = $hieskoivuVolumeHa;
  }

  public function setHieskoivuLogVolume($hieskoivuLogVolume){
    $this->hieskoivuLogVolume = $hieskoivuLogVolume;
  }

  public function setHieskoivuPulpVolume($hieskoivuPulpVolume){
    $this->hieskoivuPulpVolume = $hieskoivuPulpVolume;
  }

  public function setHieskoivuDiameter($hieskoivuDiameter){
    $this->hieskoivuDiameter = $hieskoivuDiameter;
  }

  public function setHieskoivuLength($hieskoivuLength){
    $this->hieskoivuLength = $hieskoivuLength;
  }

  public function setHieskoivuDensity($hieskoivuDensity){
    $this->hieskoivuDensity = $hieskoivuDensity;
  }

  public function setHieskoivuBasalarea($hieskoivuBasalarea){
    $this->hieskoivuBasalarea = $hieskoivuBasalarea;
  }

  public function setHieskoivuGrowth($hieskoivuGrowth){
    $this->hieskoivuGrowth = $hieskoivuGrowth;
  }

  public function setHaapaAge($haapaAge){
    $this->haapaAge = $haapaAge;
  }

  public function setHaapaVolume($haapaVolume){
    $this->haapaVolume = $haapaVolume;
  }

  public function setHaapaVolumeHa($haapaVolumeHa){
    $this->haapaVolumeHa = $haapaVolumeHa;
  }

  public function setHaapaLogVolume($haapaLogVolume){
    $this->haapaLogVolume = $haapaLogVolume;
  }

  public function setHaapaPulpVolume($haapaPulpVolume){
    $this->haapaPulpVolume = $haapaPulpVolume;
  }

  public function setHaapaDiameter($haapaDiameter){
    $this->haapaDiameter = $haapaDiameter;
  }

  public function setHaapaLength($haapaLength){
    $this->haapaLength = $haapaLength;
  }

  public function setHaapaDensity($haapaDensity){
    $this->haapaDensity = $haapaDensity;
  }

  public function setHaapaBasalarea($haapaBasalarea){
    $this->haapaBasalarea = $haapaBasalarea;
  }

  public function setHaapaGrowth($haapaGrowth){
    $this->haapaGrowth = $haapaGrowth;
  }

  public function setHarmaaleppaAge($harmaaleppaAge){
    $this->harmaaleppaAge = $harmaaleppaAge;
  }

  public function setHarmaaleppaVolume($harmaaleppaVolume){
    $this->harmaaleppaVolume = $harmaaleppaVolume;
  }

  public function setHarmaaleppaVolumeHa($harmaaleppaVolumeHa){
    $this->harmaaleppaVolumeHa = $harmaaleppaVolumeHa;
  }

  public function setHarmaaleppaLogVolume($harmaaleppaLogVolume){
    $this->harmaaleppaLogVolume = $harmaaleppaLogVolume;
  }

  public function setharmaaleppaPulpVolume($harmaaleppaPulpVolume){
    $this->harmaaleppaPulpVolume = $harmaaleppaPulpVolume;
  }

  public function setharmaaleppaDiameter($harmaaleppaDiameter){
    $this->harmaaleppaDiameter = $harmaaleppaDiameter;
  }

  public function setharmaaleppaLength($harmaaleppaLength){
    $this->harmaaleppaLength = $harmaaleppaLength;
  }

  public function setHarmaaleppaDensity($harmaaleppaDensity){
    $this->harmaaleppaDensity = $harmaaleppaDensity;
  }

  public function setHarmaaleppaBasalarea($harmaaleppaBasalarea){
    $this->harmaaleppaBasalarea = $harmaaleppaBasalarea;
  }

  public function setharmaaleppaGrowth($harmaaleppaGrowth){
    $this->harmaaleppaGrowth = $harmaaleppaGrowth;
  }

  public function setTervaleppaAge($tervaleppaAge){
    $this->tervaleppaAge = $tervaleppaAge;
  }

  public function setTervaleppaVolume($tervaleppaVolume){
    $this->tervaleppaVolume = $tervaleppaVolume;
  }

  public function setTervaleppaVolumeHa($tervaleppaVolumeHa){
    $this->tervaleppaVolumeHa = $tervaleppaVolumeHa;
  }

  public function setTervaleppaLogVolume($tervaleppaLogVolume){
    $this->tervaleppaLogVolume = $tervaleppaLogVolume;
  }

  public function setTervaleppaPulpVolume($tervaleppaPulpVolume){
    $this->tervaleppaPulpVolume = $tervaleppaPulpVolume;
  }

  public function setTervaleppaDiameter($tervaleppaDiameter){
    $this->tervaleppaDiameter = $tervaleppaDiameter;
  }

  public function setTervaleppaLength($tervaleppaLength){
    $this->tervaleppaLength = $tervaleppaLength;
  }

  public function setTervaleppaDensity($tervaleppaDensity){
    $this->tervaleppaDensity = $tervaleppaDensity;
  }

  public function setTervaleppaBasalarea($tervaleppaBasalarea){
    $this->tervaleppaBasalarea = $tervaleppaBasalarea;
  }

  public function setTervaleppaGrowth($tervaleppaGrowth){
    $this->tervaleppaGrowth = $tervaleppaGrowth;
  }

  public function setHavupuuAge($havupuuAge){
    $this->havupuuAge = $havupuuAge;
  }

  public function setHavupuuVolume($havupuuVolume){
    $this->havupuuVolume = $havupuuVolume;
  }

  public function setHavupuuVolumeHa($havupuuVolumeHa){
    $this->havupuuVolumeHa = $havupuuVolumeHa;
  }

  public function setHavupuuLogVolume($havupuuLogVolume){
    $this->havupuuLogVolume = $havupuuLogVolume;
  }

  public function setHavupuuPulpVolume($havupuuPulpVolume){
    $this->havupuuPulpVolume = $havupuuPulpVolume;
  }

  public function setHavupuuDiameter($havupuuDiameter){
    $this->havupuuDiameter = $havupuuDiameter;
  }

  public function setHavupuuLength($havupuuLength){
    $this->havupuuLength = $havupuuLength;
  }

  public function setHavupuuDensity($havupuuDensity){
    $this->havupuuDensity = $havupuuDensity;
  }

  public function setHavupuuBasalarea($havupuuBasalarea){
    $this->havupuuBasalarea = $havupuuBasalarea;
  }

  public function setHavupuuGrowth($havupuuGrowth){
    $this->havupuuGrowth = $havupuuGrowth;
  }

  public function setLehtipuuAge($lehtipuuAge){
    $this->lehtipuuAge = $lehtipuuAge;
  }

  public function setLehtipuuVolume($lehtipuuVolume){
    $this->lehtipuuVolume = $lehtipuuVolume;
  }

  public function setLehtipuuVolumeHa($lehtipuuVolumeHa){
    $this->lehtipuuVolumeHa = $lehtipuuVolumeHa;
  }

  public function setLehtipuuLogVolume($lehtipuuLogVolume){
    $this->lehtipuuLogVolume = $lehtipuuLogVolume;
  }

  public function setLehtipuuPulpVolume($lehtipuuPulpVolume){
    $this->lehtipuuPulpVolume = $lehtipuuPulpVolume;
  }

  public function setLehtipuuDiameter($lehtipuuDiameter){
    $this->lehtipuuDiameter = $lehtipuuDiameter;
  }

  public function setLehtipuuLength($lehtipuuLength){
    $this->lehtipuuLength = $lehtipuuLength;
  }

  public function setLehtipuuDensity($lehtipuuDensity){
    $this->lehtipuuDensity = $lehtipuuDensity;
  }

  public function setLehtipuuBasalarea($lehtipuuBasalarea){
    $this->lehtipuuBasalarea = $lehtipuuBasalarea;
  }

  public function setLehtipuuGrowth($lehtipuuGrowth){
    $this->lehtipuuGrowth = $lehtipuuGrowth;
  }

  public function setTotalAge($totalAge){
    $this->totalAge = $totalAge;
  }

  public function setTotalVolume($totalVolume){
    $this->totalVolume = $totalVolume;
  }

  public function setTotalVolumeHa($totalVolumeHa){
    $this->totalVolumeHa = $totalVolumeHa;
  }

  public function setTotalLogVolume($totalLogVolume){
    $this->totalLogVolume = $totalLogVolume;
  }

  public function setTotalPulpVolume($totalPulpVolume){
    $this->totalPulpVolume = $totalPulpVolume;
  }

  public function setTotalDiameter($totalDiameter){
    $this->totalDiameter = $totalDiameter;
  }

  public function setTotalLength($totalLength){
    $this->totalLength = $totalLength;
  }

  public function setTotalDensity($totalDensity){
    $this->totalDensity = $totalDensity;
  }

  public function setTotalBasalarea($totalBasalarea){
    $this->totalBasalarea = $totalBasalarea;
  }

  public function setTotalGrowth($totalGrowth){
    $this->totalGrowth = $totalGrowth;
  }

  public function setAdditionality5yearsHa($additionality5yearsHa){
    $this->additionality5yearsHa = $additionality5yearsHa;
  }

  public function setAdditionality10yearsHa($additionality10yearsHa){
    $this->additionality10yearsHa = $additionality10yearsHa;
  }

  public function setAdditionality5yearsPercentage($additionality5yearsPercentage){
    $this->additionality5yearsPercentage = $additionality5yearsPercentage;
  }

  public function setAdditionality10yearsPercentage($additionality10yearsPercentage){
    $this->additionality10yearsPercentage = $additionality10yearsPercentage;
  }

  /**
 * Set property
 *
 * @param \App\Entity\Property $property
 *
 * @return Property
 */
  public function setProperty(\App\Entity\Property $property = null)
  {
      $this->property = $property;

      return $this;
  }


}
