<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $name;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $businessId;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $industry;
    /**
   *
   * @ORM\ManyToMany(targetEntity="User", inversedBy="companies")
   * @ORM\JoinTable(
   *  name="user_companies",
   *  joinColumns={
   *      @ORM\JoinColumn(name="company_id", referencedColumnName="id")
   *  },
   *  inverseJoinColumns={
   *      @ORM\JoinColumn(name="user_id", referencedColumnName="id")
   *  }
   * )
   */
  protected $users;
  /**
   * Default constructor, initializes collections
   */
  public function __construct()
  {
      $this->users = new ArrayCollection();
  }

   //Getters & Setters
  public function getId(){
    return $this->id;
  }

  public function getBusinessId(){
    return $this->businessId;
  }

  public function setBusinessId($businessId){
    $this->businessId = $businessId;
  }

  public function getName(){
    return $this->name;
  }

  public function setName($name){
    $this->name = $name;
  }

  public function getIndustry(){
    return $this->industry;
  }

  public function setIndustry($industry){
    $this->industry = $industry;
  }

  /**
   * Get users
   *
   * @return \Doctrine\Common\Collections\Collection
   */
    public function getUsers()
    {
        return $this->users;
    }

  /**
 * @param User $user
 */
  public function addUser(User $user)
  {
      if ($this->users->contains($user)) {
          return;
      }
      $this->users->add($user);
      $user->addCompany($this);
  }

  /**
 * @param User $user
 */
  public function removeUser(User $user)
  {
      if (!$this->users->contains($user)) {
          return;
      }
      $this->users->removeElement($user);
      $user->removeCompany($this);
  }

}
