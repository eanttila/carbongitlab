<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PropertyRepository")
 */
class Property
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $address;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $identifier;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $owner;

  /**
  * @ORM\ManyToOne(targetEntity="User", inversedBy="properties")
  * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
  */
  private $user;

  /**
  * @ORM\OneToMany(targetEntity="Article", mappedBy="property")
  */
  private $articles;

  /**
  * @ORM\OneToMany(targetEntity="ImportedArticle", mappedBy="property")
  */
  private $importedArticles;

  public function __construct()
  {
      $this->articles = new ArrayCollection();
  }

   //Getters & Setters
  public function getId(){
    return $this->id;
  }

  public function getAddress(){
    return $this->address;
  }

  public function getIdentifier(){
    return $this->identifier;
  }

  public function getOwner(){
    return $this->owner;
  }

  public function setAddress($address){
    $this->address = $address;
  }

  public function setIdentifier($identifier){
    $this->identifier = $identifier;
  }

  public function setOwner($owner){
    $this->owner = $owner;
  }


  /**
   * Add article
   *
   * @param \App\Entity\Article $article
   *
   * @return Category
   */
  public function addArticle(\App\Entity\Article $article)
  {
      $this->articles[] = $article;

      return $this;
  }

  /**
   * Remove article
   *
   * @param \App\Entity\Article $article
   */
  public function removeArticle(\App\Entity\Article $article)
  {
      $this->articles->removeElement($article);
  }

  /**
   * Get imported articles
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getImportedArticles()
  {
	$importedArticles = $this->importedArticles->toArray();
	 
	usort($importedArticles, function($a, $b) {
      return strnatcmp($a->getStandNumber(), $b->getStandNumber());
	});
	  
	return $importedArticles;
  }

  /**
   * Get articles
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getArticles()
  {
      return $this->articles;
  }

  /**
 * Get user
 *
 * @return \App\Entity\User
 */
  public function getUser()
  {
      return $this->user;
  }

  /**
 * Set user
 *
 * @param \App\Entity\User $user
 *
 * @return User
 */
  public function setUser(\App\Entity\User $user = null)
  {
      $this->user = $user;

      return $this;
  }

}
