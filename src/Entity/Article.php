<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @Assert\Length(max=1000)
    * @ORM\Column(type="decimal", scale=2)
   */
   private $area;

   /**
   * @ORM\Column(type="text", length=100)
   */
   private $place;

   /**
   * @ORM\Column(type="decimal", scale=2)
  */
  private $height;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $sort;

  /**
  * @Assert\Length(max=200)
   * @ORM\Column(type="integer")
 */
  private $age;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $development_class;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $mat;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $kut;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $kot;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $mut;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $mak;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $kuk;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $kok;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $muk;

  /**
  * @Assert\Length(max=1000)
   * @ORM\Column(type="integer")
 */
  private $enp;

  /**
  * @ORM\ManyToOne(targetEntity="User", inversedBy="articles")
  * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
  */
  private $user;

  /**
  * @ORM\ManyToOne(targetEntity="Property", inversedBy="articles")
  * @ORM\JoinColumn(name="property_id", referencedColumnName="id")
  */
  private $property;

   //Getters & Setters
  public function getId(){
    return $this->id;
  }

  public function getArea(){
    return $this->area;
  }

  public function getPlace(){
    return $this->place;
  }

  public function getHeight(){
    return $this->height;
  }

  public function getSort(){
    return $this->sort;
  }

  public function getAge(){
    return $this->age;
  }

  public function getDevelopment_class(){
    return $this->development_class;
  }

  public function getMat(){
    return $this->mat;
  }

  public function getKut(){
    return $this->kut;
  }

  public function getKot(){
    return $this->kot;
  }

  public function getMut(){
    return $this->mut;
  }

  public function getMak(){
    return $this->mak;
  }

  public function getKuk(){
    return $this->kuk;
  }

  public function getKok(){
    return $this->kok;
  }

  public function getMuk(){
    return $this->muk;
  }

  public function getEnp(){
    return $this->enp;
  }

  /**
   * Get property
   *
   * @return \App\Entity\Property
   */
    public function getProperty()
    {
        return $this->property;
    }

/**
 * Get user
 *
 * @return \App\Entity\User
 */
  public function getUser()
  {
      return $this->user;
  }

  public function setArea($area){
    $this->area = $area;
  }

  public function setPlace($place){
    $this->place = $place;
  }

  public function setHeight($height){
    $this->height = $height;
  }

  public function setSort($sort){
    $this->sort = $sort;
  }

  public function setAge($age){
    $this->age = $age;
  }

  public function setDevelopment_class($development_class){
    $this->development_class = $development_class;
  }

  public function setMat($mat){
    $this->mat = $mat;
  }

  public function setKut($kut){
    $this->kut = $kut;
  }

  public function setKot($kot){
    $this->kot = $kot;
  }

  public function setMut($mut){
    $this->mut = $mut;
  }

  public function setMak($mak){
    $this->mak = $mak;
  }

  public function setKuk($kuk){
    $this->kuk = $kuk;
  }

  public function setKok($kok){
    $this->kok = $kok;
  }

  public function setMuk($muk){
    $this->muk = $muk;
  }

  public function setEnp($enp){
    $this->enp = $enp;
  }

  /**
 * Set property
 *
 * @param \App\Entity\Property $property
 *
 * @return Property
 */
  public function setProperty(\App\Entity\Property $property = null)
  {
      $this->property = $property;

      return $this;
  }

  /**
 * Set user
 *
 * @param \App\Entity\User $user
 *
 * @return User
 */
  public function setUser(\App\Entity\User $user = null)
  {
      $this->user = $user;

      return $this;
  }

}
