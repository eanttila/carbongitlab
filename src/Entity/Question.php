<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 */
class Question
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
  * @ORM\Column(type="text", length=1000)
  */
  private $content;

  /**
  * @ORM\Column(type="text", length=1000)
  */
  private $formName;

  /**
  * @ORM\Column(type="text", length=100)
  */
  private $tabName;

  /**
  * @ORM\ManyToOne(targetEntity="Company", inversedBy="questions")
  * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
  */
  private $company;

  /**
  * @ORM\ManyToOne(targetEntity="Factor", inversedBy="questions")
  * @ORM\JoinColumn(name="factor_id", referencedColumnName="id")
  */
  private $factor;


   //Getters & Setters
  public function getId() {
    return $this->id;
  }

  public function getContent() {
    return $this->content;
  }

  public function setContent($content){
    $this->content = $content;
  }

  public function getFormName() {
    return $this->formName;
  }

  public function setFormName($formName){
    $this->formName = $formName;
  }

  public function getTabName() {
    return $this->tabName;
  }

  public function setTabName($tabName) {
    $this->tabName = $tabName;
  }

  /**
   * Get factor
   *
   * @return \App\Entity\Factor
   */
    public function getFactor()
    {
        return $this->factor;
    }

    /**
   * Set factor
   *
   * @param \App\Entity\Factor $factor
   *
   * @return Factor
   */
    public function setFactor(\App\Entity\Factor $factor = null)
    {
        $this->factor = $factor;

        return $this;
    }

    /**
     * Get company
     *
     * @return \App\Entity\Company
     */
      public function getCompany()
      {
          return $this->company;
      }

      /**
     * Set company
     *
     * @param \App\Entity\Company $company
     *
     * @return Company
     */
      public function setCompany(\App\Entity\Company $company = null)
      {
          $this->company = $company;

          return $this;
      }

}
