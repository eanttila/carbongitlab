<?php

// src/Entity/User.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\Length(max=190)
     * @ORM\Column(type="string", length=190, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
    * @ORM\Column(type="text", length=100)
    */
    private $name;

    /**
    * @ORM\Column(type="text", length=100)
    */
    private $role;

    /**
    * @ORM\Column(type="text", length=100)
    */
    private $language;

    /**
    * @ORM\Column(type="text", length=100)
    */
    private $phone;

    /**
    * @ORM\Column(type="text", length=100)
    */
    private $address;

    /**
    * @ORM\Column(type="text", length=100)
    */
    private $zip;

    /**
    * @ORM\Column(type="text", length=100)
    */
    private $city;

    /**
    * @Assert\Length(max=200)
   * @ORM\Column(type="integer")
   */
    private $joined;

    /**
     * @Assert\Length(max=190)
     * @ORM\Column(type="string", length=190, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=190)
     */
    private $plainPassword;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
    * @ORM\OneToMany(targetEntity="Article", mappedBy="user")
    */
    private $articles;

    /**
    * @ORM\OneToMany(targetEntity="Property", mappedBy="user")
    */
    private $properties;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Company", mappedBy="users")
     */
    protected $companies;


    public function __construct()
    {
        $this->roles = array('ROLE_USER');
        $this->articles = new ArrayCollection();
        $this->properties = new ArrayCollection();
        $this->companies = new ArrayCollection();
    }

    /**
     * @param Company $company
     */
    public function addCompany(Company $company)
    {
        if ($this->companies->contains($company)) {
            return;
        }
        $this->companies->add($company);
        $company->addUser($this);
    }

    /**
   * @param UserGroup $userGroup
   */
    public function removeCompany(Company $company)
    {
        if (!$this->companies->contains($company)) {
            return;
        }
        $this->companies->removeElement($company);
        $company->removeUser($this);
    }

    /**
     * Add article
     *
     * @param \App\Entity\Article $article
     *
     * @return Category
     */
    public function addArticle(\App\Entity\Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \App\Entity\Article $article
     */
    public function removeArticle(\App\Entity\Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add property
     *
     * @param \App\Entity\Property $property
     *
     * @return Category
     */
    public function addProperty(\App\Entity\Property $property)
    {
        $this->properties[] = $property;

        return $this;
    }

    /**
     * Remove property
     *
     * @param \App\Entity\Property $property
     */
    public function removeProperty(\App\Entity\Property $property)
    {
        $this->properties->removeElement($property);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
   * Get articles
   *
   * @return \Doctrine\Common\Collections\Collection
   */
    public function getCompanies()
    {
        return $this->companies;
    }

    // other properties and methods

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getJoined()
    {
        return $this->joined;
    }

    public function setJoined($joined)
    {
        $this->joined = $joined;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        // The bcrypt and argon2i algorithms don't require a separate salt.
        // You *may* need a real salt if you choose a different encoder.
        return null;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
    }
}
