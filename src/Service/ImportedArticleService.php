<?php

namespace App\Service;

use App\Entity\Property;
use App\Entity\ImportedArticle;
use Doctrine\ORM\EntityManager;


class ImportedArticleService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getProperty($property_id)
    {
        return $this->find($property_id);
    }

    public function getAllProperties()
    {
        return $this->findAll();
    }

    public function searchByPropertyIdByImportedArticleId($importedArticleId) {

      $sql = "SELECT property_id FROM imported_article WHERE id = '".$importedArticleId."'";
      $stmt = $this->em->getConnection()->prepare($sql);
      $stmt->execute();
      $propertyIdResults = $stmt->fetchAll();

      return $propertyIdResults[0]['property_id'];
    }

    public function getForestKitMainclass($maingroup) {

      if ($maingroup == '1') {
        $mainclass = 'Metsämaa';
      } else if ($maingroup == '2') {
        $mainclass = 'Kitumaa';
      } else if ($maingroup == '3') {
        $mainclass = 'Joutomaa';
      } else if ($maingroup == '4') {
        $mainclass = 'Muu metsätalousmaa';
      } else if ($maingroup == '5') {
        $mainclass = 'Tontti';
      } else if ($maingroup == '6') {
        $mainclass = 'Maatalousmaa';
      } else if ($maingroup == '7') {
        $mainclass = 'Muu maa';
      } else if ($maingroup == '8') {
        $mainclass = 'Vesistö';
      } else {
        $mainclass = 'Muu';
      }

      return $mainclass;
    }

    public function getForestKitGrowplace($fertilityClass) {

      if ($fertilityClass == '1') {
        $growplace = 'Lehto, letto ja lehtomainen suo (ja ruohoturvekangas)';
      } else if ($fertilityClass == '2') {
        $growplace = 'Lehtomainen kangas, vastaava suo ja ruohoturvekangas';
      } else if ($fertilityClass == '3') {
        $growplace = 'Tuore kangas, vastaava suo ja mustikkaturvekangas';
      } else if ($fertilityClass == '4') {
        $growplace = 'Kuivahko kangas, vastaava suo ja puolukkaturvekangas';
      } else if ($fertilityClass == '5') {
        $growplace = 'Kuiva kangas, vastaava suo ja varputurvekangas';
      } else if ($fertilityClass == '6') {
        $growplace = 'Karukkokangas, vastaava suo (ja jäkäläturvekangas)';
      } else if ($fertilityClass == '7') {
        $growplace = 'Kalliomaa ja hietikko';
      } else if ($fertilityClass == '8') {
        $growplace = 'Lakimetsä ja tunturi';
      } else {
        $growplace = 'Muu';
      }

      return $growplace;
    }

    public function getForestKitSoiltype($soiltype) {

      if ($soiltype == '10') {
        $soiltypeResult = 'Keskikarkea tai karkea kangasmaa';
      } else if ($soiltype == '11') {
        $soiltypeResult = 'Karkea moreeni';
      } else if ($soiltype == '12') {
        $soiltypeResult = 'Karkea lajittunut maalaji';
      } else if ($soiltype == '20') {
        $soiltypeResult = 'Hienojakoinen kangasmaa';
      } else if ($soiltype == '21') {
        $soiltypeResult = 'Hienoainesmoreeni';
      } else if ($soiltype == '22') {
        $soiltypeResult = 'Hienojakoinen lajittunut maalaji';
      } else if ($soiltype == '23') {
        $soiltypeResult = 'Silttipitoinen maalaji';
      } else if ($soiltype == '24') {
        $soiltypeResult = 'Savimaa';
      } else if ($soiltype == '30') {
        $soiltypeResult = 'Kivinen keskikarkea tai karkea kangasmaa';
      } else if ($soiltype == '31') {
        $soiltypeResult = 'Kivinen karkea moreeni';
      } else if ($soiltype == '32') {
        $soiltypeResult = 'Kivinen karkea lajittunut maalaji';
      } else if ($soiltype == '40') {
        $soiltypeResult = 'Kivinen hienojakoinen kangasmaa';
      } else if ($soiltype == '50') {
        $soiltypeResult = 'Kallio tai kivikko';
      } else if ($soiltype == '60') {
        $soiltypeResult = 'Turvemaa';
      } else if ($soiltype == '61') {
        $soiltypeResult = 'Saraturve';
      } else if ($soiltype == '62') {
        $soiltypeResult = 'Rahkaturve';
      } else if ($soiltype == '63') {
        $soiltypeResult = 'Puuvaltainen turve';
      } else if ($soiltype == '64') {
        $soiltypeResult = 'Eroosioherkkä saraturve (von Post luokka yli 5)';
      } else if ($soiltype == '65') {
        $soiltypeResult = 'Eroosioherkkä rahkaturve (von Post luokka yli 5)';
      } else if ($soiltype == '66') {
        $soiltypeResult = 'Maatumaton saraturve (von Post luokka enintään 5)';
      } else if ($soiltype == '67') {
        $soiltypeResult = 'Maatumaton rahkaturve (von Post luokka enintään 5)';
      } else if ($soiltype == '70') {
        $soiltypeResult = 'Multamaa';
      } else if ($soiltype == '80') {
        $soiltypeResult = 'Liejumaa';
      } else {
        $soiltypeResult = 'Muu';
      }

      return $soiltypeResult;
    }

    public function getForestKitDevelopmentclass($developmentclass) {

      if ($developmentclass == 'A0') {
        $developmentclassResult = 'Aukea';
      } else if ($developmentclass == 'S0') {
        $developmentclassResult = 'Siemenpuumetsikkö';
      } else if ($developmentclass == 'T1') {
        $developmentclassResult = 'Taimikko alle 1,3 m';
      } else if ($developmentclass == 'T2') {
        $developmentclassResult = 'Taimikko yli 1,3 m';
      } else if ($developmentclass == 'Y1') {
        $developmentclassResult = 'Ylispuustoinen taimikko';
      } else if ($developmentclass == '02') {
        $developmentclassResult = 'Nuori kasvatusmetsikkö';
      } else if ($developmentclass == '03') {
        $developmentclassResult = 'Varttunut kasvatusmetsikkö';
      } else if ($developmentclass == '04') {
        $developmentclassResult = 'Uudistuskypsä metsikkö';
      } else if ($developmentclass == '05') {
        $developmentclassResult = 'Suojuspuumetsikkö';
      } else if ($developmentclass == 'ER') {
        $developmentclassResult = 'Eri-ikäisrakenteinen metsikkö';
      } else {
        $developmentclassResult = 'Muu';
      }

      return $developmentclassResult;
    }

    public function getForestKitAccessibility($accessibility) {

      if ($accessibility == '1') {
        $accessibilityResult = 'Myös kelirikon aikana';
      } else if ($accessibility == '2') {
        $accessibilityResult = 'Myös sulan maan, mutta ei kelirikon aikana';
      } else if ($accessibility == '3') {
        $accessibilityResult = 'Myös sulan maan aikana kuivana kautena';
      } else if ($accessibility == '4') {
        $accessibilityResult = 'Vain kun maa on jäässä';
      } else {
        $accessibilityResult = 'Muu';
      }

      return $accessibilityResult;
    }

    public function getForestKitQuality($quality) {

      if ($quality == '0') {
        $qualityResult = 'Ei määritelty';
      } else if ($quality == '10') {
        $qualityResult = 'Hyvä';
      } else if ($quality == '11') {
        $qualityResult = 'Kehityskelpoinen, hyvä';
      } else if ($quality == '20') {
        $qualityResult = 'Tyydyttävä';
      } else if ($quality == '21') {
        $qualityResult = 'Kehityskelpoinen, tyydyttävä: aukkoinen, harva, vähäpuustoinen';
      } else if ($quality == '22') {
        $qualityResult = 'Kehityskelpoinen, tyydyttävä: ylitiheä, hoitamaton';
      } else if ($quality == '23') {
        $qualityResult = 'Kehityskelpoinen tyydyttävä/välttävä: vajaalaatuinen';
      } else if ($quality == '30') {
        $qualityResult = 'Välttävä';
      } else if ($quality == '31') {
        $qualityResult = 'Kehityskelvoton: aukkoinen, harva, vähäpuustoinen';
      } else if ($quality == '32') {
        $qualityResult = 'Kehityskelvoton: ylitiheä, hoitamaton';
      } else if ($quality == '33') {
        $qualityResult = 'Kehityskelvoton: kasvupaikalle metsänhoidollisesti sopimaton puulaji';
      } else if ($quality == '34') {
        $qualityResult = 'Kehityskelvoton: yli-ikäinen';
      } else if ($quality == '35') {
        $qualityResult = 'Kehityskelvoton: huonokasvuinen, harsittu, jätemetsikkö';
      } else if ($quality == '36') {
        $qualityResult = 'Kehityskelvoton: tuhometsikkö';
      } else {
        $qualityResult = 'Muu';
      }

      return $qualityResult;
    }

    public function getForestKitTreespecie($treespecie) {

      if ($treespecie == '1') {
        $treespecieResult = 'Mänty';
      } else if ($treespecie == '2') {
        $treespecieResult = 'Kuusi';
      } else if ($treespecie == '3') {
        $treespecieResult = 'Rauduskoivu';
      } else if ($treespecie == '4') {
        $treespecieResult = 'Hieskoivu';
      } else if ($treespecie == '5') {
        $treespecieResult = 'Haapa';
      } else if ($treespecie == '6') {
        $treespecieResult = 'Harmaaleppä';
      } else if ($treespecie == '7') {
        $treespecieResult = 'Tervaleppä';
      } else if ($treespecie == '8') {
        $treespecieResult = 'Muu havupuu';
      } else if ($treespecie == '9') {
        $treespecieResult = 'Muu lehtipuu';
      } else if ($treespecie == '29') {
        $treespecieResult = 'Lehtipuu';
      } else if ($treespecie == '30') {
        $treespecieResult = 'Havupuu';
      } else {
        $treespecieResult = 'Muu';
      }

      return $treespecieResult;
    }

    public function getGrowplaceSubgroupForExcel($growplace, $soiltype) {

      $subgroup = 10;
      $growplace = strtolower($growplace);
      $soiltype = strtolower($soiltype);
      if (strpos($soiltype, 'kangasmaa') !== false) {
        $subgroup = 1; //kangas = 1
      } if (strpos($soiltype, 'turvemaa') !== false
      && (strpos($growplace, 'lehtomainen kangas') !== false
      || strpos($growplace, 'tuore kangas') !== false)) {
        $subgroup = 2; //korpi = 2
      } if (strpos($soiltype, 'turvemaa') !== false
      && (strpos($growplace, 'kuivahko kangas') !== false
      || strpos($growplace, 'kuiva kangas') !== false
      || strpos($growplace, 'karukkokangas') !== false)) {
        $subgroup = 3; //räme = 3
      }

      return $subgroup;

    }

    public function getGrowplaceForExcel($growplaceString) {

      //longer string, shorter string
      $growplace = 1;
      if (strpos($growplaceString, 'ruoho') !== false) {
        $growplace = 2;
      } if (strpos($growplaceString, 'mustikka') !== false) {
        $growplace = 3;
      } if (strpos($growplaceString, 'puolukka') !== false) {
        $growplace = 4;
      } if (strpos($growplaceString, 'varpu') !== false) {
        $growplace = 5;
      } if (strpos($growplaceString, 'jäkälä') !== false) {
        $growplace = 6;
      }

      return $growplace;

    }

    public function getGrowplaceSubgroupForExcel1($growplace, $soiltype) {

      $subgroup = 1;
      if (strpos($growplace, 'kangas') !== false) {
        $subgroup = 1;
      } if (strpos($growplace, 'korpi') !== false) {
        $subgroup = 2;
      } if (strpos($growplace, 'räme') !== false) {
        $subgroup = 3;
      }

      return $subgroup;

    }

    public function getGrowplaceForExcel1($growplaceString) {

      //longer string, shorter string
      $growplace = 1;
      if (strpos($growplaceString, 'Lehtomainen kangas') !== false) {
        $growplace = 2;
      } if (strpos($growplaceString, 'Kuivahko kangas') !== false) {
        $growplace = 4;
      } if (strpos($growplaceString, 'Tuore kangas') !== false) {
        $growplace = 3;
      } if (strpos($growplaceString, 'Kuiva kangas') !== false) {
        $growplace = 5;
      }

      return $growplace;

    }

    public function createImportedArticle($articleData) {

      $newImportedArticle = new ImportedArticle();

      //echo $articleData['totals']['totalAge'].'-';
	  if (isset($articleData['area'])) {
		  $newImportedArticle->setArea($articleData['area']);
		  if ($articleData['standnumberExtension'] !== '') {
			$newImportedArticle->setStandNumber($articleData['standnumber'].'.'.$articleData['standnumberExtension']);
		  } else {
			$newImportedArticle->setStandNumber($articleData['standnumber']);
		  }
		  $newImportedArticle->setMainclass($articleData['mainclass']);
		  if (isset($articleData['growplace'])) {
			$newImportedArticle->setGrowplace($articleData['growplace']);
		  }
		  if (isset($articleData['soiltype'])) {
		  $newImportedArticle->setSoiltype($articleData['soiltype']);
		  }
		   if (isset($articleData['developmentclass'])) {
			$newImportedArticle->setDevelopmentclass($articleData['developmentclass']);
			}
			if (isset($articleData['accessibility'])) {
		  $newImportedArticle->setAccessibility($articleData['accessibility']);
			}
			if (isset($articleData['subgroup_number'])) {
		  $newImportedArticle->setSubgroupNumber($articleData['subgroup_number']);
			}
			if (isset($articleData['fertilityclass_number'])) {
		  $newImportedArticle->setFertilityclassNumber($articleData['fertilityclass_number']);
			}
		  if (isset($articleData['quality'])) {
			$newImportedArticle->setQuality($articleData['quality']);
		  }
		  $newImportedArticle->setOperations('');
		  $newImportedArticle->setAdded(time());

		  $newImportedArticle->setTotalAge($articleData['totals']['totalAge']);
		  $newImportedArticle->setTotalVolume($articleData['totals']['totalVolume']);
		  $newImportedArticle->setTotalVolumeHa($articleData['totals']['totalVolumeHa']);
		  $newImportedArticle->setTotalLogVolume($articleData['totals']['totalLogVolume']);
		  $newImportedArticle->setTotalPulpVolume($articleData['totals']['totalPulpVolume']);
		  $newImportedArticle->setTotalDiameter($articleData['totals']['totalDiameter']);
		  $newImportedArticle->setTotalLength($articleData['totals']['totalLength']);
		  $newImportedArticle->setTotalDensity($articleData['totals']['totalDensity']);
		  $newImportedArticle->setTotalBasalarea($articleData['totals']['totalBasalarea']);
		  $newImportedArticle->setTotalGrowth($articleData['totals']['totalGrowth']);

		  $newImportedArticle->setMantyAge(0);
		  $newImportedArticle->setMantyVolume(0);
		  $newImportedArticle->setMantyVolumeHa(0);
		  $newImportedArticle->setMantyLogVolume(0);
		  $newImportedArticle->setMantyPulpVolume(0);
		  $newImportedArticle->setMantyDiameter(0);
		  $newImportedArticle->setMantyLength(0);
		  $newImportedArticle->setMantyDensity(0);
		  $newImportedArticle->setMantyBasalarea(0);
		  $newImportedArticle->setMantyGrowth(0);

		  $newImportedArticle->setKuusiAge(0);
		  $newImportedArticle->setKuusiVolume(0);
		  $newImportedArticle->setKuusiVolumeHa(0);
		  $newImportedArticle->setKuusiLogVolume(0);
		  $newImportedArticle->setKuusiPulpVolume(0);
		  $newImportedArticle->setKuusiDiameter(0);
		  $newImportedArticle->setKuusiLength(0);
		  $newImportedArticle->setKuusiDensity(0);
		  $newImportedArticle->setKuusiBasalarea(0);
		  $newImportedArticle->setKuusiGrowth(0);

		  $newImportedArticle->setRauduskoivuAge(0);
		  $newImportedArticle->setRauduskoivuVolume(0);
		  $newImportedArticle->setRauduskoivuVolumeHa(0);
		  $newImportedArticle->setRauduskoivuLogVolume(0);
		  $newImportedArticle->setRauduskoivuPulpVolume(0);
		  $newImportedArticle->setRauduskoivuDiameter(0);
		  $newImportedArticle->setRauduskoivuLength(0);
		  $newImportedArticle->setRauduskoivuDensity(0);
		  $newImportedArticle->setRauduskoivuBasalarea(0);
		  $newImportedArticle->setRauduskoivuGrowth(0);

		  $newImportedArticle->setHieskoivuAge(0);
		  $newImportedArticle->setHieskoivuVolume(0);
		  $newImportedArticle->setHieskoivuVolumeHa(0);
		  $newImportedArticle->setHieskoivuLogVolume(0);
		  $newImportedArticle->setHieskoivuPulpVolume(0);
		  $newImportedArticle->setHieskoivuDiameter(0);
		  $newImportedArticle->setHieskoivuLength(0);
		  $newImportedArticle->setHieskoivuDensity(0);
		  $newImportedArticle->setHieskoivuBasalarea(0);
		  $newImportedArticle->setHieskoivuGrowth(0);

		  $newImportedArticle->setHaapaAge(0);
		  $newImportedArticle->setHaapaVolume(0);
		  $newImportedArticle->setHaapaVolumeHa(0);
		  $newImportedArticle->setHaapaLogVolume(0);
		  $newImportedArticle->setHaapaPulpVolume(0);
		  $newImportedArticle->setHaapaDiameter(0);
		  $newImportedArticle->setHaapaLength(0);
		  $newImportedArticle->setHaapaDensity(0);
		  $newImportedArticle->setHaapaBasalarea(0);
		  $newImportedArticle->setHaapaGrowth(0);

		  $newImportedArticle->setHarmaaleppaAge(0);
		  $newImportedArticle->setHarmaaleppaVolume(0);
		  $newImportedArticle->setHarmaaleppaVolumeHa(0);
		  $newImportedArticle->setHarmaaleppaLogVolume(0);
		  $newImportedArticle->setHarmaaleppaPulpVolume(0);
		  $newImportedArticle->setHarmaaleppaDiameter(0);
		  $newImportedArticle->setHarmaaleppaLength(0);
		  $newImportedArticle->setHarmaaleppaDensity(0);
		  $newImportedArticle->setHarmaaleppaBasalarea(0);
		  $newImportedArticle->setHarmaaleppaGrowth(0);

		  $newImportedArticle->setTervaleppaAge(0);
		  $newImportedArticle->setTervaleppaVolume(0);
		  $newImportedArticle->setTervaleppaVolumeHa(0);
		  $newImportedArticle->setTervaleppaLogVolume(0);
		  $newImportedArticle->setTervaleppaPulpVolume(0);
		  $newImportedArticle->setTervaleppaDiameter(0);
		  $newImportedArticle->setTervaleppaLength(0);
		  $newImportedArticle->setTervaleppaDensity(0);
		  $newImportedArticle->setTervaleppaBasalarea(0);
		  $newImportedArticle->setTervaleppaGrowth(0);

		  $newImportedArticle->setHavupuuAge(0);
		  $newImportedArticle->setHavupuuVolume(0);
		  $newImportedArticle->setHavupuuVolumeHa(0);
		  $newImportedArticle->setHavupuuLogVolume(0);
		  $newImportedArticle->setHavupuuPulpVolume(0);
		  $newImportedArticle->setHavupuuDiameter(0);
		  $newImportedArticle->setHavupuuLength(0);
		  $newImportedArticle->setHavupuuDensity(0);
		  $newImportedArticle->setHavupuuBasalarea(0);
		  $newImportedArticle->setHavupuuGrowth(0);

		  $newImportedArticle->setLehtipuuAge(0);
		  $newImportedArticle->setLehtipuuVolume(0);
		  $newImportedArticle->setLehtipuuVolumeHa(0);
		  $newImportedArticle->setLehtipuuLogVolume(0);
		  $newImportedArticle->setLehtipuuPulpVolume(0);
		  $newImportedArticle->setLehtipuuDiameter(0);
		  $newImportedArticle->setLehtipuuLength(0);
		  $newImportedArticle->setLehtipuuDensity(0);
		  $newImportedArticle->setLehtipuuBasalarea(0);
		  $newImportedArticle->setLehtipuuGrowth(0);

		  $newImportedArticle->setAdditionality5yearsHa(0);
		  $newImportedArticle->setAdditionality10yearsHa(0);
		  $newImportedArticle->setAdditionality5yearsPercentage(0);
		  $newImportedArticle->setAdditionality10yearsPercentage(0);
	  }

	  if (isset($articleData['treeData'])) {
      foreach ($articleData['treeData'] as $k => $product) {
		if (isset($articleData['treeData'][$k]['treespecie'])) {
        //echo $articleData['treeData'][$k]['treespecie'];
        if ($articleData['treeData'][$k]['treespecie'] == "Mänty") {
          $newImportedArticle->setMantyAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setMantyVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setMantyVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setMantyLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setMantyPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setMantyDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setMantyLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setMantyDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setMantyBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setMantyGrowth($articleData['treeData'][$k]['growth']);
        }
        if ($articleData['treeData'][$k]['treespecie'] == "Kuusi") {
          $newImportedArticle->setKuusiAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setKuusiVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setKuusiVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setKuusiLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setKuusiPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setKuusiDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setKuusiLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setKuusiDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setKuusiBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setKuusiGrowth($articleData['treeData'][$k]['growth']);
        }
        if ($articleData['treeData'][$k]['treespecie'] == "Rauduskoivu") {
          $newImportedArticle->setRauduskoivuAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setRauduskoivuVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setRauduskoivuVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setRauduskoivuLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setRauduskoivuPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setRauduskoivuDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setRauduskoivuLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setRauduskoivuDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setRauduskoivuBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setRauduskoivuGrowth($articleData['treeData'][$k]['growth']);
        }
        if ($articleData['treeData'][$k]['treespecie'] == "Hieskoivu") {
          $newImportedArticle->setHieskoivuAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setHieskoivuVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setHieskoivuVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setHieskoivuLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setHieskoivuPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setHieskoivuDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setHieskoivuLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setHieskoivuDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setHieskoivuBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setHieskoivuGrowth($articleData['treeData'][$k]['growth']);
        }
        if ($articleData['treeData'][$k]['treespecie'] == "Haapa") {
          $newImportedArticle->setHaapaAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setHaapaVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setHaapaVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setHaapaLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setHaapaPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setHaapaDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setHaapaLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setHaapaDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setHaapaBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setHaapaGrowth($articleData['treeData'][$k]['growth']);
        }
        if ($articleData['treeData'][$k]['treespecie'] == "Harmaaleppä") {
          $newImportedArticle->setHarmaaleppaAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setHarmaaleppaVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setHarmaaleppaVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setHarmaaleppaLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setHarmaaleppaPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setHarmaaleppaDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setHarmaaleppaLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setHarmaaleppaDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setHarmaaleppaBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setHarmaaleppaGrowth($articleData['treeData'][$k]['growth']);
        }
        if ($articleData['treeData'][$k]['treespecie'] == "Tervaleppä") {
          $newImportedArticle->setTervaleppaAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setTervaleppaVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setTervaleppaVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setTervaleppaLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setTervaleppaPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setTervaleppaDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setTervaleppaLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setTervaleppaDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setTervaleppaBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setTervaleppaGrowth($articleData['treeData'][$k]['growth']);
        }
        if ($articleData['treeData'][$k]['treespecie'] == "Lehtipuu") {
          $newImportedArticle->setLehtipuuAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setLehtipuuVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setLehtipuuVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setLehtipuuLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setLehtipuuPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setLehtipuuDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setLehtipuuLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setLehtipuuDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setLehtipuuBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setLehtipuuGrowth($articleData['treeData'][$k]['growth']);
        }
        if ($articleData['treeData'][$k]['treespecie'] == "Havupuu") {
          $newImportedArticle->setHavupuuAge($articleData['treeData'][$k]['age']);
          $newImportedArticle->setHavupuuVolume(round($articleData['treeData'][$k]['volume'], 3));
          $newImportedArticle->setHavupuuVolumeHa(round($articleData['treeData'][$k]['volume_ha'], 3));
          $newImportedArticle->setHavupuuLogVolume(round($articleData['treeData'][$k]['log_volume'], 3));
          $newImportedArticle->setHavupuuPulpVolume(round($articleData['treeData'][$k]['pulp_volume'], 3));
          $newImportedArticle->setHavupuuDiameter($articleData['treeData'][$k]['diameter']);
          $newImportedArticle->setHavupuuLength($articleData['treeData'][$k]['length']);
          $newImportedArticle->setHavupuuDensity($articleData['treeData'][$k]['density']);
          $newImportedArticle->setHavupuuBasalarea($articleData['treeData'][$k]['basalarea']);
          $newImportedArticle->setHavupuuGrowth($articleData['treeData'][$k]['growth']);
        }
      }
	}
	  }

      return $newImportedArticle;

    }

    public function createImportedArticle1($propertyParents, $treeInfoHeader, $treeInfo) {

      $newImportedArticle = new ImportedArticle();
      //print_r($treeInfoHeader);

      $newImportedArticle->setArea($propertyParents['area']);
      $newImportedArticle->setMainclass($propertyParents['mainclass']);
      $newImportedArticle->setGrowplace($propertyParents['growplace']);
      $newImportedArticle->setSoiltype($propertyParents['soiltype']);
      $newImportedArticle->setDevelopmentclass($propertyParents['developmentclass']);
      $newImportedArticle->setAccessibility($propertyParents['accessibility']);
      $newImportedArticle->setQuality($propertyParents['quality']);
      $newImportedArticle->setOperations('');
      $newImportedArticle->setAdded(time());

      $newImportedArticle->setTotalAge(0);
      $newImportedArticle->setTotalVolume(0);
      $newImportedArticle->setTotalVolumeHa(0);
      $newImportedArticle->setTotalLogVolume(0);
      $newImportedArticle->setTotalPulpVolume(0);
      $newImportedArticle->setTotalDiameter(0);
      $newImportedArticle->setTotalLength(0);
      $newImportedArticle->setTotalDensity(0);
      $newImportedArticle->setTotalBasalarea(0);
      $newImportedArticle->setTotalGrowth(0);

      if (isset($treeInfoHeader['@attributes']['Textbox6'])) { //treeless have only: Textbox5="0" Textbox16="0" Textbox17="0"
        $newImportedArticle->setTotalAge(round($treeInfoHeader['@attributes']['Textbox5'], 3));
        $newImportedArticle->setTotalVolume(round($treeInfoHeader['@attributes']['Textbox6'], 3));
        $newImportedArticle->setTotalVolumeHa(round($treeInfoHeader['@attributes']['Textbox13'], 3));
        $newImportedArticle->setTotalLogVolume(round($treeInfoHeader['@attributes']['Textbox14'], 3));
        if (isset($treeInfoHeader['@attributes']['Textbox15'])) {
          $newImportedArticle->setTotalPulpVolume(round($treeInfoHeader['@attributes']['Textbox15'], 3));
        }
        $newImportedArticle->setTotalDiameter(round($treeInfoHeader['@attributes']['Textbox16'], 3));
        $newImportedArticle->setTotalLength(round($treeInfoHeader['@attributes']['Textbox17'], 3));
        $newImportedArticle->setTotalDensity(round($treeInfoHeader['@attributes']['Textbox18'], 3));
        $newImportedArticle->setTotalBasalarea(round($treeInfoHeader['@attributes']['Textbox19'], 3));
        $newImportedArticle->setTotalGrowth(round($treeInfoHeader['@attributes']['Textbox20'], 3));
      }

      $newImportedArticle->setMantyAge(0);
      $newImportedArticle->setMantyVolume(0);
      $newImportedArticle->setMantyVolumeHa(0);
      $newImportedArticle->setMantyLogVolume(0);
      $newImportedArticle->setMantyPulpVolume(0);
      $newImportedArticle->setMantyDiameter(0);
      $newImportedArticle->setMantyLength(0);
      $newImportedArticle->setMantyDensity(0);
      $newImportedArticle->setMantyBasalarea(0);
      $newImportedArticle->setMantyGrowth(0);

      $newImportedArticle->setKuusiAge(0);
      $newImportedArticle->setKuusiVolume(0);
      $newImportedArticle->setKuusiVolumeHa(0);
      $newImportedArticle->setKuusiLogVolume(0);
      $newImportedArticle->setKuusiPulpVolume(0);
      $newImportedArticle->setKuusiDiameter(0);
      $newImportedArticle->setKuusiLength(0);
      $newImportedArticle->setKuusiDensity(0);
      $newImportedArticle->setKuusiBasalarea(0);
      $newImportedArticle->setKuusiGrowth(0);

      $newImportedArticle->setRauduskoivuAge(0);
      $newImportedArticle->setRauduskoivuVolume(0);
      $newImportedArticle->setRauduskoivuVolumeHa(0);
      $newImportedArticle->setRauduskoivuLogVolume(0);
      $newImportedArticle->setRauduskoivuPulpVolume(0);
      $newImportedArticle->setRauduskoivuDiameter(0);
      $newImportedArticle->setRauduskoivuLength(0);
      $newImportedArticle->setRauduskoivuDensity(0);
      $newImportedArticle->setRauduskoivuBasalarea(0);
      $newImportedArticle->setRauduskoivuGrowth(0);

      $newImportedArticle->setHieskoivuAge(0);
      $newImportedArticle->setHieskoivuVolume(0);
      $newImportedArticle->setHieskoivuVolumeHa(0);
      $newImportedArticle->setHieskoivuLogVolume(0);
      $newImportedArticle->setHieskoivuPulpVolume(0);
      $newImportedArticle->setHieskoivuDiameter(0);
      $newImportedArticle->setHieskoivuLength(0);
      $newImportedArticle->setHieskoivuDensity(0);
      $newImportedArticle->setHieskoivuBasalarea(0);
      $newImportedArticle->setHieskoivuGrowth(0);

      $newImportedArticle->setHaapaAge(0);
      $newImportedArticle->setHaapaVolume(0);
      $newImportedArticle->setHaapaVolumeHa(0);
      $newImportedArticle->setHaapaLogVolume(0);
      $newImportedArticle->setHaapaPulpVolume(0);
      $newImportedArticle->setHaapaDiameter(0);
      $newImportedArticle->setHaapaLength(0);
      $newImportedArticle->setHaapaDensity(0);
      $newImportedArticle->setHaapaBasalarea(0);
      $newImportedArticle->setHaapaGrowth(0);

      $newImportedArticle->setHarmaaleppaAge(0);
      $newImportedArticle->setHarmaaleppaVolume(0);
      $newImportedArticle->setHarmaaleppaVolumeHa(0);
      $newImportedArticle->setHarmaaleppaLogVolume(0);
      $newImportedArticle->setHarmaaleppaPulpVolume(0);
      $newImportedArticle->setHarmaaleppaDiameter(0);
      $newImportedArticle->setHarmaaleppaLength(0);
      $newImportedArticle->setHarmaaleppaDensity(0);
      $newImportedArticle->setHarmaaleppaBasalarea(0);
      $newImportedArticle->setHarmaaleppaGrowth(0);

      $newImportedArticle->setTervaleppaAge(0);
      $newImportedArticle->setTervaleppaVolume(0);
      $newImportedArticle->setTervaleppaVolumeHa(0);
      $newImportedArticle->setTervaleppaLogVolume(0);
      $newImportedArticle->setTervaleppaPulpVolume(0);
      $newImportedArticle->setTervaleppaDiameter(0);
      $newImportedArticle->setTervaleppaLength(0);
      $newImportedArticle->setTervaleppaDensity(0);
      $newImportedArticle->setTervaleppaBasalarea(0);
      $newImportedArticle->setTervaleppaGrowth(0);

      $newImportedArticle->setHavupuuAge(0);
      $newImportedArticle->setHavupuuVolume(0);
      $newImportedArticle->setHavupuuVolumeHa(0);
      $newImportedArticle->setHavupuuLogVolume(0);
      $newImportedArticle->setHavupuuPulpVolume(0);
      $newImportedArticle->setHavupuuDiameter(0);
      $newImportedArticle->setHavupuuLength(0);
      $newImportedArticle->setHavupuuDensity(0);
      $newImportedArticle->setHavupuuBasalarea(0);
      $newImportedArticle->setHavupuuGrowth(0);

      $newImportedArticle->setLehtipuuAge(0);
      $newImportedArticle->setLehtipuuVolume(0);
      $newImportedArticle->setLehtipuuVolumeHa(0);
      $newImportedArticle->setLehtipuuLogVolume(0);
      $newImportedArticle->setLehtipuuPulpVolume(0);
      $newImportedArticle->setLehtipuuDiameter(0);
      $newImportedArticle->setLehtipuuLength(0);
      $newImportedArticle->setLehtipuuDensity(0);
      $newImportedArticle->setLehtipuuBasalarea(0);
      $newImportedArticle->setLehtipuuGrowth(0);

      foreach ($treeInfo as $k => $product) {
        if (isset($treeInfo[$k]['@attributes']['treeSpec'])) {
          $treeSpecie = $treeInfo[$k]['@attributes']['treeSpec'];
          if ($treeSpecie == "Mänty") {
            $newImportedArticle->setMantyAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setMantyVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setMantyVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setMantyLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setMantyPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setMantyDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setMantyLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setMantyDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setMantyBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setMantyGrowth($treeInfo[$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Kuusi") {
            $newImportedArticle->setKuusiAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setKuusiVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setKuusiVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setKuusiLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setKuusiPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setKuusiDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setKuusiLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setKuusiDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setKuusiBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setKuusiGrowth($treeInfo[$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Rauduskoivu") {
            $newImportedArticle->setRauduskoivuAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setRauduskoivuVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setRauduskoivuVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setRauduskoivuLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setRauduskoivuPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setRauduskoivuDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setRauduskoivuLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setRauduskoivuDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setRauduskoivuBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setRauduskoivuGrowth($treeInfo[$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Hieskoivu") {
            $newImportedArticle->setHieskoivuAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setHieskoivuVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setHieskoivuVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setHieskoivuLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setHieskoivuPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setHieskoivuDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setHieskoivuLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setHieskoivuDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setHieskoivuBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setHieskoivuGrowth($treeInfo[$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Haapa") {
            $newImportedArticle->setHaapaAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setHaapaVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setHaapaVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setHaapaLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setHaapaPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setHaapaDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setHaapaLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setHaapaDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setHaapaBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setHaapaGrowth($treeInfo[$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Harmaaleppä") {
            $newImportedArticle->setHarmaaleppaAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setHarmaaleppaVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setHarmaaleppaVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setHarmaaleppaLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setHarmaaleppaPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setHarmaaleppaDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setHarmaaleppaLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setHarmaaleppaDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setHarmaaleppaBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setHarmaaleppaGrowth($treeInfo[$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Tervaleppä") {
            $newImportedArticle->setTervaleppaAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setTervaleppaVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setTervaleppaVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setTervaleppaLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setTervaleppaPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setTervaleppaDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setTervaleppaLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setTervaleppaDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setTervaleppaBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setTervaleppaGrowth($treeInfo[$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Lehtipuu") {
            $newImportedArticle->setLehtipuuAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setLehtipuuVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setLehtipuuVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setLehtipuuLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setLehtipuuPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setLehtipuuDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setLehtipuuLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setLehtipuuDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setLehtipuuBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setLehtipuuGrowth($treeInfo[$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Havupuu") {
            $newImportedArticle->setHavupuuAge($treeInfo[$k]['@attributes']['age']);
            $newImportedArticle->setHavupuuVolume(round($treeInfo[$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setHavupuuVolumeHa(round($treeInfo[$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setHavupuuLogVolume(round($treeInfo[$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setHavupuuPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setHavupuuDiameter($treeInfo[$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setHavupuuLength($treeInfo[$k]['@attributes']['averageLength']);
            $newImportedArticle->setHavupuuDensity($treeInfo[$k]['@attributes']['density']);
            $newImportedArticle->setHavupuuBasalarea($treeInfo[$k]['@attributes']['basalArea']);
            $newImportedArticle->setHavupuuGrowth($treeInfo[$k]['@attributes']['growth']);
          }
        }
      }

      return $newImportedArticle;

    }

    public function createImportedArticleWithFoundProperty($propertyParents, $treeInfoHeader, $property) {

      $newImportedArticle = new ImportedArticle();

      $newImportedArticle->setTotalAge(0);
      $newImportedArticle->setTotalVolume(0);
      $newImportedArticle->setTotalVolumeHa(0);
      $newImportedArticle->setTotalLogVolume(0);
      $newImportedArticle->setTotalPulpVolume(0);
      $newImportedArticle->setTotalDiameter(0);
      $newImportedArticle->setTotalLength(0);
      $newImportedArticle->setTotalDensity(0);
      $newImportedArticle->setTotalBasalarea(0);
      $newImportedArticle->setTotalGrowth(0);

      if (isset($treeInfoHeader['@attributes']['Textbox6'])) { //treeless have only: Textbox5="0" Textbox16="0" Textbox17="0"
        $newImportedArticle->setTotalAge(round($treeInfoHeader['@attributes']['Textbox5'], 3));
        $newImportedArticle->setTotalVolume(round($treeInfoHeader['@attributes']['Textbox6'], 3));
        $newImportedArticle->setTotalVolumeHa(round($treeInfoHeader['@attributes']['Textbox13'], 3));
        $newImportedArticle->setTotalLogVolume(round($treeInfoHeader['@attributes']['Textbox14'], 3));
        if (isset($treeInfoHeader['@attributes']['Textbox15'])) {
          $newImportedArticle->setTotalPulpVolume(round($treeInfoHeader['@attributes']['Textbox15'], 3));
        }
        $newImportedArticle->setTotalDiameter(round($treeInfoHeader['@attributes']['Textbox16'], 3));
        $newImportedArticle->setTotalLength(round($treeInfoHeader['@attributes']['Textbox17'], 3));
        $newImportedArticle->setTotalDensity(round($treeInfoHeader['@attributes']['Textbox18'], 3));
        $newImportedArticle->setTotalBasalarea(round($treeInfoHeader['@attributes']['Textbox19'], 3));
        $newImportedArticle->setTotalGrowth(round($treeInfoHeader['@attributes']['Textbox20'], 3));
      }

      $newImportedArticle->setArea($propertyParents['area']);
      $newImportedArticle->setMainclass($propertyParents['mainclass']);
      $newImportedArticle->setGrowplace($propertyParents['growplace']);
      $newImportedArticle->setSoiltype($propertyParents['soiltype']);
      $newImportedArticle->setDevelopmentclass($propertyParents['developmentclass']);
      $newImportedArticle->setAccessibility($propertyParents['accessibility']);
      $newImportedArticle->setQuality($propertyParents['quality']);
      $newImportedArticle->setOperations('');
      $newImportedArticle->setAdded(time());
      $newImportedArticle->setProperty($property);

      $newImportedArticle->setMantyAge(0);
      $newImportedArticle->setMantyVolume(0);
      $newImportedArticle->setMantyVolumeHa(0);
      $newImportedArticle->setMantyLogVolume(0);
      $newImportedArticle->setMantyPulpVolume(0);
      $newImportedArticle->setMantyDiameter(0);
      $newImportedArticle->setMantyLength(0);
      $newImportedArticle->setMantyDensity(0);
      $newImportedArticle->setMantyBasalarea(0);
      $newImportedArticle->setMantyGrowth(0);

      $newImportedArticle->setKuusiAge(0);
      $newImportedArticle->setKuusiVolume(0);
      $newImportedArticle->setKuusiVolumeHa(0);
      $newImportedArticle->setKuusiLogVolume(0);
      $newImportedArticle->setKuusiPulpVolume(0);
      $newImportedArticle->setKuusiDiameter(0);
      $newImportedArticle->setKuusiLength(0);
      $newImportedArticle->setKuusiDensity(0);
      $newImportedArticle->setKuusiBasalarea(0);
      $newImportedArticle->setKuusiGrowth(0);

      $newImportedArticle->setRauduskoivuAge(0);
      $newImportedArticle->setRauduskoivuVolume(0);
      $newImportedArticle->setRauduskoivuVolumeHa(0);
      $newImportedArticle->setRauduskoivuLogVolume(0);
      $newImportedArticle->setRauduskoivuPulpVolume(0);
      $newImportedArticle->setRauduskoivuDiameter(0);
      $newImportedArticle->setRauduskoivuLength(0);
      $newImportedArticle->setRauduskoivuDensity(0);
      $newImportedArticle->setRauduskoivuBasalarea(0);
      $newImportedArticle->setRauduskoivuGrowth(0);

      $newImportedArticle->setHieskoivuAge(0);
      $newImportedArticle->setHieskoivuVolume(0);
      $newImportedArticle->setHieskoivuVolumeHa(0);
      $newImportedArticle->setHieskoivuLogVolume(0);
      $newImportedArticle->setHieskoivuPulpVolume(0);
      $newImportedArticle->setHieskoivuDiameter(0);
      $newImportedArticle->setHieskoivuLength(0);
      $newImportedArticle->setHieskoivuDensity(0);
      $newImportedArticle->setHieskoivuBasalarea(0);
      $newImportedArticle->setHieskoivuGrowth(0);

      $newImportedArticle->setHaapaAge(0);
      $newImportedArticle->setHaapaVolume(0);
      $newImportedArticle->setHaapaVolumeHa(0);
      $newImportedArticle->setHaapaLogVolume(0);
      $newImportedArticle->setHaapaPulpVolume(0);
      $newImportedArticle->setHaapaDiameter(0);
      $newImportedArticle->setHaapaLength(0);
      $newImportedArticle->setHaapaDensity(0);
      $newImportedArticle->setHaapaBasalarea(0);
      $newImportedArticle->setHaapaGrowth(0);

      $newImportedArticle->setHarmaaleppaAge(0);
      $newImportedArticle->setHarmaaleppaVolume(0);
      $newImportedArticle->setHarmaaleppaVolumeHa(0);
      $newImportedArticle->setHarmaaleppaLogVolume(0);
      $newImportedArticle->setHarmaaleppaPulpVolume(0);
      $newImportedArticle->setHarmaaleppaDiameter(0);
      $newImportedArticle->setHarmaaleppaLength(0);
      $newImportedArticle->setHarmaaleppaDensity(0);
      $newImportedArticle->setHarmaaleppaBasalarea(0);
      $newImportedArticle->setHarmaaleppaGrowth(0);

      $newImportedArticle->setTervaleppaAge(0);
      $newImportedArticle->setTervaleppaVolume(0);
      $newImportedArticle->setTervaleppaVolumeHa(0);
      $newImportedArticle->setTervaleppaLogVolume(0);
      $newImportedArticle->setTervaleppaPulpVolume(0);
      $newImportedArticle->setTervaleppaDiameter(0);
      $newImportedArticle->setTervaleppaLength(0);
      $newImportedArticle->setTervaleppaDensity(0);
      $newImportedArticle->setTervaleppaBasalarea(0);
      $newImportedArticle->setTervaleppaGrowth(0);

      $newImportedArticle->setHavupuuAge(0);
      $newImportedArticle->setHavupuuVolume(0);
      $newImportedArticle->setHavupuuVolumeHa(0);
      $newImportedArticle->setHavupuuLogVolume(0);
      $newImportedArticle->setHavupuuPulpVolume(0);
      $newImportedArticle->setHavupuuDiameter(0);
      $newImportedArticle->setHavupuuLength(0);
      $newImportedArticle->setHavupuuDensity(0);
      $newImportedArticle->setHavupuuBasalarea(0);
      $newImportedArticle->setHavupuuGrowth(0);

      $newImportedArticle->setLehtipuuAge(0);
      $newImportedArticle->setLehtipuuVolume(0);
      $newImportedArticle->setLehtipuuVolumeHa(0);
      $newImportedArticle->setLehtipuuLogVolume(0);
      $newImportedArticle->setLehtipuuPulpVolume(0);
      $newImportedArticle->setLehtipuuDiameter(0);
      $newImportedArticle->setLehtipuuLength(0);
      $newImportedArticle->setLehtipuuDensity(0);
      $newImportedArticle->setLehtipuuBasalarea(0);
      $newImportedArticle->setLehtipuuGrowth(0);

      foreach ($propertyParents['treeinfo'] as $k => $product) {
        if (isset($propertyParents['treeinfo'][$k]['@attributes']['treeSpec'])) {
          $treeSpecie = $propertyParents['treeinfo'][$k]['@attributes']['treeSpec'];
          if ($treeSpecie == "Mänty") {
            $newImportedArticle->setMantyAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setMantyVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setMantyVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setMantyLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setMantyPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setMantyDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setMantyLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setMantyDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setMantyBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setMantyGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Kuusi") {
            $newImportedArticle->setKuusiAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setKuusiVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setKuusiVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setKuusiLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setKuusiPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setKuusiDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setKuusiLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setKuusiDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setKuusiBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setKuusiGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Rauduskoivu") {
            $newImportedArticle->setRauduskoivuAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setRauduskoivuVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setRauduskoivuVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setRauduskoivuLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setRauduskoivuPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setRauduskoivuDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setRauduskoivuLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setRauduskoivuDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setRauduskoivuBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setRauduskoivuGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Hieskoivu") {
            $newImportedArticle->setHieskoivuAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setHieskoivuVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setHieskoivuVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setHieskoivuLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setHieskoivuPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setHieskoivuDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setHieskoivuLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setHieskoivuDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setHieskoivuBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setHieskoivuGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Haapa") {
            $newImportedArticle->setHaapaAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setHaapaVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setHaapaVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setHaapaLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setHaapaPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setHaapaDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setHaapaLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setHaapaDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setHaapaBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setHaapaGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Harmaaleppä") {
            $newImportedArticle->setHarmaaleppaAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setHarmaaleppaVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setHarmaaleppaVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setHarmaaleppaLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setHarmaaleppaPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setHarmaaleppaDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setHarmaaleppaLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setHarmaaleppaDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setHarmaaleppaBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setHarmaaleppaGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Tervaleppä") {
            $newImportedArticle->setTervaleppaAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setTervaleppaVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setTervaleppaVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setTervaleppaLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setTervaleppaPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setTervaleppaDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setTervaleppaLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setTervaleppaDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setTervaleppaBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setTervaleppaGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Lehtipuu") {
            $newImportedArticle->setLehtipuuAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setLehtipuuVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setLehtipuuVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setLehtipuuLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setLehtipuuPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setLehtipuuDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setLehtipuuLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setLehtipuuDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setLehtipuuBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setLehtipuuGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
          if ($treeSpecie == "Havupuu") {
            $newImportedArticle->setHavupuuAge($propertyParents['treeinfo'][$k]['@attributes']['age']);
            $newImportedArticle->setHavupuuVolume(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3'], 3));
            $newImportedArticle->setHavupuuVolumeHa(round($propertyParents['treeinfo'][$k]['@attributes']['volume_m3_ha'], 3));
            $newImportedArticle->setHavupuuLogVolume(round($propertyParents['treeinfo'][$k]['@attributes']['logVolume'], 3));
            if (isset($treeInfo[$k]['@attributes']['pulpVolume'])) {
              $newImportedArticle->setHavupuuPulpVolume(round($treeInfo[$k]['@attributes']['pulpVolume'], 3));
            }
            $newImportedArticle->setHavupuuDiameter($propertyParents['treeinfo'][$k]['@attributes']['averageDiameter']);
            $newImportedArticle->setHavupuuLength($propertyParents['treeinfo'][$k]['@attributes']['averageLength']);
            $newImportedArticle->setHavupuuDensity($propertyParents['treeinfo'][$k]['@attributes']['density']);
            $newImportedArticle->setHavupuuBasalarea($propertyParents['treeinfo'][$k]['@attributes']['basalArea']);
            $newImportedArticle->setHavupuuGrowth($propertyParents['treeinfo'][$k]['@attributes']['growth']);
          }
        }
      }

      return $newImportedArticle;

    }

    public function addProperty($property)
    {
        return $this->save($property);
    }

    public function deleteProperty($id)
    {
        return $this->delete($this->find($id));
    }



}
