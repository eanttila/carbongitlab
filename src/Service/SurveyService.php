<?php

namespace App\Service;

use App\Entity\Survey;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class SurveyService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getSurvey($survey_id)
    {
        return $this->find($survey_id);
    }

    public function getAllSurveys()
    {
        return $this->findAll();
    }
	
	public function getForestKitPropertyAddress($address) {

      $newAddress = $address;

      return $newAddress;
    }

    public function getAllSurveysAsArray()
    {
      $query = $this->em->getRepository(Survey::class)
      ->createQueryBuilder('c')
      ->getQuery();
      
      return $result = $query->getResult(Query::HYDRATE_ARRAY);

    }

    public function searchByOwnerAndGetImportedArticles($owner) {

      $result = $this->em->getRepository(Property::class)
      ->createQueryBuilder('u')
      ->select('u, myAlias')
      ->leftJoin('u.importedArticles', 'myAlias')
      ->where('u.address LIKE :owner')
      ->setParameter('owner', '%'.$owner.'%')
      ->getQuery()
      ->getArrayResult();

      return $result;

    }

    public function searchByOwner($owner) {

      $result = $this->em->getRepository(Property::class)
      ->createQueryBuilder('u')
      ->where('u.owner LIKE :owner')
      ->setParameter('owner', '%'.$owner.'%')
      ->getQuery()
      ->getArrayResult();

      return $result;

    }

    public function addSurvey($survey)
    {
        return $this->save($survey);
    }

    public function deleteSurvey($id)
    {
        return $this->delete($this->find($id));
    }



}
