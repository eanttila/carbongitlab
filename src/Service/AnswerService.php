<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;


class AnswerService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getQuestion($question_id)
    {
        return $this->find($question_id);
    }

    public function getAllQuestions()
    {
        return $this->findAll();
    }

    public function getAllAnswersInArray() //these are not objects
    {
      $sql = "SELECT * FROM question";
      $stmt = $this->em->getConnection()->prepare($sql);
      $stmt->execute();
      $questionResults = $stmt->fetchAll();

      return $questionResults;
    }

    public function getAllQuestionsByCategory($categoryName)
    {

      $sql = "SELECT * FROM question WHERE category = '".$categoryName."'";
      $stmt = $this->em->getConnection()->prepare($sql);
      $stmt->execute();
      $questionResults = $stmt->fetchAll();

      return $questionResults;
    }

    public function filterQuestionsByLanguage($questions, $language)
    {
      $newQuestions = array();
      foreach ($questions as $item) {
        if ($item['language'] == $language) {
          $newQuestions[] = $item;
        }
      }
      return $newQuestions;
    }

/*
    public function getAllQuestionsByCategory($categoryName)
    {
        $result = array();
        $questions = $this->findAll();
        foreach ($questions as $item) {
          if ($item->getCategory() == $categoryName) {
            $result[] = $item;
          }
        }
        return $result;
    }
*/

    public function addQuestion($question)
    {
        return $this->save($question);
    }

    public function deleteQuestion($id)
    {
        return $this->delete($this->find($id));
    }



}
