<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;
use App\Entity\Property;
use App\Entity\Article;
use App\Entity\User;
use App\Entity\Company;

class UserService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getUser($user_id)
    {
        return $this->find($user_id);
    }

    public function getAllUser()
    {
        return $this->findAll();
    }

    public function searchByEmail($email) {

      $jsonResult = $this->em->getRepository(User::class)
      ->createQueryBuilder('u')
      ->where('u.username LIKE :email')
      ->setParameter('email', '%'.$email.'%')
      ->getQuery()
      ->getArrayResult();

      return $jsonResult;

    }

    public function addUser($user)
    {
        return $this->save($user);
    }

    public function deleteUser($id)
    {
        return $this->delete($this->find($id));
    }



}
