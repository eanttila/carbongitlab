<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;


class FactorService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getFactor($factor_id)
    {
        return $this->find($factor_id);
    }

    public function getAllFactors()
    {
        return $this->findAll();
    }

    public function addFactor($factor)
    {
        return $this->save($factor);
    }

    public function deleteFactor($id)
    {
        return $this->delete($this->find($id));
    }



}
