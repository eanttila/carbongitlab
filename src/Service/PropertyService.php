<?php

namespace App\Service;

use App\Entity\Property;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class PropertyService extends AbstractService
{

    public function __construct(EntityManager $em, $entityName)
    {
        $this->em = $em;
        $this->model = $em->getRepository($entityName);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getProperty($property_id)
    {
        return $this->find($property_id);
    }

    public function getAllProperties()
    {
        return $this->findAll();
    }
	
	public function getForestKitPropertyAddress($address) {

      $newAddress = $address;

      return $newAddress;
    }

    public function getAllPropertiesAsArray()
    {
      $query = $this->em->getRepository(Property::class)
      ->createQueryBuilder('c')
      ->getQuery();
      
      return $result = $query->getResult(Query::HYDRATE_ARRAY);

    }

    public function searchByOwnerAndGetImportedArticles($owner) {

      $result = $this->em->getRepository(Property::class)
      ->createQueryBuilder('u')
      ->select('u, myAlias')
      ->leftJoin('u.importedArticles', 'myAlias')
      ->where('u.address LIKE :owner')
      ->setParameter('owner', '%'.$owner.'%')
      ->getQuery()
      ->getArrayResult();

      return $result;

    }

    public function searchByOwner($owner) {

      $result = $this->em->getRepository(Property::class)
      ->createQueryBuilder('u')
      ->where('u.owner LIKE :owner')
      ->setParameter('owner', '%'.$owner.'%')
      ->getQuery()
      ->getArrayResult();

      return $result;

    }

    public function addProperty($property)
    {
        return $this->save($property);
    }

    public function deleteProperty($id)
    {
        return $this->delete($this->find($id));
    }



}
