***
git add .
git commit -am "make it better"
git push heroku master

If changes in ORMs/DB:
- Drop tables if needed schema update (MySqlWorkbench)
- run on heroku: (heroku run) php bin/console doctrine:schema:update --force
Also for migration stuff (pretty much not needed):
- run on heroku: php bin/console doctrine:migrations:diff
- run on heroku: php bin/console doctrine:migrations:migrate
NOTES:
Can't use user repo directly or sessions are dead!
Use Doctrine queries for finding user's stuff.
***